package org.hashdrive.ui.createwalletboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import android.net.Uri;
import android.os.Bundle;

import org.hashdrive.R;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateWalletActivity extends AppCompatActivity implements CreateWalletStep1Fragment.OnFragmentInteractionListener,
        CreateWalletStep2Fragment.OnFragmentInteractionListener,
        CreateWalletStep3Fragment.OnFragmentInteractionListener,
        CreateWalletStep4Fragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_wallet);
        ButterKnife.bind(this);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
