package org.hashdrive.ui;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavOptions;
import androidx.navigation.fragment.NavHostFragment;

import org.hashdrive.R;
import org.hashdrive.ui.auth.LoginSessionViewModel;
import org.hashdrive.ui.onboard.MainActivity;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public abstract class BaseFragment extends Fragment {
    protected MainActivity mainActivity;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    protected LoginSessionViewModel loginSessionViewModel;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }
        loginSessionViewModel = ViewModelProviders.of(requireActivity()).get(LoginSessionViewModel.class);
        loginSessionViewModel.getAuthenticationState().observe(getViewLifecycleOwner(), this::handleLoginSessionAuthState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        compositeDisposable.clear();
    }

    protected void handleLoginSessionAuthState(LoginSessionViewModel.AuthenticationState authenticationState) {
        Timber.d("handleLoginSessionAuthState %s!", authenticationState.name());
        if(authenticationState != LoginSessionViewModel.AuthenticationState.AUTHENTICATED) {
            Timber.d("Not valid login session, opening splash activity...");
            // TODO: problem if used
            //NavOptions.Builder navBuilder = new NavOptions.Builder();
           // NavOptions navOptions = navBuilder.setLaunchSingleTop(true).build();
            // NavHostFragment.findNavController(this).navigate(R.id.splashFragment, null, navOptions);
        }
    }

}
