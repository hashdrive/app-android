package org.hashdrive.ui.mainboard;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.snackbar.Snackbar;
import com.ramotion.circlemenu.CircleMenuView;

import org.hashdrive.R;
import org.hashdrive.database.ride.VehicleType;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Completable;
import timber.log.Timber;


public class HomeFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private VehicleType mVehicleType;

    @BindView(R.id.circle_menu)
    CircleMenuView circleMenuView;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        setCircleMenuEventListener();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Completable.timer(400, TimeUnit.MILLISECONDS)
                .subscribe(() -> {
                    Timber.d("Executing delayed open circle menu operation...");
                    requireActivity().runOnUiThread(() -> circleMenuView.open(true));
                }, throwable -> Timber.e(throwable, "Failed to execute delayed open circle menu operation!"));
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void setCircleMenuEventListener() {
        TypedArray icons = getResources().obtainTypedArray(R.array.icons);
        circleMenuView.setEventListener(new CircleMenuView.EventListener() {
            @Override
            public void onMenuOpenAnimationStart(@NonNull CircleMenuView view) {
                super.onMenuOpenAnimationStart(view);
                Timber.d("onMenuOpenAnimationStart");
            }

            @Override
            public void onMenuOpenAnimationEnd(@NonNull CircleMenuView view) {
                super.onMenuOpenAnimationEnd(view);
                Timber.d("onMenuOpenAnimationEnd");

            }

            @Override
            public void onMenuCloseAnimationStart(@NonNull CircleMenuView view) {
                super.onMenuCloseAnimationStart(view);
                Timber.d("onMenuCloseAnimationStart");
            }

            @Override
            public void onMenuCloseAnimationEnd(@NonNull CircleMenuView view) {
                super.onMenuCloseAnimationEnd(view);
                Timber.d("onMenuCloseAnimationEnd");
            }

            @Override
            public void onButtonClickAnimationStart(@NonNull CircleMenuView view, int buttonIndex) {
                super.onButtonClickAnimationStart(view, buttonIndex);
                Timber.d("onButtonClickAnimationStart at index: %d", buttonIndex);
            }

            @Override
            public void onButtonClickAnimationEnd(@NonNull CircleMenuView view, int buttonIndex) {
                super.onButtonClickAnimationEnd(view, buttonIndex);
                Timber.d("onButtonClickAnimationEnd at index: %d", buttonIndex);
                int resId = icons.getResourceId(buttonIndex, -1);
                if (resId == R.drawable.ic_car) {
                    Navigation.findNavController(Objects.requireNonNull(getView())).navigate(HomeFragmentDirections.actionHomeFragmentToRideOffersActivity().setVehicleType(VehicleType.CAR));
                } else if (resId == R.drawable.icon_scooter_30) {
                    Navigation.findNavController(Objects.requireNonNull(getView())).navigate(HomeFragmentDirections.actionHomeFragmentToRideOffersActivity().setVehicleType(VehicleType.MOTORCYCLE));
                } else if (resId == R.drawable.icon_mobile_home_30) {
                    Navigation.findNavController(Objects.requireNonNull(getView())).navigate(HomeFragmentDirections.actionHomeFragmentToRideOffersActivity().setVehicleType(VehicleType.VAN));
                } else if (resId == R.drawable.icon_jet_ski_30) {
                    Navigation.findNavController(Objects.requireNonNull(getView())).navigate(HomeFragmentDirections.actionHomeFragmentToRideOffersActivity().setVehicleType(VehicleType.JETSKI)); // TODO: 2019-07-14 this should be disabled at first
                } else if (resId == R.drawable.icon_sail_boat_26) {
                    Navigation.findNavController(Objects.requireNonNull(getView())).navigate(HomeFragmentDirections.actionHomeFragmentToRideOffersActivity().setVehicleType(VehicleType.BOAT)); // TODO: 2019-07-14 this should be disabled at first
                } else {
                    Snackbar.make(getView(), "Unknown Button Clicked!", 1).show();
                }
            }

            @Override
            public boolean onButtonLongClick(@NonNull CircleMenuView view, int buttonIndex) {
                Timber.d("onButtonLongClick at index: %d", buttonIndex);
                return super.onButtonLongClick(view, buttonIndex);
            }

            @Override
            public void onButtonLongClickAnimationStart(@NonNull CircleMenuView view, int buttonIndex) {
                super.onButtonLongClickAnimationStart(view, buttonIndex);
                Timber.d("onButtonLongClickAnimationStart at index: %d", buttonIndex);
            }

            @Override
            public void onButtonLongClickAnimationEnd(@NonNull CircleMenuView view, int buttonIndex) {
                super.onButtonLongClickAnimationEnd(view, buttonIndex);
                Timber.d("onButtonLongClickAnimationEnd at index: %d", buttonIndex);
            }
        });

    }
}
