package org.hashdrive.ui.mainboard;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.navigation.Navigation;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import org.hashdrive.R;

import java.util.Objects;

import butterknife.ButterKnife;
import timber.log.Timber;


public class SettingsFragment extends PreferenceFragmentCompat {

    private OnFragmentInteractionListener mListener;
    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        String preferenceKey = preference.getKey(); // see R.xml.preferences for possible options
        Timber.d("Preference clicked: %s", preferenceKey);
        if(preferenceKey.equals("setup_wallet")) {
                    Navigation.findNavController(Objects.requireNonNull(getView())).navigate(SettingsFragmentDirections.actionSettingsFragmentToCreateImportFragment());
        } else if(preferenceKey.equals("developer_tools")) {
                    Navigation.findNavController(Objects.requireNonNull(getView())).navigate(SettingsFragmentDirections.actionSettingsFragmentToDeveloperToolsActivity());
        }
        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
}
