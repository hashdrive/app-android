package org.hashdrive.ui.mainboard;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kizitonwose.calendarview.CalendarView;
import com.kizitonwose.calendarview.model.CalendarDay;
import com.kizitonwose.calendarview.model.CalendarMonth;
import com.kizitonwose.calendarview.model.DayOwner;
import com.kizitonwose.calendarview.ui.DayBinder;
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder;
import com.kizitonwose.calendarview.ui.ViewContainer;

import org.hashdrive.R;
import org.hashdrive.ui.BaseFragment;
import org.jetbrains.annotations.NotNull;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;
import org.threeten.bp.YearMonth;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.WeekFields;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;


public class RidesCalendarFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;

    private RidesCalendarEventsAdapter ridesCalendarEventsAdapter;
    private LocalDate selectedDate;
    private LocalDate today = LocalDate.now();

    private DateTimeFormatter titleSameYearFormatter = DateTimeFormatter.ofPattern("MMMM");
    private DateTimeFormatter titleFormatter = DateTimeFormatter.ofPattern("MMM yyyy");
    private DateTimeFormatter selectionFormatter = DateTimeFormatter.ofPattern("d MMM yyyy");
    private Map<LocalDate, List<Event>> events = new HashMap<LocalDate, List<Event>>();

    @BindView(R.id.tv_calendar_header)
    TextView calendarHeaderTextView;

    @BindView(R.id.calendar_view_rides)
    CalendarView ridesCalendarView;

    @BindView(R.id.tv_selected_date)
    TextView selectedDateTextView;

    @BindView(R.id.recycler_view__rides_calendar)
    RecyclerView ridesCalendarRecyclerView;


    public RidesCalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rides_calendar, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ridesCalendarEventsAdapter = new RidesCalendarEventsAdapter(new RidesCalendarEventsListener() {
            @Override
            public void onClick(Event event) {
                Timber.d("event clicked: %s", event);
            }
        });
        initRidesCalendar();
    }

    private void initRidesCalendar() {
        ridesCalendarRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false));
        ridesCalendarRecyclerView.setAdapter(ridesCalendarEventsAdapter);
        ridesCalendarRecyclerView.addItemDecoration(new DividerItemDecoration(requireContext(), RecyclerView.VERTICAL));

        DayOfWeek[] daysOfWeek = daysOfWeekFromLocale();
        YearMonth currentMonth = YearMonth.now();
        ridesCalendarView.setup(currentMonth.minusMonths(10), currentMonth.plusMonths(10), daysOfWeek[0]);
        ridesCalendarView.scrollToMonth(currentMonth);

        // Show today's events initially.
        selectDate(today);

        ridesCalendarView.setDayBinder(new DayBinder<DayViewContainer>() {
            @NotNull
            @Override
            public DayViewContainer create(@NotNull View view) {
                return new DayViewContainer(view);
            }

            @Override
            public void bind(@NotNull DayViewContainer dayViewContainer, @NotNull CalendarDay calendarDay) {
                dayViewContainer.day = calendarDay;
                TextView textView = dayViewContainer.ridesCalendarDayTextView;
                View dotView = dayViewContainer.ridesCalendarDotView;

                textView.setText(calendarDay.getDate().getDayOfMonth() + "");

                if (calendarDay.getOwner() == DayOwner.THIS_MONTH) {
                    textView.setVisibility(View.VISIBLE);
                    if (calendarDay.getDate() == today) {
                        textView.setTextColor(getResources().getColor(R.color.rides_calendar_white));
                        textView.setBackgroundResource(R.drawable.rides_calendar_today_bg);
                        dotView.setVisibility(View.INVISIBLE);
                    } else if (calendarDay.getDate() == selectedDate) {
                        textView.setTextColor(getResources().getColor(R.color.rides_calendar_blue));
                        textView.setBackgroundResource(R.drawable.rides_calendar_selected_bg);
                        dotView.setVisibility(View.INVISIBLE);
                    } else {
                        textView.setTextColor(getResources().getColor(R.color.rides_calendar_black));
                        textView.setBackground(null);
                        List<Event> eventList = events.get(calendarDay.getDate());
                        dotView.setVisibility(eventList != null && !eventList.isEmpty() ? View.VISIBLE : View.GONE);
                    }
                } else {
                    textView.setVisibility(View.INVISIBLE);
                    dotView.setVisibility(View.INVISIBLE);
                }
            }

        });


        ridesCalendarView.setMonthScrollListener(calendarMonth -> {
            calendarHeaderTextView.setText((calendarMonth.getYear() == today.getYear() ?
                    titleSameYearFormatter.format(calendarMonth.getYearMonth()) :
                    titleFormatter.format(calendarMonth.getYearMonth()))
            );

            // Select the first day of the month when
            // we scroll to a new month.
            selectDate(calendarMonth.getYearMonth().atDay(1));
            return null;
        });

        ridesCalendarView.setMonthHeaderBinder(new MonthHeaderFooterBinder<MonthViewContainer>() {
            @NotNull
            @Override
            public MonthViewContainer create(@NotNull View view) {
                return new MonthViewContainer(view);
            }

            @Override
            public void bind(@NotNull MonthViewContainer monthViewContainer, @NotNull CalendarMonth calendarMonth) {
                // Setup each header day text if we have not done that already.
                if (monthViewContainer.legendLayout.getTag() == null) {
                    monthViewContainer.legendLayout.setTag(calendarMonth.getYearMonth());
                    int count = monthViewContainer.legendLayout.getChildCount();
                    for (int i = 0; i < count; i++) {
                        TextView textView = (TextView) monthViewContainer.legendLayout.getChildAt(i);
                        textView.setText(Character.toString(daysOfWeek[i].name().charAt(0)));
                        textView.setTextColor(getResources().getColor(R.color.rides_calendar_black));
                    }
                }
            }
        });



        String[] stubRides = new String[] {
                "Drive to Ljubljana", "Drive to Novo mesto", "Drive to Koper", "Drive to Dunaj", "Drive to Maribor"
        };

        Random random = new Random();
        for (int i = 0; i < 30; i++) {
            int dayOfMonth = random.nextInt() % selectedDate.getMonth().maxLength();
            if(dayOfMonth == 0) {
                dayOfMonth = i > 0? i : 1;
            }
            selectDate(LocalDate.of(selectedDate.getYear(), selectedDate.getMonthValue(), Math.abs(dayOfMonth)));
            int index = random.nextInt() % stubRides.length;
            saveEvent(stubRides[Math.abs(index)]);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    protected class Event {
        String id;
        String text;
        LocalDate date;

        public Event(String id, String text, LocalDate date) {
            this.id = id;
            this.text = text;
            this.date = date;
        }

        @Override
        public String toString() {
            return "Event{" +
                    "id='" + id + '\'' +
                    ", text='" + text + '\'' +
                    ", date=" + date +
                    '}';
        }
    }

    private DayOfWeek[] daysOfWeekFromLocale() {
        DayOfWeek firstDayOfWeek = WeekFields.of(Locale.getDefault()).getFirstDayOfWeek();
        DayOfWeek[] daysOfWeek = DayOfWeek.values();
        // Order `daysOfWeek` array so that firstDayOfWeek is at index 0.
        if (firstDayOfWeek != DayOfWeek.MONDAY) {
            DayOfWeek[] daysOfWeekFromLocale = new DayOfWeek[daysOfWeek.length];
            DayOfWeek[] rhs = Arrays.copyOfRange(daysOfWeek, firstDayOfWeek.ordinal(), daysOfWeek.length);
            DayOfWeek[] lhs = Arrays.copyOfRange(daysOfWeek, 0, firstDayOfWeek.ordinal());
            System.arraycopy(rhs, 0, daysOfWeekFromLocale, 0, rhs.length);
            System.arraycopy(lhs, 0, daysOfWeekFromLocale, rhs.length, lhs.length);
            return daysOfWeekFromLocale;
        }
        return daysOfWeek;
    }

    interface RidesCalendarEventsListener {
        void onClick(Event event);
    }

    protected class RidesCalendarEventsAdapter extends RecyclerView.Adapter<RidesCalendarEventsAdapter.ViewHolder> {
        ArrayList<Event> events = new ArrayList();
        RidesCalendarEventsListener listener;

        public RidesCalendarEventsAdapter(RidesCalendarEventsListener listener) {
            this.listener = listener;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.rides_calendar_event_item_view, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.bind(events.get(position));
        }

        @Override
        public int getItemCount() {
            return events.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.itemEventText)
            TextView itemEventTextView;

            ViewHolder(@NonNull View containerView) {
                super(containerView);
                ButterKnife.bind(ViewHolder.this, containerView);
                itemView.setOnClickListener(v -> {
                    listener.onClick(events.get(getAdapterPosition()));
                });
            }

            void bind(Event event) {
                itemEventTextView.setText(event.text);
            }
        }
    }

    protected class DayViewContainer extends ViewContainer {
        // Will be set when this container is bound
        private CalendarDay day;
        @BindView(R.id.ridesCalendarDayTextView)
        TextView ridesCalendarDayTextView;
        @BindView(R.id.ridesCalendarDotView)
        View ridesCalendarDotView;

        public DayViewContainer(@NotNull View view) {
            super(view);
            ButterKnife.bind(DayViewContainer.this, view);
            view.setOnClickListener(v -> {
                if (day.getOwner() == DayOwner.THIS_MONTH) {
                    selectDate(day.getDate());
                }
            });
        }
    }

    protected class MonthViewContainer extends ViewContainer {
        @BindView(R.id.legendLayout)
        LinearLayout legendLayout;

        public MonthViewContainer(@NotNull View view) {
            super(view);
            ButterKnife.bind(MonthViewContainer.this, view);
        }
    }

    private void selectDate(LocalDate date) {
        if (selectedDate != date) {
            LocalDate oldDate = selectedDate;
            selectedDate = date;
            if (oldDate != null) {
                ridesCalendarView.notifyDateChanged(oldDate);
            }
            ridesCalendarView.notifyDateChanged(date);
            updateAdapterForDate(date);
        }
    }

    private void saveEvent(String text) {
        if (text.isEmpty()) {
            Toast.makeText(requireContext(), R.string.input_required, Toast.LENGTH_LONG).show();
        } else {
            if (selectedDate != null) {
                List<Event> eventList = events.get(selectedDate);
                if (eventList == null) {
                    eventList = new ArrayList<>();
                    events.put(selectedDate, eventList);
                }
                eventList.add(new Event(UUID.randomUUID().toString(), text, selectedDate));
                updateAdapterForDate(selectedDate);
            }
        }
    }

    private void deleteEvent(Event event) {
        LocalDate date = event.date;
        List<Event> eventList = events.get(date);
        if (eventList != null) {
            eventList.remove(event);
            updateAdapterForDate(date);
        }
    }

    private void updateAdapterForDate(LocalDate date) {
        ridesCalendarEventsAdapter.events.clear();
        List<Event> eventsToAdd = events.get(date);
        if (eventsToAdd == null) {
            eventsToAdd = new ArrayList<>();
        }
        ridesCalendarEventsAdapter.events.addAll(eventsToAdd);
        ridesCalendarEventsAdapter.notifyDataSetChanged();
        selectedDateTextView.setText(selectionFormatter.format(date));
    }

//    override fun onStart() {
//        super.onStart()
//        (activity as AppCompatActivity).homeToolbar.setBackgroundColor(requireContext().getColorCompat(R.color.example_3_toolbar_color))
//        requireActivity().window.statusBarColor = requireContext().getColorCompat(R.color.example_3_statusbar_color)
//    }
//
//    override fun onStop() {
//        super.onStop()
//        (activity as AppCompatActivity).homeToolbar.setBackgroundColor(requireContext().getColorCompat(R.color.colorPrimary))
//        requireActivity().window.statusBarColor = requireContext().getColorCompat(R.color.colorPrimaryDark)
//    }

}
