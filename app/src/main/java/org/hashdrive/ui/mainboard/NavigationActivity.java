package org.hashdrive.ui.mainboard;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.hashdrive.R;
import org.hashdrive.ui.onboard.CreateImportFragment;

import java.util.Objects;

import butterknife.ButterKnife;

public class NavigationActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener,
        RidesCalendarFragment.OnFragmentInteractionListener,
        SettingsFragment.OnFragmentInteractionListener,
        CreateImportFragment.OnFragmentInteractionListener {

    private BottomNavigationView mBottomNavigationView;
    private NavHostFragment mNavHostFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_navigation);
        ButterKnife.bind(this);
        setupNavigation();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    private void setupNavigation() {
        mBottomNavigationView = findViewById(R.id.bottomNavigationView);
        mNavHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.navigationHostFragment);
        NavigationUI.setupWithNavController(mBottomNavigationView, Objects.requireNonNull(mNavHostFragment).getNavController());
    }
}
