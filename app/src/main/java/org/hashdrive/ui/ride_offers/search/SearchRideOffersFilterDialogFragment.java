package org.hashdrive.ui.ride_offers.search;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.hashdrive.R;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.LocalTime;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;


public class SearchRideOffersFilterDialogFragment extends DialogFragment {

    private CompositeDisposable mCompositeDisposable;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.text_input_edit_text_ride_offer_departure_time)
    TextInputEditText departureTimeTextInputEditText;

    @BindView(R.id.text_input_edit_text_ride_offer_arrival_time)
    TextInputEditText arrivalTimeTextInputEditText;

    @BindView(R.id.switch_material_enable_price_limit)
    SwitchMaterial priceLimitSwitchMaterial;
    @BindView(R.id.text_input_layout_ride_offer_price)
    TextInputLayout rideOfferPriceTextInputLayout;
    @BindView(R.id.text_input_edit_text_ride_offer_price)
    TextInputEditText priceTextInputEditText;

    @BindView(R.id.switch_material_enable_pickup_location_radius_distance)
    SwitchMaterial enablePickupLocationRadiusDistanceSwitchMaterial;
    @BindView(R.id.tv_pickup_location_radius_distance)
    TextView pickupLocationRadiusDistanceTextView;
    @BindView(R.id.seek_bar_pickup_location_radius_distance)
    SeekBar pickupLocationRadiusDistanceSeekBar;

    @BindView(R.id.switch_material_enable_pickup_location_radius_time)
    SwitchMaterial enablePickupLocationRadiusTimeSwitchMaterial;
    @BindView(R.id.tv_pickup_location_radius_time)
    TextView pickupLocationRadiusTimeTextView;
    @BindView(R.id.seek_bar_pickup_location_radius_time)
    SeekBar pickupLocationRadiusTimeSeekBar;

    @BindView(R.id.switch_material_enable_end_location_radius_distance)
    SwitchMaterial enableEndLocationRadiusDistanceSwitchMaterial;
    @BindView(R.id.tv_end_location_radius_distance)
    TextView endLocationRadiusDistanceTextView;
    @BindView(R.id.seek_bar_end_location_radius_distance)
    SeekBar endLocationRadiusDistanceSeekBar;

    @BindView(R.id.switch_material_enable_end_location_radius_time)
    SwitchMaterial enableEndLocationRadiusTimeSwitchMaterial;
    @BindView(R.id.tv_end_location_radius_time)
    TextView endLocationRadiusTimeTextView;
    @BindView(R.id.seek_bar_end_location_radius_time)
    SeekBar endLocationRadiusTimeSeekBar;

    public static SearchRideOffersFilterDialogFragment display(FragmentManager fragmentManager) {
        SearchRideOffersFilterDialogFragment searchRideOffersFilterDialog = new SearchRideOffersFilterDialogFragment();
        searchRideOffersFilterDialog.show(fragmentManager, SearchRideOffersFilterDialogFragment.class.getSimpleName());
        return searchRideOffersFilterDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_ride_offers_filter_dialog, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setNavigationOnClickListener(v -> dismiss());
        toolbar.setTitle(R.string.filter_ride_offers);
        toolbar.inflateMenu(R.menu.ride_offers_filter_dialog_menu);
        toolbar.setOnMenuItemClickListener(item -> {
            dismiss();
            return true;
        });
        initDateTimePickers();
        initSwitchMaterialViews();
        initSeekBarTooltips();

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            assert dialog.getWindow() != null;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setWindowAnimations(R.style.AppTheme_Slide);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCompositeDisposable.clear();
    }

    private void initSeekBarTooltips() {
        @SuppressWarnings("unchecked")
        Pair<TextView, SeekBar>[] tooltipSeekbarPairs = new Pair[]{
                Pair.create(pickupLocationRadiusDistanceTextView, pickupLocationRadiusDistanceSeekBar),
                Pair.create(pickupLocationRadiusTimeTextView, pickupLocationRadiusTimeSeekBar),
                Pair.create(endLocationRadiusDistanceTextView, endLocationRadiusDistanceSeekBar),
                Pair.create(endLocationRadiusTimeTextView, endLocationRadiusTimeSeekBar),
        };


        for (Pair<TextView, SeekBar> tooltipSeekbarPair : tooltipSeekbarPairs) {
            final LinearLayout.LayoutParams seekBarLayoutParams = (LinearLayout.LayoutParams) tooltipSeekbarPair.second.getLayoutParams();
            final int[] location = new int[2];

            tooltipSeekbarPair.second.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                private Disposable refreshTooltipDisposable;

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    final int[] progressCounter = new int[2];
                    progressCounter[0] = seekBar.getProgress();

                    refreshTooltipDisposable = Observable.interval(16, TimeUnit.MILLISECONDS) // 16 millis translates to 62,5 fps which should be high enough refresh rate
                            .map(l -> seekBar.getProgress())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(progress -> {
                                if (progressCounter[0] == progress) {
                                    progressCounter[1]++;
                                } else {
                                    progressCounter[0] = progress;
                                    progressCounter[1] = 0;
                                }
                                // tooltip will be refresh twice (mitigation because getWidth of tooltip returns wrong value in #refreshTooltip method)
                                if (progressCounter[1] < 3) {
                                    refreshTooltip(tooltipSeekbarPair.first, seekBar, progress);
                                }
                            }, throwable -> Timber.e(throwable, "refreshTooltipDisposable failed!"));
                    mCompositeDisposable.add(refreshTooltipDisposable);
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    mCompositeDisposable.remove(refreshTooltipDisposable);
                    if (refreshTooltipDisposable != null && !refreshTooltipDisposable.isDisposed()) {
                        refreshTooltipDisposable.dispose();
                        refreshTooltipDisposable = null;
                    }
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        Timber.e(e, "Thread sleep failed!");
                    }
                    refreshTooltip(tooltipSeekbarPair.first, seekBar, seekBar.getProgress());
                }


                private void refreshTooltip(TextView tooltip, SeekBar seekBar, int progress) {
                    tooltip.setText(getTooltipText(tooltip, progress));
                    seekBar.getLocationOnScreen(location);
                    final int seekBarX = location[0];
                    float x = seekBarX
                            + seekBar.getThumb().getBounds().left
                            + seekBarLayoutParams.leftMargin
                            + seekBar.getPaddingLeft()
                            - tooltip.getWidth() / 2f;

                    float toolTipRightX = x + tooltip.getWidth();

                    float smallestOffsetX = seekBarX + seekBar.getPaddingLeft();
                    float seekBarRightX = seekBarX - seekBar.getPaddingRight() + seekBar.getWidth();
                    if (x < smallestOffsetX) {
                        x = smallestOffsetX;
                    } else {
                        if (toolTipRightX > seekBarRightX) {
                            x += seekBarRightX - toolTipRightX; // add negative value (snap to right edge)
                        }
                    }
                    Timber.d("Width of tooltip: %d", tooltip.getWidth());
                    tooltip.setX(x);
                }
            });
        }
    }

    private String getTooltipText(TextView tooltipTextView, int progress) {
        switch (tooltipTextView.getId()) {
            case R.id.tv_pickup_location_radius_distance:
            case R.id.tv_end_location_radius_distance:
                // in meters [0...30000] or 30 km max -> format %d km %d m
                int kilometers = progress / 1000;
                int meters = progress % 1000;
                if (kilometers > 0 && meters > 0) {
                    return kilometers + "km " + meters + "m";
                } else if (kilometers > 0) {
                    return kilometers + "km";
                } else {
                    return meters + "m";
                }

            case R.id.tv_pickup_location_radius_time:
            case R.id.tv_end_location_radius_time:
                // in seconds [0...1200] or 20 minutes max -> format %d min %d s
                int minutes = progress / 60;
                int seconds = progress % 60;
                if (minutes > 0 && seconds > 0) {
                    return minutes + "min " + seconds + "s";
                } else if (minutes > 0) {
                    return minutes + "min";
                } else {
                    return seconds + "sec";
                }
        }

        return "/";
    }

    private void initSwitchMaterialViews() {
        // price limit is special (uses text input instead of seek bars)
        priceLimitSwitchMaterial.setChecked(false);
        rideOfferPriceTextInputLayout.setVisibility(View.GONE);
        priceLimitSwitchMaterial.setOnCheckedChangeListener((buttonView, isChecked) -> {
            rideOfferPriceTextInputLayout.setVisibility(isChecked? View.VISIBLE : View.GONE);
        });

        @SuppressWarnings("unchecked") Pair<SwitchMaterial, Pair<TextView, SeekBar>> switchMaterialSeekBars[] = new Pair[] {
                Pair.create(enablePickupLocationRadiusDistanceSwitchMaterial, Pair.create(pickupLocationRadiusDistanceTextView, pickupLocationRadiusDistanceSeekBar)),
                Pair.create(enablePickupLocationRadiusTimeSwitchMaterial, Pair.create(pickupLocationRadiusTimeTextView, pickupLocationRadiusTimeSeekBar)),
                Pair.create(enableEndLocationRadiusDistanceSwitchMaterial, Pair.create(endLocationRadiusDistanceTextView, endLocationRadiusDistanceSeekBar)),
                Pair.create(enableEndLocationRadiusTimeSwitchMaterial, Pair.create(endLocationRadiusTimeTextView, endLocationRadiusTimeSeekBar))
        };

        for (Pair<SwitchMaterial, Pair<TextView, SeekBar>> switchMaterialSeekBar : switchMaterialSeekBars) {
            switchMaterialSeekBar.first.setChecked(false);
            switchMaterialSeekBar.second.first.setVisibility(View.GONE);
            switchMaterialSeekBar.second.second.setVisibility(View.GONE);
            switchMaterialSeekBar.first.setOnCheckedChangeListener((buttonView, isChecked) -> {
                switchMaterialSeekBar.second.first.setVisibility(isChecked? View.VISIBLE : View.GONE);
                switchMaterialSeekBar.second.second.setVisibility(isChecked? View.VISIBLE : View.GONE);
            });
        }
    }

    private void initDateTimePickers() {
        departureTimeTextInputEditText.setInputType(InputType.TYPE_NULL);
        departureTimeTextInputEditText.setOnTouchListener((view, motionEvent) -> {
            Timber.d("Motion event: %s", motionEvent.getAction());
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                mCompositeDisposable.add(showDatePicker(R.string.arrival_date, LocalDate.now())
                        .flatMap(localDate -> showTimePicker(R.string.arrival_time).map(localTime -> Pair.create(localDate, localTime)))
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(localDateLocalTimePair -> {
                            LocalDateTime localDateTime = LocalDateTime.of(localDateLocalTimePair.first, localDateLocalTimePair.second);
                            Timber.d("Date and time set to: %s", localDateTime.toString());
                            ((EditText) view).setText(localDateTime.toString());
                        }, throwable -> {
                            Timber.e(throwable, "Setting departure time failed!");
                        })
                );
            }
            return false;
        });
        arrivalTimeTextInputEditText.setInputType(InputType.TYPE_NULL);
        arrivalTimeTextInputEditText.setOnTouchListener((view, motionEvent) -> {
            Timber.d("Motion event: %s", motionEvent.getAction());
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                mCompositeDisposable.add(showDatePicker(R.string.departure_date, LocalDate.now())
                        .flatMap(localDate -> showTimePicker(R.string.departure_time).map(localTime -> Pair.create(localDate, localTime)))
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(localDateLocalTimePair -> {
                            LocalDateTime localDateTime = LocalDateTime.of(localDateLocalTimePair.first, localDateLocalTimePair.second);
                            Timber.d("Date and time set to: %s", localDateTime.toString());
                            ((EditText) view).setText(localDateTime.toString());
                        }, throwable -> {
                            Timber.e(throwable, "Setting arrival time failed!");
                        })
                );
            }
            return false;
        });

    }

    private Single<LocalDate> showDatePicker(@StringRes int titleResId, @NonNull final LocalDate initialDate) {
        return Single.create(emitter -> {
            DatePickerDialog dpd = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) -> {
                LocalDate localDate = LocalDate.of(year, monthOfYear + 1, dayOfMonth);
                Timber.d("Date picker set to: %s", localDate.toString());
                emitter.onSuccess(localDate);
            }, initialDate.getYear(), initialDate.getMonthValue() - 1, initialDate.getDayOfMonth());
            dpd.setOnCancelListener(dialogInterface -> emitter.onError(new Throwable("Date picker canceled!")));
            dpd.setOnDismissListener(dialogInterface -> Timber.d("Date picker dismissed!"));

            assert getContext() != null;
            assert getActivity() != null;

            Resources res = getContext().getResources();
            dpd.setVersion(DatePickerDialog.Version.VERSION_2);
            dpd.setAccentColor(res.getColor(R.color.colorPrimary));
            dpd.setCancelColor(res.getColor(R.color.colorPureWhite));
            dpd.setOkColor(res.getColor(R.color.colorPureWhite));
            dpd.setYearRange(LocalDate.now().getYear() - 3, LocalDate.now().getYear());
            dpd.showYearPickerFirst(false);
            dpd.setTitle(res.getString(titleResId));
            dpd.show(getActivity().getSupportFragmentManager(), SearchRideOffersFilterDialogFragment.class.getSimpleName());
        });
    }

    private Single<LocalTime> showTimePicker(@StringRes int titleResId) {
        return Single.create(emitter -> {
            LocalTime localTime = LocalTime.now();
            TimePickerDialog tpd = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {
                Timber.d("Time picker - item selected!");
                emitter.onSuccess(LocalTime.of(hourOfDay, minute, second));
            }, localTime.getHour(), localTime.getMinute(), DateFormat.is24HourFormat(getContext()));

            tpd.setOnCancelListener(dialogInterface -> emitter.onError(new Throwable("Time picker cancelled!")));
            tpd.setOnDismissListener(dialogInterface -> Timber.d("Time picker dismissed!"));

            assert getContext() != null;
            assert getActivity() != null;

            Resources res = getContext().getResources();
            tpd.setVersion(TimePickerDialog.Version.VERSION_2);
            tpd.setAccentColor(res.getColor(R.color.colorPrimary));
            tpd.setCancelColor(res.getColor(R.color.colorPureWhite));
            tpd.setOkColor(res.getColor(R.color.colorPureWhite));
            tpd.setTitle(res.getString(titleResId));
            tpd.show(getActivity().getSupportFragmentManager(), SearchRideOffersFilterDialogFragment.class.getSimpleName());
        });
    }

}
