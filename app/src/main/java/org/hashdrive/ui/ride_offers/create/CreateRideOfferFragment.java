package org.hashdrive.ui.ride_offers.create;

import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Pair;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputEditText;
import com.google.common.base.Preconditions;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.hashdrive.Constants;
import org.hashdrive.R;
import org.hashdrive.ui.ride_offers.BaseFragment;
import org.hashdrive.ui.ride_offers.RideOffer;
import org.hashdrive.ui.ride_offers.RideOffersActivity;
import org.hashdrive.ui.ride_offers.search.SearchRideOffersFilterDialogFragment;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.LocalTime;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneOffset;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.gas.ContractGasProvider;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class CreateRideOfferFragment extends BaseFragment {

    private int mInitialFabVisibility;
    private int[] flipperPages = new int[]{
            R.string.departure_arrival_time, R.string.available_spots_and_price, R.string.other_options, R.string.notes
    };
    private GestureDetector mGestureDetector;
    private CompositeDisposable mCompositeDisposable;
    private CreateRideOfferViewModel mCreateRideOfferViewModel;

    class CustomGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            // Swipe left - next
            if (e1.getX() > e2.getX()) {
                flipperShowNext();
            }
            // Swipe right - previous
            if (e1.getX() < e2.getX()) {
                flipperShowPrevious();
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }

    private Web3j mWeb3;


    @BindView(R.id.tv_active_page_title)
    TextView activePageTitleTextView;

    @BindView(R.id.btn_prev)
    MaterialButton previousMaterialButton;

    @BindView(R.id.btn_next)
    MaterialButton nextMaterialButton;

    @BindView(R.id.ll_progress_bar_container)
    LinearLayout progressBarContainerLinearLayout;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.imageview_ride_offer_status)
    ImageView rideOfferStatusImageView;
    @BindView(R.id.tv_progress_bar_caption)
    TextView progressBarCaptionTextView;
    @BindView(R.id.btn_retry)
    MaterialButton retryMaterialButton;


    @BindView(R.id.view_flipper_create_ride_offer)
    ViewFlipper createRideOfferViewFlipper;

    @BindView(R.id.ll_flipper_progress_container)
    LinearLayout flipperProgressContainerLinearLayout;

    // page 1 - departure/arrival time
    @BindView(R.id.text_input_edit_text_ride_offer_departure_time)
    TextInputEditText departureTimeTextInputEditText;
    @BindView(R.id.text_input_edit_text_ride_offer_arrival_time)
    TextInputEditText arrivalTimeTextInputEditText;

    // page 2 - available spots and price
    @BindView(R.id.text_input_edit_text_ride_offer_available_spots)
    TextInputEditText availableSpotsTextInputEditText;
    @BindView(R.id.text_input_edit_text_ride_offer_price)
    TextInputEditText rideOfferPriceTextInputEditText;

    // page 3 - other options
    @BindView(R.id.switch_material_enable_pickup_location_radius_distance)
    SwitchMaterial enablePickupLocationRadiusDistanceSwitchMaterial;
    @BindView(R.id.tv_pickup_location_radius_distance)
    TextView pickupLocationRadiusDistanceTextView;
    @BindView(R.id.seek_bar_pickup_location_radius_distance)
    SeekBar pickupLocationRadiusDistanceSeekBar;

    @BindView(R.id.switch_material_enable_pickup_location_radius_time)
    SwitchMaterial enablePickupLocationRadiusTimeSwitchMaterial;
    @BindView(R.id.tv_pickup_location_radius_time)
    TextView pickupLocationRadiusTimeTextView;
    @BindView(R.id.seek_bar_pickup_location_radius_time)
    SeekBar pickupLocationRadiusTimeSeekBar;

    @BindView(R.id.switch_material_enable_end_location_radius_distance)
    SwitchMaterial enableEndLocationRadiusDistanceSwitchMaterial;
    @BindView(R.id.tv_end_location_radius_distance)
    TextView endLocationRadiusDistanceTextView;
    @BindView(R.id.seek_bar_end_location_radius_distance)
    SeekBar endLocationRadiusDistanceSeekBar;

    @BindView(R.id.switch_material_enable_end_location_radius_time)
    SwitchMaterial enableEndLocationRadiusTimeSwitchMaterial;
    @BindView(R.id.tv_end_location_radius_time)
    TextView endLocationRadiusTimeTextView;
    @BindView(R.id.seek_bar_end_location_radius_time)
    SeekBar endLocationRadiusTimeSeekBar;

    // page 4 - notes
    @BindView(R.id.text_input_edit_text_notes)
    TextInputEditText notesTextInputEditText;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("onCreate");
        mCompositeDisposable = new CompositeDisposable();
        mRideOffersActivity = (RideOffersActivity) getActivity();
        mCreateRideOfferViewModel = ViewModelProviders.of(mRideOffersActivity).get(CreateRideOfferViewModel.class);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Timber.d("onCreateView");
        View view = inflater.inflate(R.layout.fragment_create_ride_offer, container, false);
        ButterKnife.bind(this, view);
        Preconditions.checkState(flipperProgressContainerLinearLayout.getChildCount() == createRideOfferViewFlipper.getChildCount(),
                "View flipper and flipper progress container should have the same child count!");
        Preconditions.checkState(flipperPages.length == createRideOfferViewFlipper.getChildCount(),
                "flipper pages length should be equal to view flipper child count!");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Timber.d("onDestroyView");
        // reset fab visibility to initial value
        mCompositeDisposable.clear();
    }


    //region Fab
    private RideOffersActivity mRideOffersActivity;

    @Override
    protected void initFabMenu() {
        mInitialFabVisibility = mRideOffersActivity.fab.getVisibility();
        mRideOffersActivity.fab.setVisibility(View.GONE);
    }

    @Override
    protected void clearFabMenu() {
        mRideOffersActivity.fab.setVisibility(mInitialFabVisibility);
    }
    //endregion

    private void initViews() {
        nextMaterialButton.setOnClickListener(v -> flipperShowNext());
        previousMaterialButton.setOnClickListener(v -> flipperShowPrevious());
        mGestureDetector = new GestureDetector(getContext(), new CustomGestureDetector());
        createRideOfferViewFlipper.setOnTouchListener((v, event) -> {
            Timber.d("Touch event: %s", event);
            mGestureDetector.onTouchEvent(event);
            return true;
        });
        setActivePageTitle(0);
        updateFlipperProgressContainer(1, 0);

        initDateTimePickers();
        initSwitchMaterialViews();
        initSeekBarTooltips();
    }


    private void flipperShowNext() {
        int pos = createRideOfferViewFlipper.getDisplayedChild();
        if (pos == createRideOfferViewFlipper.getChildCount() - 1) {
            Timber.d("Do nothing (preventing overflow)!");
            return;
        }
        if (!handleAndValidateInputFields(pos)) {
            return;
        }
        createRideOfferViewFlipper.setInAnimation(getContext(), R.anim.left_in);
        createRideOfferViewFlipper.setOutAnimation(getContext(), R.anim.left_out);
        createRideOfferViewFlipper.showNext();
        updateFlipperProgressContainer(pos, pos + 1);
        setActivePageTitle(pos + 1);
    }

    private void flipperShowPrevious() {
        int pos = createRideOfferViewFlipper.getDisplayedChild();
        if (pos == 0) {
            Timber.d("Do nothing (preventing underflow)!");
            return;
        }
        if (!handleAndValidateInputFields(pos)) {
            return;
        }
        createRideOfferViewFlipper.setInAnimation(getContext(), R.anim.right_in);
        createRideOfferViewFlipper.setOutAnimation(getContext(), R.anim.right_out);
        createRideOfferViewFlipper.showPrevious();
        updateFlipperProgressContainer(pos, pos - 1);
        setActivePageTitle(pos - 1);
    }

    private boolean handleAndValidateInputFields(int pageIndex) {
        if (flipperPages[pageIndex] == R.string.departure_arrival_time) {
            boolean isValid = true;
            // already handled by date time picker
            if (mCreateRideOfferViewModel.getEstDepartureTime() == null) {
                departureTimeTextInputEditText.setError(getResources().getString(R.string.input_required));
                isValid = false;
            }
            if (mCreateRideOfferViewModel.getEstArrivalTime() == null) {
                arrivalTimeTextInputEditText.setError(getResources().getString(R.string.input_required));
                isValid = false;
            }

            if (isValid) {
                // departure must be at least 5 minute in the future (because of how smart contract works)
                long estDepartureNowTimeDiff = mCreateRideOfferViewModel.getEstDepartureTime() - System.currentTimeMillis();
                if (estDepartureNowTimeDiff < 5 * 60_000) {
                    departureTimeTextInputEditText.setError(getResources().getString(R.string.invalid_departure_time));
                    isValid = false;
                }
            }
            // departure time must be gt than arrival time
            if (isValid && mCreateRideOfferViewModel.getEstArrivalTime() < mCreateRideOfferViewModel.getEstDepartureTime()) {
                arrivalTimeTextInputEditText.setError(getResources().getString(R.string.invalid_arrival_time));
                isValid = false;
            }
            return isValid;

        } else if (flipperPages[pageIndex] == R.string.available_spots_and_price) {
            boolean isValid = true;
            // both fields are required
            if (availableSpotsTextInputEditText.getText().toString().isEmpty()) {
                availableSpotsTextInputEditText.setError(getResources().getString(R.string.input_required));
                isValid = false;
            } else {
                mCreateRideOfferViewModel.setFreeSpots(Integer.valueOf(availableSpotsTextInputEditText.getText().toString()));
            }
            if (rideOfferPriceTextInputEditText.getText().toString().isEmpty()) {
                rideOfferPriceTextInputEditText.setError(getResources().getString(R.string.input_required));
                isValid = false;
            } else {
                mCreateRideOfferViewModel.setPrice(Long.valueOf(rideOfferPriceTextInputEditText.getText().toString()));
            }
            return isValid;
        } else if (flipperPages[pageIndex] == R.string.other_options) {
            // no validation needed, already handled by seek bar change listener
            return true;

        } else if (flipperPages[pageIndex] == R.string.notes) {
            // no validation needed
            mCreateRideOfferViewModel.setNotes(notesTextInputEditText.getText().toString());
            return true;
        }
        return false;
    }

    private void setActivePageTitle(int position) {
        activePageTitleTextView.setText(flipperPages[position]);
    }

    private void updateFlipperProgressContainer(int prevPos, int pos) {
        ImageView prevImageView = (ImageView) flipperProgressContainerLinearLayout.getChildAt(prevPos);
        ImageView imageView = (ImageView) flipperProgressContainerLinearLayout.getChildAt(pos);
        prevImageView.clearColorFilter();
        imageView.setColorFilter(getResources().getColor(R.color.colorPrimary));
        if (pos == 0) {
            previousMaterialButton.setEnabled(false);
        } else {
            if (pos == 1) {
                previousMaterialButton.setEnabled(true);
            } else if (pos == flipperPages.length - 1) {
                nextMaterialButton.setText(R.string.finish);
                nextMaterialButton.setOnClickListener(v -> {
                    Drawable icon = getResources().getDrawable(R.drawable.ic_car);
                    icon.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
                    new MaterialAlertDialogBuilder(getContext())
                            .setTitle("Create a new ride offer")
                            .setMessage("You are about to submit a new ride offer to the main Hedera network. \n\nYou will be charged small amount of hbars " +
                                    "by Hedera Hashgraph network as a service fee for creating and hosting smart contract on the public ledger. \n\nDo you want to continue?")
                            .setPositiveButton(getString(R.string.yes), (dialog, which) -> {
                                Timber.d("Positive button clicked: which: %d", which);
                                createRideOffer();
                            })
                            .setNegativeButton(getString(R.string.no), (dialog, which) -> {
                                Timber.d("Negative button clicked: which: %d", which);
                                mRideOffersActivity.onBackPressed(); // workaround for non-working Navigation.findNavController(getView()).popBackStack()
                            })
                            .setIcon(icon)
                            .setCancelable(false)
                            .show();
                });
            } else if (pos == flipperPages.length - 2 && prevPos == pos + 1) {
                nextMaterialButton.setText(R.string.next);
                nextMaterialButton.setOnClickListener(v -> flipperShowNext());
            }
        }
    }

    private void createRideOffer() {
        createRideOfferViewFlipper.setVisibility(View.GONE);
        nextMaterialButton.setVisibility(View.GONE);
        previousMaterialButton.setVisibility(View.GONE);
        flipperProgressContainerLinearLayout.setVisibility(View.GONE);
        activePageTitleTextView.setVisibility(View.GONE);
        rideOfferStatusImageView.setVisibility(View.GONE);
        retryMaterialButton.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        progressBarContainerLinearLayout.setVisibility(View.VISIBLE);

/*
//                              replace this with real smart contract creation logic (see below)
                                mCompositeDisposable.add(Single.timer(3, TimeUnit.SECONDS).subscribe(aLong -> {
                                    progressBar.setVisibility(View.GONE);
                                    rideOfferStatusImageView.setVisibility(View.VISIBLE);
                                    // for success
                                    progressBarCaptionTextView.setText(R.string.ride_offer_created);
                                    // for failure
//                                    rideOfferStatusImageView.clearColorFilter();
//                                    rideOfferStatusImageView.setImageResource(R.drawable.ic_baseline_error_24px);
//                                    rideOfferStatusImageView.setColorFilter(getResources().getColor(R.color.warning_color), PorterDuff.Mode.SRC_ATOP);
//                                    progressBarCaptionTextView.setText(R.string.ride_offer_creation_failed);


                                }, throwable -> Timber.e(throwable, "Timer subscription failed!")));
*/

        mCompositeDisposable.add(
                deployAndInitSmartContractFlowable()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(rideOffer -> {
                            Timber.d("Ride offer successfully deployed and initialized!");
                            mCreateRideOfferViewModel.setOfferAddress(rideOffer.getContractAddress()); // TODO: 2019-08-04 driver address is not yet set in mCreateRideOfferViewModel!
                            progressBar.setVisibility(View.GONE);
                            rideOfferStatusImageView.clearColorFilter();
                            rideOfferStatusImageView.setImageResource(R.drawable.ic_baseline_check_circle_outline_24px);
                            rideOfferStatusImageView.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
                            rideOfferStatusImageView.setVisibility(View.VISIBLE);
                            progressBarCaptionTextView.setText(R.string.ride_offer_created);
                            compositeDisposable.add(Completable.timer(1, TimeUnit.SECONDS)
                                    .doFinally(() -> mRideOffersActivity.runOnUiThread(() -> mRideOffersActivity.onBackPressed())) // workaround for non-working Navigation.findNavController(getView()).popBackStack()
                                    .subscribe(() -> {
                                        Timber.d("Closing navigation graph ride offers after 1 second...");
                                    }, throwable -> {
                                        Timber.e(throwable, "Delaying 1 second failed, closing navigation graph ride offers now...");
                                    }));
                        }, throwable -> {
                            Timber.e(throwable, "Deploying ride offer failed!");
                            mRideOffersActivity.runOnUiThread(() -> {
                                progressBar.setVisibility(View.GONE);
                                rideOfferStatusImageView.clearColorFilter();
                                rideOfferStatusImageView.setImageResource(R.drawable.ic_baseline_error_24px);
                                rideOfferStatusImageView.setColorFilter(getResources().getColor(R.color.warning_color), PorterDuff.Mode.SRC_ATOP);
                                rideOfferStatusImageView.setVisibility(View.VISIBLE);
                                progressBarCaptionTextView.setText(R.string.ride_offer_creation_failed);
                                retryMaterialButton.setOnClickListener(v -> createRideOffer());
                                retryMaterialButton.setVisibility(View.VISIBLE);
                            });
                        })
        );
    }

    private void initSeekBarTooltips() {
        @SuppressWarnings("unchecked")
        Pair<TextView, SeekBar>[] tooltipSeekbarPairs = new Pair[]{
                Pair.create(pickupLocationRadiusDistanceTextView, pickupLocationRadiusDistanceSeekBar),
                Pair.create(pickupLocationRadiusTimeTextView, pickupLocationRadiusTimeSeekBar),
                Pair.create(endLocationRadiusDistanceTextView, endLocationRadiusDistanceSeekBar),
                Pair.create(endLocationRadiusTimeTextView, endLocationRadiusTimeSeekBar),
        };

        pickupLocationRadiusDistanceTextView.setText(getTooltipText(pickupLocationRadiusDistanceTextView, mCreateRideOfferViewModel.getDistanceLimitStart()));
        endLocationRadiusDistanceTextView.setText(getTooltipText(endLocationRadiusDistanceTextView, mCreateRideOfferViewModel.getDistanceLimitEnd()));
        pickupLocationRadiusTimeTextView.setText(getTooltipText(pickupLocationRadiusTimeTextView, mCreateRideOfferViewModel.getTimeLimitStart()));
        endLocationRadiusTimeTextView.setText(getTooltipText(endLocationRadiusTimeTextView, mCreateRideOfferViewModel.getTimeLimitEnd()));
        pickupLocationRadiusDistanceSeekBar.setProgress(mCreateRideOfferViewModel.getDistanceLimitStart());
        endLocationRadiusDistanceSeekBar.setProgress(mCreateRideOfferViewModel.getDistanceLimitEnd());
        pickupLocationRadiusTimeSeekBar.setProgress(mCreateRideOfferViewModel.getTimeLimitStart());
        endLocationRadiusTimeSeekBar.setProgress(mCreateRideOfferViewModel.getTimeLimitEnd());

        for (Pair<TextView, SeekBar> tooltipSeekbarPair : tooltipSeekbarPairs) {
            final LinearLayout.LayoutParams seekBarLayoutParams = (LinearLayout.LayoutParams) tooltipSeekbarPair.second.getLayoutParams();
            final int[] location = new int[2];

            tooltipSeekbarPair.second.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                private Disposable refreshTooltipDisposable;

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    final int[] progressCounter = new int[2];
                    progressCounter[0] = seekBar.getProgress();

                    refreshTooltipDisposable = Observable.interval(16, TimeUnit.MILLISECONDS) // 16 millis translates to 62,5 fps which should be high enough refresh rate
                            .map(l -> getProgress(seekBar))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(progress -> {
                                if (progressCounter[0] == progress) {
                                    progressCounter[1]++;
                                } else {
                                    progressCounter[0] = progress;
                                    progressCounter[1] = 0;
                                }
                                // tooltip will be refresh twice (mitigation because getWidth of tooltip returns wrong value in #refreshTooltip method)
                                if (progressCounter[1] < 3) {
                                    refreshTooltip(tooltipSeekbarPair.first, seekBar, progress);
                                }
                            }, throwable -> Timber.e(throwable, "refreshTooltipDisposable failed!"));
                    mCompositeDisposable.add(refreshTooltipDisposable);
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    mCompositeDisposable.remove(refreshTooltipDisposable);
                    if (refreshTooltipDisposable != null && !refreshTooltipDisposable.isDisposed()) {
                        refreshTooltipDisposable.dispose();
                        refreshTooltipDisposable = null;
                    }
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        Timber.e(e, "Thread sleep failed!");
                    }
                    int progress = getProgress(seekBar);
                    refreshTooltip(tooltipSeekbarPair.first, seekBar, progress);
                    if (seekBar.getId() == R.id.seek_bar_pickup_location_radius_time) {
                        mCreateRideOfferViewModel.setTimeLimitStart(progress);
                    } else if (seekBar.getId() == R.id.seek_bar_end_location_radius_time) {
                        mCreateRideOfferViewModel.setTimeLimitEnd(progress);
                    } else if (seekBar.getId() == R.id.seek_bar_pickup_location_radius_distance) {
                        mCreateRideOfferViewModel.setDistanceLimitStart(progress);
                    } else if (seekBar.getId() == R.id.seek_bar_end_location_radius_distance) {
                        mCreateRideOfferViewModel.setDistanceLimitEnd(progress);
                    }
                }


                private void refreshTooltip(TextView tooltip, SeekBar seekBar, int progress) {
                    tooltip.setText(getTooltipText(tooltip, progress));
                    seekBar.getLocationOnScreen(location);
                    final int seekBarX = location[0];
                    float x = seekBarX
                            + seekBar.getThumb().getBounds().left
                            + seekBarLayoutParams.leftMargin
                            + seekBar.getPaddingLeft()
                            - tooltip.getWidth() / 2f;

                    float toolTipRightX = x + tooltip.getWidth();

                    float smallestOffsetX = seekBarX + seekBar.getPaddingLeft();
                    float seekBarRightX = seekBarX - seekBar.getPaddingRight() + seekBar.getWidth();
                    if (x < smallestOffsetX) {
                        x = smallestOffsetX;
                    } else {
                        if (toolTipRightX > seekBarRightX) {
                            x += seekBarRightX - toolTipRightX; // add negative value (snap to right edge)
                        }
                    }
                    Timber.d("Width of tooltip: %d", tooltip.getWidth());
                    tooltip.setX(x);
                }
            });
        }
    }

    private int getProgress(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        if (seekBar.getId() == R.id.seek_bar_pickup_location_radius_time || seekBar.getId() == R.id.seek_bar_end_location_radius_time) {
            progress /= 15;
            progress *= 15;
        } else {
            progress /= 250;
            progress *= 250;
        }
        return progress;
    }

    private String getTooltipText(TextView tooltipTextView, int progress) {
        switch (tooltipTextView.getId()) {
            case R.id.tv_pickup_location_radius_distance:
            case R.id.tv_end_location_radius_distance:
                return String.format(Locale.GERMAN, "%.2f km", progress / 1000.0);


            case R.id.tv_pickup_location_radius_time:
            case R.id.tv_end_location_radius_time:
                return String.format(Locale.GERMAN, "%.2f min", progress / 60.0);
        }

        return "/";
    }

    private void initSwitchMaterialViews() {
        @SuppressWarnings("unchecked") Pair<SwitchMaterial, Pair<TextView, SeekBar>> switchMaterialSeekBars[] = new Pair[]{
                Pair.create(enablePickupLocationRadiusDistanceSwitchMaterial, Pair.create(pickupLocationRadiusDistanceTextView, pickupLocationRadiusDistanceSeekBar)),
                Pair.create(enablePickupLocationRadiusTimeSwitchMaterial, Pair.create(pickupLocationRadiusTimeTextView, pickupLocationRadiusTimeSeekBar)),
                Pair.create(enableEndLocationRadiusDistanceSwitchMaterial, Pair.create(endLocationRadiusDistanceTextView, endLocationRadiusDistanceSeekBar)),
                Pair.create(enableEndLocationRadiusTimeSwitchMaterial, Pair.create(endLocationRadiusTimeTextView, endLocationRadiusTimeSeekBar))
        };

        for (Pair<SwitchMaterial, Pair<TextView, SeekBar>> switchMaterialSeekBar : switchMaterialSeekBars) {
            switchMaterialSeekBar.first.setChecked(false);
            switchMaterialSeekBar.second.first.setVisibility(View.GONE);
            switchMaterialSeekBar.second.second.setVisibility(View.GONE);
            switchMaterialSeekBar.first.setOnCheckedChangeListener((buttonView, isChecked) -> {
                switchMaterialSeekBar.second.first.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                switchMaterialSeekBar.second.second.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            });
        }
    }

    private void initDateTimePickers() {
        departureTimeTextInputEditText.setInputType(InputType.TYPE_NULL);
        final ZoneOffset zoneOffset = OffsetDateTime.now().getOffset();
        departureTimeTextInputEditText.setOnTouchListener((view, motionEvent) -> {
            Timber.d("Motion event: %s", motionEvent.getAction());
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                departureTimeTextInputEditText.setError(null);
                mCompositeDisposable.add(showDatePicker(R.string.arrival_date, LocalDate.now())
                        .flatMap(localDate -> showTimePicker(R.string.arrival_time).map(localTime -> Pair.create(localDate, localTime)))
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(localDateLocalTimePair -> {
                            LocalDateTime localDateTime = LocalDateTime.of(localDateLocalTimePair.first, localDateLocalTimePair.second);
                            Timber.d("Date and time set to: %s", localDateTime.toString());
                            ((EditText) view).setText(localDateTime.toString());
                            mCreateRideOfferViewModel.setEstDepartureTime(localDateTime.toEpochSecond(zoneOffset) * 1000);
                        }, throwable -> {
                            Timber.e(throwable, "Setting departure time failed!");
                        })
                );
            }
            return false;
        });
        arrivalTimeTextInputEditText.setInputType(InputType.TYPE_NULL);
        arrivalTimeTextInputEditText.setOnTouchListener((view, motionEvent) -> {
            Timber.d("Motion event: %s", motionEvent.getAction());
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                arrivalTimeTextInputEditText.setError(null);
                mCompositeDisposable.add(showDatePicker(R.string.departure_date, LocalDate.now())
                        .flatMap(localDate -> showTimePicker(R.string.departure_time).map(localTime -> Pair.create(localDate, localTime)))
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(localDateLocalTimePair -> {
                            LocalDateTime localDateTime = LocalDateTime.of(localDateLocalTimePair.first, localDateLocalTimePair.second);
                            Timber.d("Date and time set to: %s", localDateTime.toString());
                            ((EditText) view).setText(localDateTime.toString());
                            mCreateRideOfferViewModel.setEstArrivalTime(localDateTime.toEpochSecond(zoneOffset) * 1000);
                        }, throwable -> {
                            Timber.e(throwable, "Setting arrival time failed!");
                        })
                );
            }
            return false;
        });

    }

    private Single<LocalDate> showDatePicker(@StringRes int titleResId, @NonNull final LocalDate initialDate) {
        return Single.create(emitter -> {
            DatePickerDialog dpd = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) -> {
                LocalDate localDate = LocalDate.of(year, monthOfYear + 1, dayOfMonth);
                Timber.d("Date picker set to: %s", localDate.toString());
                emitter.onSuccess(localDate);
            }, initialDate.getYear(), initialDate.getMonthValue() - 1, initialDate.getDayOfMonth());
            dpd.setOnCancelListener(dialogInterface -> emitter.onError(new Throwable("Date picker canceled!")));
            dpd.setOnDismissListener(dialogInterface -> Timber.d("Date picker dismissed!"));

            assert getContext() != null;
            assert getActivity() != null;

            Resources res = getContext().getResources();
            dpd.setVersion(DatePickerDialog.Version.VERSION_2);
            dpd.setAccentColor(res.getColor(R.color.colorPrimary));
            dpd.setCancelColor(res.getColor(R.color.colorPureWhite));
            dpd.setOkColor(res.getColor(R.color.colorPureWhite));
            dpd.setYearRange(LocalDate.now().getYear() - 3, LocalDate.now().getYear());
            dpd.showYearPickerFirst(false);
            dpd.setTitle(res.getString(titleResId));
            dpd.show(getActivity().getSupportFragmentManager(), SearchRideOffersFilterDialogFragment.class.getSimpleName());
        });
    }

    private Single<LocalTime> showTimePicker(@StringRes int titleResId) {
        return Single.create(emitter -> {
            LocalTime localTime = LocalTime.now().plusMinutes(10);
            TimePickerDialog tpd = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {
                Timber.d("Time picker - item selected!");
                emitter.onSuccess(LocalTime.of(hourOfDay, minute, second));
            }, localTime.getHour(), localTime.getMinute(), DateFormat.is24HourFormat(getContext()));

            tpd.setOnCancelListener(dialogInterface -> emitter.onError(new Throwable("Time picker cancelled!")));
            tpd.setOnDismissListener(dialogInterface -> Timber.d("Time picker dismissed!"));

            assert getContext() != null;
            assert getActivity() != null;

            Resources res = getContext().getResources();
            tpd.setVersion(TimePickerDialog.Version.VERSION_2);
            tpd.setAccentColor(res.getColor(R.color.colorPrimary));
            tpd.setCancelColor(res.getColor(R.color.colorPureWhite));
            tpd.setOkColor(res.getColor(R.color.colorPureWhite));
            tpd.setTitle(res.getString(titleResId));
            tpd.show(getActivity().getSupportFragmentManager(), SearchRideOffersFilterDialogFragment.class.getSimpleName());
        });
    }

    private Flowable<RideOffer> deployAndInitSmartContractFlowable() {
        String address = Paper.book().<String>read(Constants.RCP_SERVER_ADDRESS, null);
        Preconditions.checkNotNull(address, "Go to developer tools and set RCP_SERVER_ADDRESS first!");
        mWeb3 = Web3j.build(new HttpService(address));

        Credentials credentials = Credentials.create("dcc99b60e2f47d8512a849b66c9e814a067ebaac781595a212b38e7732cf0780"); // TODO: 2019-08-04 this is hardcoded private key for my first account (used for development, generated by ganache)
        ContractGasProvider contractGasProvider = new ContractGasProvider() {
            @Override
            public BigInteger getGasPrice(String contractFunc) {
                return getGasPrice();
            }

            @Override
            public BigInteger getGasPrice() {
                return BigInteger.valueOf(10000000000L);
            }

            @Override
            public BigInteger getGasLimit(String contractFunc) {
                return getGasLimit();
            }

            @Override
            public BigInteger getGasLimit() {
                return BigInteger.valueOf(8000000L);
            }
        };


        // get values from mCreateRideOfferViewModel
        BigInteger _vehicleType = BigInteger.valueOf(mCreateRideOfferViewModel.getVehicleType().ordinal());
        BigInteger _priceTinybars = BigInteger.valueOf(mCreateRideOfferViewModel.getPrice() * 100_000_000);
        BigInteger _freeSpots = BigInteger.valueOf(mCreateRideOfferViewModel.getFreeSpots());
        int estDepartureTime = (int) (mCreateRideOfferViewModel.getEstDepartureTime() / 1000);
        int estArrivalTime = (int) (mCreateRideOfferViewModel.getEstArrivalTime() / 1000);
        BigInteger _estDepartureTime = BigInteger.valueOf(estDepartureTime);
        BigInteger _estArrivalTime = BigInteger.valueOf(estArrivalTime);

        String lineString = mCreateRideOfferViewModel.getPath().toLineString(false).toString(); // TODO: 2019-08-17 create file and obtain fileid
        String lineStringFileId = "0.0.1234";
        try {
            lineString.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Timber.d("Path lineString length: %d\ndata: %s", lineString.length(), lineString);
        Flowable<RideOffer> deploySmartContractFlowable = RideOffer.deploy(mWeb3, credentials, contractGasProvider, _vehicleType, _priceTinybars, _freeSpots,
                _estDepartureTime, _estArrivalTime, lineStringFileId).flowable();

        return deploySmartContractFlowable.subscribeOn(Schedulers.io())
                .doOnNext(rideOffer -> Timber.d("Ride offer deployed!"))
                .flatMap(this::initCheckpoints)
                .doOnNext(rideOffer -> Timber.d("Checkpoints successfully initialized!"))
                .flatMap(this::joinAsDriverFlowable);
    }

    private Flowable<RideOffer> initCheckpoints(RideOffer rideOffer) {
        // initialize intermediate payment checkpoints (currently we only support final destination checkpoint)
        List<BigInteger> checkPoints = new ArrayList<>(1);
        List<BigInteger> checkPointPercentage = new ArrayList<>(1);
        checkPoints.add(BigInteger.valueOf(Integer.parseInt(mCreateRideOfferViewModel.getFromLocationAddress().getOsmId())));
        checkPointPercentage.add(BigInteger.valueOf(100));
        return rideOffer.initCheckpoints(checkPoints, checkPointPercentage).flowable()
                .map(transactionReceipt -> {
                    Timber.d("Transaction receipt (initCheckpoints): %s", transactionReceipt.toString());
                    Preconditions.checkState(transactionReceipt.isStatusOK());
                    return rideOffer;
                });
    }

    private Flowable<RideOffer> joinAsDriverFlowable(RideOffer rideOffer) {
        // and join the ride as a driver
        return rideOffer.advancePayment().flowable()
                .flatMap(advancePayment -> rideOffer.joinRide(BigInteger.ZERO, BigInteger.ZERO, advancePayment).flowable())
                .map(transactionReceipt -> {
                    Timber.d("Transaction receipt (joinAsDriver): %s", transactionReceipt.toString());
                    Preconditions.checkState(transactionReceipt.isStatusOK());
                    return rideOffer;
                });
    }


}
