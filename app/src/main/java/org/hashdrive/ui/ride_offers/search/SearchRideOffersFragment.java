package org.hashdrive.ui.ride_offers.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import org.hashdrive.R;
import org.hashdrive.ui.ride_offers.BaseFragment;
import org.hashdrive.ui.ride_offers.RideOffersActivity;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.kobakei.materialfabspeeddial.FabSpeedDialMenu;
import timber.log.Timber;

public class SearchRideOffersFragment extends BaseFragment {


    private RideOffersActivity mRideOffersActivity;
    private String[] mTabTitles;
    private RidesSlidePagerAdapter mPagerAdapter;

    @BindView(R.id.tab_layout_ride_offers)
    TabLayout ridesTabLayout;

    @BindView(R.id.view_pager_rides)
    ViewPager searchRidesViewPager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Timber.d("onCreateView");
        View view = inflater.inflate(R.layout.fragment_search_rides, container, false);
        ButterKnife.bind(this, view);
        mRideOffersActivity = (RideOffersActivity) getActivity();
        mTabTitles = new String[]{getString(R.string.tab_label_title_departures), getString(R.string.tab_label_title_arrivals)};
        mPagerAdapter = new RidesSlidePagerAdapter(Objects.requireNonNull(getActivity()).getSupportFragmentManager());
        ridesTabLayout.setTabsFromPagerAdapter(mPagerAdapter);
        searchRidesViewPager.setAdapter(mPagerAdapter);
        searchRidesViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(ridesTabLayout));
        ridesTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(searchRidesViewPager));
        initFabMenu();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Timber.d("onDestroyView");
        // reset main fab onClick listener
        mRideOffersActivity.fab.getMainFab().setOnClickListener(v -> {
            if (mRideOffersActivity.fab.isOpeningMenu()) {
                mRideOffersActivity.fab.closeMenu();
            } else {
                mRideOffersActivity.fab.openMenu();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPagerAdapter.clear(); // clear instantiated fragments (mandatory)
        Timber.d("onDestroy");
    }

    //region Fab
    @Override
    protected void initFabMenu() {
        mRideOffersActivity.fab.setMenu(new FabSpeedDialMenu(getContext())); // with no submenus
        mRideOffersActivity.fab.getMainFab().setImageResource(R.drawable.ic_baseline_filter_list_24px);
        mRideOffersActivity.fab.getMainFab().setOnClickListener(v -> {
            Timber.d("Main fab clicked, opening ride offer filter dialog...");
            SearchRideOffersFilterDialogFragment.display(getFragmentManager());
        });
    }

    @Override
    protected void clearFabMenu() {
        mRideOffersActivity.fab.getMainFab().setOnClickListener(null);
    }
    //endregion

    private class RidesSlidePagerAdapter extends FragmentPagerAdapter {

        private final FragmentManager mFragmentManager;

        public RidesSlidePagerAdapter(@NonNull FragmentManager fm) {
            super(fm);
            mFragmentManager = fm;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return new RideOffersContainerFragment();
        }

        @Override
        public int getCount() {
            return mTabTitles.length;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTabTitles[position];
        }

        public void clear() {
            int n = getCount();
            Timber.d("Clearing %d fragments from RidesSlidePagerAdapter...", n);

            List<Fragment> fragments = mFragmentManager.getFragments();
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            for (Fragment f : fragments) {
                //You can perform additional check to remove some (not all) fragments:
                if (f instanceof RideOffersContainerFragment) {
                    ft.remove(f);
                }
            }
            ft.commitAllowingStateLoss();
        }
    }
}
