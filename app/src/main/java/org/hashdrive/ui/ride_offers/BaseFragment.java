package org.hashdrive.ui.ride_offers;

public abstract class BaseFragment extends org.hashdrive.ui.BaseFragment {

    @Override
    public void onStart() {
        super.onStart();
        initFabMenu();
    }

    @Override
    public void onPause() {
        super.onPause();
        clearFabMenu();

    }

    protected abstract void initFabMenu();
    protected abstract void clearFabMenu();
}
