package org.hashdrive.ui.ride_offers;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import org.hashdrive.R;
import org.hashdrive.database.ride.VehicleType;
import org.hashdrive.ui.auth.AuthActivity;
import org.hashdrive.ui.mainboard.HomeFragment;
import org.hashdrive.ui.ride_offers.create.CreateRideOfferViewModel;
import org.hashdrive.ui.ride_offers.search.RideOffersContainerFragment;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.kobakei.materialfabspeeddial.FabSpeedDial;
import timber.log.Timber;


public class RideOffersActivity extends AuthActivity implements
        SimpleMapFragment.OnFragmentInteractionListener,
        RideOffersContainerFragment.OnFragmentInteractionListener,
        HomeFragment.OnFragmentInteractionListener {

    public VehicleType vehicleType;
    private CreateRideOfferViewModel mCreateRideOfferViewModel;

    @BindView(R.id.fab)
    public FabSpeedDial fab;

    @BindView(R.id.bg_fab_menu)
    public View fabMenuBgView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("onCreate");
        setContentView(R.layout.activity_ride_offers);
        ButterKnife.bind(this);
        vehicleType = RideOffersActivityArgs.fromBundle(Objects.requireNonNull(getIntent().getExtras())).getVehicleType();
        mCreateRideOfferViewModel = ViewModelProviders.of(this).get(CreateRideOfferViewModel.class);
        mCreateRideOfferViewModel.setVehicleType(vehicleType);
    }

    @Override
    public boolean onSupportNavigateUp() {
        Timber.d("onSupportNavigateUp");
        return Navigation.findNavController(this, R.id.navHostFragmentRideOffers).navigateUp();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAuthSetupSuccess() {
        Timber.d("onAuthSetupSuccess!");
    }

    @Override
    public void onAuthSetupFailed(boolean isCancelled) {
        Timber.d("onAuthSetupFailed - isCancelled: %b!", isCancelled);
        finish();

    }

    @Override
    public void onAuthSuccess(int requestCode) {
        Timber.d("onAuthSuccess - requestCode: %d!", requestCode);
    }

    @Override
    public void onAuthFailed(int requestCode, boolean isCancelled) {
        Timber.d("onAuthFailed - requestCode: %d, isCancelled: %b!", requestCode, isCancelled);
        finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

