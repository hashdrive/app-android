package org.hashdrive.ui.ride_offers.search;

import org.hashdrive.HashdriveApp;
import org.hashdrive.database.DatabaseManager;
import org.hashdrive.database.ride.Ride;
import org.hashdrive.database.ride.VehicleType;
import org.oscim.core.GeoPoint;

import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.Flowable;
import timber.log.Timber;

public class SearchRideOffersViewModel {

    private DatabaseManager mDatabaseManager;

    public SearchRideOffersViewModel() {
        Timber.d("Creating search ride offers view model instance...");
        mDatabaseManager = HashdriveApp.getInstance().createDatabaseManager();
        mDatabaseManager.getRideOffer("rideOfferAddress"); // TODO: 2019-07-18 stub create new query with filters below

    }

    // Retrieved from SimpleMapFragment (step 1/3)
    VehicleType vehicleType;
    GeoPoint fromLocation;
    GeoPoint toLocation;
    Map<String, Set<String>> countryStateDistrictsMap;

    // Retrieved from SearchRideOffersFragment (step 2/3), apply filters with two tabs (Departure/Arrivals)
    Long estDepartureFromTime;
    Long estDepartureToTime;
    Long estArrivalFromTime;
    Long estArrivalToTime;
    Integer freeSpots;
    Integer timeLimitStart;     // in minutes
    Integer distanceLimitStart; // in meters
    Integer timeLimitEnd;       // in minutes
    Integer distanceLimitEnd;   // in meters
    Long price;

    Flowable<List<Ride>> filteredRides; // TODO: 2019-07-18 should trigger mqtt data fetch for countryStateDistrictsMap and observe database changes


    // Choose ride from the list and either begin negotiating or join the ride (it will execute the tx which can either succeed or fail)
    // when negotiating, user should specify pickup & drop off location with associated offer for each
    // both operations (negotiating and joining) are bonding if executed <=24 hours before the ride (this should be set by the driver in the future)
    // you are actually sending private or sum(price, pickupPrice, dropoffPrice) hbars in case of custom pickup & drop off location to the smart contract.
    // The same is true for the driver (#free spots * price * 0.25 is set aside in order to punish bad acts - e.g.: driver don't pick up the passengers)

    // A driver Alice has 3 free spots in the car and she wants to charge 5 hbars per passenger. When submitting the offer she will need to stake 3.75 (lock it into smart contract).
    // Bob, Carmen and Dave join the ride. Dave wants to be picked up at home which is near departure location and he is willing to pay 1 hbar for this. Alice accepts the offer
    // B., C. and D. combined have 16 hbars locked inside the smart contract. If B. claims that Alice did not pick him up the dispute should be handled by Hashdrive administrators
    // at first (or random peers from the community with a high enough reputation in the future). If dispute is not sorted the funds should be released back to both, Alice and Bob.
    // Otherwise either Alice or Bob should be compensated. Small percentage will be deduced as a fee and put to the pool from where the honest dispute reviewers will be payed.


}
