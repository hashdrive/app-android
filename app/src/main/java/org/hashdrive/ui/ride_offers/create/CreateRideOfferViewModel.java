package org.hashdrive.ui.ride_offers.create;

import androidx.lifecycle.ViewModel;

import com.graphhopper.util.PointList;

import org.hashdrive.database.ride.VehicleType;
import org.oscim.core.GeoPoint;

import java.util.Map;
import java.util.Set;

import fr.dudie.nominatim.model.Address;

public class CreateRideOfferViewModel extends ViewModel {

    private VehicleType vehicleType; // set by RideOffersActivity

    // Retrieved from SimpleMapFragment (step 1/3)
    private Address fromLocationAddress;
    private Address toLocationAddress;
    private Map<String, Set<String>> countryStateDistrictsMap;
    private PointList path;

    // Retrieved from CreateRideOfferFragment (step 2/3)
    private Long price;
    private Long estDepartureTime;
    private Long estArrivalTime;
    private Integer freeSpots;
    private Integer timeLimitStart = 600;        // in seconds
    private Integer distanceLimitStart = 5000;  // in meters
    private Integer timeLimitEnd = 600;          // in seconds
    private Integer distanceLimitEnd = 5000;    // in meters
    private String notes;

    // Retrieved from RideOfferCreatedFragment (step 3/3)
    private String offerAddress;
    private String driverAddress;

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public PointList getPath() {
        return path;
    }

    public void setPath(PointList path) {
        this.path = path;
    }

    public Address getFromLocationAddress() {
        return fromLocationAddress;
    }

    public void setFromLocationAddress(Address fromLocationAddress) {
        this.fromLocationAddress = fromLocationAddress;
    }

    public Address getToLocationAddress() {
        return toLocationAddress;
    }

    public void setToLocationAddress(Address toLocationAddress) {
        this.toLocationAddress = toLocationAddress;
    }

    public Map<String, Set<String>> getCountryStateDistrictsMap() {
        return countryStateDistrictsMap;
    }

    public void setCountryStateDistrictsMap(Map<String, Set<String>> countryStateDistrictsMap) {
        this.countryStateDistrictsMap = countryStateDistrictsMap;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getEstDepartureTime() {
        return estDepartureTime;
    }

    public void setEstDepartureTime(Long estDepartureTime) {
        this.estDepartureTime = estDepartureTime;
    }

    public Long getEstArrivalTime() {
        return estArrivalTime;
    }

    public void setEstArrivalTime(Long estArrivalTime) {
        this.estArrivalTime = estArrivalTime;
    }

    public Integer getFreeSpots() {
        return freeSpots;
    }

    public void setFreeSpots(Integer freeSpots) {
        this.freeSpots = freeSpots;
    }

    public Integer getTimeLimitStart() {
        return timeLimitStart;
    }

    public void setTimeLimitStart(Integer timeLimitStart) {
        this.timeLimitStart = timeLimitStart;
    }

    public Integer getDistanceLimitStart() {
        return distanceLimitStart;
    }

    public void setDistanceLimitStart(Integer distanceLimitStart) {
        this.distanceLimitStart = distanceLimitStart;
    }

    public Integer getTimeLimitEnd() {
        return timeLimitEnd;
    }

    public void setTimeLimitEnd(Integer timeLimitEnd) {
        this.timeLimitEnd = timeLimitEnd;
    }

    public Integer getDistanceLimitEnd() {
        return distanceLimitEnd;
    }

    public void setDistanceLimitEnd(Integer distanceLimitEnd) {
        this.distanceLimitEnd = distanceLimitEnd;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getOfferAddress() {
        return offerAddress;
    }

    public void setOfferAddress(String offerAddress) {
        this.offerAddress = offerAddress;
    }

    public String getDriverAddress() {
        return driverAddress;
    }

    public void setDriverAddress(String driverAddress) {
        this.driverAddress = driverAddress;
    }
}
