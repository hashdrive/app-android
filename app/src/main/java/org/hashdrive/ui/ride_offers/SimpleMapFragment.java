package org.hashdrive.ui.ride_offers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.PathWrapper;
import com.graphhopper.isochrone.algorithm.DelaunayTriangulationIsolineBuilder;
import com.graphhopper.isochrone.algorithm.Isochrone;
import com.graphhopper.json.geo.JsonFeature;
import com.graphhopper.routing.QueryGraph;
import com.graphhopper.routing.util.DefaultEdgeFilter;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.weighting.FastestWeighting;
import com.graphhopper.storage.Graph;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.Constants;
import com.graphhopper.util.PMap;
import com.graphhopper.util.Parameters;
import com.graphhopper.util.PointList;
import com.graphhopper.util.shapes.BBox;
import com.jakewharton.rxbinding3.widget.RxSearchView;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.hashdrive.R;
import org.hashdrive.network.VolleySingleton;
import org.hashdrive.ui.development.NominatimSuggestionsAdapter;
import org.hashdrive.ui.ride_offers.create.CreateRideOfferViewModel;
import org.hashdrive.utils.FabSpeedDialUtils;
import org.hashdrive.utils.StorageHelper;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.oscim.android.MapView;
import org.oscim.android.canvas.AndroidGraphics;
import org.oscim.backend.CanvasAdapter;
import org.oscim.backend.canvas.Bitmap;
import org.oscim.core.GeoPoint;
import org.oscim.event.Gesture;
import org.oscim.event.GestureListener;
import org.oscim.event.MotionEvent;
import org.oscim.layers.Layer;
import org.oscim.layers.PathLayer;
import org.oscim.layers.marker.ItemizedLayer;
import org.oscim.layers.marker.MarkerInterface;
import org.oscim.layers.marker.MarkerItem;
import org.oscim.layers.marker.MarkerSymbol;
import org.oscim.layers.tile.buildings.BuildingLayer;
import org.oscim.layers.tile.vector.VectorTileLayer;
import org.oscim.layers.tile.vector.labeling.LabelLayer;
import org.oscim.renderer.GLViewport;
import org.oscim.scalebar.DefaultMapScaleBar;
import org.oscim.scalebar.MapScaleBar;
import org.oscim.scalebar.MapScaleBarLayer;
import org.oscim.theme.VtmThemes;
import org.oscim.tiling.TileSource;
import org.oscim.tiling.source.OkHttpEngine;
import org.oscim.tiling.source.mapfile.MapFileTileSource;
import org.oscim.tiling.source.oscimap4.OSciMap4TileSource;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.dudie.nominatim.client.JsonNominatimClient;
import fr.dudie.nominatim.model.Address;
import fr.dudie.nominatim.model.Element;
import io.github.kobakei.materialfabspeeddial.FabSpeedDialMenu;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


public class SimpleMapFragment extends BaseFragment {

    /**
     * Note that graph hopper files in location
     * sdcard/Android/data/org.hashdrive/files/Documents/Hashdrive/#MAP_BASE_NAME-gh
     * are required.
     * - true if local map file should be used as tile source
     * - false if map should be loaded from the internet (without cache support for now)
     */
    private static final boolean USE_LOCAL_MAP = false;
    private static final String MAP_BASE_NAME = "slovenia";
    private OnFragmentInteractionListener mListener;
    private RideOffersActivity mRideOffersActivity;
    private CreateRideOfferViewModel mCreateRideOfferViewModel;

    private JsonNominatimClient mNominatimClient;
    private NominatimSuggestionsAdapter mFromLocationSuggestionAdapter;
    private NominatimSuggestionsAdapter mToLocationSuggestionAdapter;
    private MapScaleBar mMapScaleBar;
    private GraphHopper hopper;
    private GeoPoint start;
    private GeoPoint end;
    private ItemizedLayer<MarkerInterface> itemizedLayer;
    private Pair<MarkerInterface, MarkerInterface> markerItems = Pair.create(null, null); // start, end
    private PathLayer pathLayer;
    private Pair<PathLayer, PathLayer> isochroneLayers = Pair.create(null, null); // start, end
    private Gson mGson = new GsonBuilder().create();
    private State mState = State.READY;

    @BindView(R.id.searchview_from_location)
    SearchView fromLocationSearchView;

    @BindView(R.id.searchview_to_location)
    SearchView toLocationSearchView;

    @OnClick(R.id.imageview_swap_locations)
    public void onSearchImageViewClicked(View view) {
        Timber.d("onSearchImageViewClicked!");
        if (mState != State.READY) {
            FabSpeedDialUtils.applySnackbarPositionFix(Snackbar.make(getView(),
                    String.format(Locale.GERMAN, getString(R.string.not_ready_try_again_later)), Snackbar.LENGTH_LONG), mRideOffersActivity.fab).show();
            return;
        }
        mState = State.SWAPPING_LOCATIONS;
        GeoPoint oldStart = start;
        start = end;
        end = oldStart;
        isochroneLayers = Pair.create(isochroneLayers.second, isochroneLayers.first);
        markerItems = Pair.create(markerItems.second, markerItems.first);
        String oldFromLocation = fromLocationSearchView.getQuery().toString();
        List<Address> oldFromLocationAddresses = mFromLocationSuggestionAdapter.getAddresses();
        fromLocationSearchView.setQuery(toLocationSearchView.getQuery(), false);
        toLocationSearchView.setQuery(oldFromLocation, false);
        mFromLocationSuggestionAdapter.setAddressList(mToLocationSuggestionAdapter.getAddresses());
        mToLocationSuggestionAdapter.setAddressList(oldFromLocationAddresses);
        fromLocationSearchView.requestFocus();
        toLocationSearchView.requestFocus();
        mapView.requestFocus();
        // update map views
        removePath();
        removeIsochroneLayers(isochroneLayers.second); // because it will be recreated in handleEndLocation
        updateMarkerItems(createMarkerItem(start, R.drawable.marker_location_start, "Start"), createMarkerItem(end, R.drawable.marker_location_end, "End"));
        mapView.map().updateMap(true);
        handleEndLocation();
    }

    @OnClick(R.id.imageview_back)
    public void onBackImageViewClicked(View view) {
        Timber.d("onBackImageViewClicked!");
        requireActivity().onBackPressed();
    }


    @BindView(R.id.mapview_container)
    MapView mapView;


    public SimpleMapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("onCreate");
        mRideOffersActivity = (RideOffersActivity) getActivity();
        mCreateRideOfferViewModel = ViewModelProviders.of(mRideOffersActivity).get(CreateRideOfferViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Timber.d("onCreateView");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_simple_map, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Timber.d("onViewCreated");
        // TODO: 2019-07-14 show loading animation ?
        initNominatim();
        initMapView(view);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Timber.d("onAttach");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Timber.d("onDetach");
        mListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        Timber.d("onPause");
        if (mapView != null) {
            mapView.onPause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Timber.d("onDestroyView");
        mRideOffersActivity.fab.removeAllOnMenuItemClickListeners();

        if (hopper != null) {
            hopper.close();
        }
        hopper = null;

        if (mMapScaleBar != null) {
            mMapScaleBar.destroy();
            mMapScaleBar = null;
        }

        mapView.onDestroy();
        mapView = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.d("onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.d("onResume");
        if (mapView != null) {
            mapView.onResume();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.d("onDestroy");
    }

    //region Fab
    @Override
    protected void initFabMenu() {
        mRideOffersActivity.fab.getMainFab().setImageResource(R.drawable.ic_action_add);
        mRideOffersActivity.fab.setMenu(new FabSpeedDialMenu(Objects.requireNonNull(getContext())));
        mRideOffersActivity.fab.inflateMenu(R.menu.ride_offers_fab);
        mRideOffersActivity.fab.addOnStateChangeListener(isOpen -> {
            Timber.d("Fab state changed: isOpened: %b", isOpen);
            mRideOffersActivity.runOnUiThread(() -> {
                if (isOpen) {
                    mRideOffersActivity.fabMenuBgView.setVisibility(View.VISIBLE);
                    mRideOffersActivity.fabMenuBgView.animate().alpha(1f).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation, boolean isReverse) {
                            Timber.d("Animation ended - fabMenuBgView is visible!");
                        }
                    }).start();
                } else {
                    mRideOffersActivity.fabMenuBgView.animate().alpha(0f).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            Timber.d("Animation ended - fabMenuBgView is no longer visible!");
                            mRideOffersActivity.fabMenuBgView.setVisibility(View.GONE);
                        }
                    }).start();
                }
            });
            return null;
        });
        mRideOffersActivity.fab.addOnMenuItemClickListener((miniFab, label, itemId) -> {
            Timber.d("Floating action button at position %d clicked!", itemId);
            if (start == null || end == null) {
                FabSpeedDialUtils.applySnackbarPositionFix(Snackbar.make(getView(),
                        String.format(Locale.GERMAN, getString(R.string.select_start_and_end_location)), Snackbar.LENGTH_LONG), mRideOffersActivity.fab).show();
                return null;
            } // TODO: 2019-08-09 validate view model properties (e.g.: it could happen that reverse geocoding failed) it that case reset start and end location and try again!

            if (mState != State.READY) {
                FabSpeedDialUtils.applySnackbarPositionFix(Snackbar.make(getView(),
                        String.format(Locale.GERMAN, getString(R.string.not_ready_try_again_later)), Snackbar.LENGTH_LONG), mRideOffersActivity.fab).show();
                return null;
            }

            if (itemId == R.id.find_ride_offers) {
                Navigation.findNavController(getView()).navigate(SimpleMapFragmentDirections.actionSimpleMapFragmentToSearchRideFragment());

            } else if (itemId == R.id.submit_ride_offer) {
                Navigation.findNavController(getView()).navigate(R.id.action_simpleMapFragment_to_createRideOfferFragment);
            }
            return null;
        });
    }

    @Override
    protected void clearFabMenu() {
        mRideOffersActivity.fab.removeAllOnMenuItemClickListeners();
        mRideOffersActivity.fab.removeAllOnStateChangeListeners();
    }
    //endregion

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void initNominatim() {
        HttpClient httpClient = new DefaultHttpClient();
        String email = "prpo.podpora@gmail.com";
        mNominatimClient = new JsonNominatimClient(httpClient, email);

        mFromLocationSuggestionAdapter = new NominatimSuggestionsAdapter(getContext(), null, 0);
        fromLocationSearchView.setSuggestionsAdapter(mFromLocationSuggestionAdapter);
        compositeDisposable.add(RxSearchView.queryTextChanges(fromLocationSearchView)
                .skipInitialValue()
                .filter(charSequence -> !charSequence.toString().trim().isEmpty() && mState == State.READY)
                .debounce(2, TimeUnit.SECONDS)
                .doOnNext(charSequence -> {
                    Timber.d("Query text changed to: %s, executing nominatim search query (from location)...", charSequence.toString());
                })
                .flatMapSingle(charSequence -> executeNominatimSearchQuery(charSequence.toString()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(addresses -> {
                    Timber.d("Swapping cursor with new one (from location)...");
                    mFromLocationSuggestionAdapter.setAddressList(addresses);
                }, throwable -> Timber.e(throwable, "Search failed (from location)!"))
        );
        fromLocationSearchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                Timber.d("From location suggestion selected at pos: %d", position);
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Timber.d("From location suggestion clicked at pos: %d", position);
                Address address = mFromLocationSuggestionAdapter.getAddresses().get(position);
                updateSearchViewQuery(fromLocationSearchView, address.getDisplayName());
                start = new GeoPoint(address.getLatitude(), address.getLongitude());
                removePath();
                removeIsochroneLayers(isochroneLayers.first);
                // update marker and handle start location
                updateMarkerItems(createMarkerItem(start, R.drawable.marker_location_start, "Start"), markerItems.second);
                mapView.map().updateMap(true);
                handleStartLocation();
                return true;
            }
        });

        mToLocationSuggestionAdapter = new NominatimSuggestionsAdapter(getContext(), null, 0);
        toLocationSearchView.setSuggestionsAdapter(mToLocationSuggestionAdapter);
        compositeDisposable.add(RxSearchView.queryTextChanges(toLocationSearchView)
                .skipInitialValue()
                .debounce(2, TimeUnit.SECONDS)
                .filter(charSequence -> !charSequence.toString().trim().isEmpty() && mState == State.READY)
                .doOnNext(charSequence -> {
                    Timber.d("Query text changed to: %s, executing nominatim search query (to location)...", charSequence.toString());
                })
                .flatMapSingle(charSequence -> executeNominatimSearchQuery(charSequence.toString()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(addresses -> {
                    Timber.d("Swapping cursor with new one (to location)...");
                    mToLocationSuggestionAdapter.setAddressList(addresses);
                }, throwable -> Timber.e(throwable, "Search failed (to location)!"))
        );
        toLocationSearchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                Timber.d("To location suggestion selected at pos: %d", position);
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Timber.d("To location suggestion clicked at pos: %d", position);
                Address address = mToLocationSuggestionAdapter.getAddresses().get(position);
                updateSearchViewQuery(toLocationSearchView, address.getDisplayName());
                end = new GeoPoint(address.getLatitude(), address.getLongitude());
                removePath();
                removeIsochroneLayers(isochroneLayers.second);
                // update marker and handle end location
                updateMarkerItems(markerItems.first, createMarkerItem(end, R.drawable.marker_location_end, "End"));
                mapView.map().updateMap(true);
                handleEndLocation();
                return true;
            }
        });

        int searchCloseButtonId = fromLocationSearchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);

        int searchEditTextId = fromLocationSearchView.getContext().getResources()
                .getIdentifier("android:id/search_src_text", null, null);

        EditText fromEditText = fromLocationSearchView.findViewById(searchEditTextId);
        EditText toEditText = toLocationSearchView.findViewById(searchEditTextId);

        ImageView fromCloseButton = fromLocationSearchView.findViewById(searchCloseButtonId);
        ImageView toCloseButton = toLocationSearchView.findViewById(searchCloseButtonId);

        fromEditText.setOnFocusChangeListener((v, hasFocus) -> {
            Timber.d("From edit text focus changed (has focus: %b)", hasFocus);
            if (!hasFocus) {
                fromEditText.setSelection(0);
            }
        });
        toEditText.setOnFocusChangeListener((v, hasFocus) -> {
            Timber.d("To edit text focus changed (has focus: %b)", hasFocus);
            if (!hasFocus) {
                toEditText.setSelection(0);
            }
        });

        fromCloseButton.setOnClickListener(v -> {
            Timber.d("From close button clicked!");
            if (mState != State.READY) {
                FabSpeedDialUtils.applySnackbarPositionFix(Snackbar.make(getView(),
                        String.format(Locale.GERMAN, getString(R.string.not_ready_try_again_later)), Snackbar.LENGTH_LONG), mRideOffersActivity.fab).show();
                return;
            }
            fromEditText.setText(null);
            mFromLocationSuggestionAdapter.setAddressList(new ArrayList<>());
            start = null;
            removePath();
            removeIsochroneLayers(isochroneLayers.first);
            updateMarkerItems(null, markerItems.second);
            mapView.map().updateMap(true);
        });
        toCloseButton.setOnClickListener(v -> {
            Timber.d("To close button clicked!");
            if (mState != State.READY) {
                FabSpeedDialUtils.applySnackbarPositionFix(Snackbar.make(getView(),
                        String.format(Locale.GERMAN, getString(R.string.not_ready_try_again_later)), Snackbar.LENGTH_LONG), mRideOffersActivity.fab).show();
                return;
            }
            toEditText.setText(null);
            mToLocationSuggestionAdapter.setAddressList(new ArrayList<>());
            end = null;
            removePath();
            removeIsochroneLayers(isochroneLayers.second);
            updateMarkerItems(markerItems.first, null);
            mapView.map().updateMap(true);
        });
    }

    @UiThread
    private void updateSearchViewQuery(SearchView searchView, String query) {
        searchView.requestFocus();
        searchView.setQuery(query, false);
        searchView.clearFocus();
    }

    private void initMapView(View view) {
        TileSource tileSource;

        // Graph hopper (needed for routing)
        File mapDir = new File(StorageHelper.getExternalMapsDir(Objects.requireNonNull(getContext())), MAP_BASE_NAME + "-gh");
        if (!mapDir.exists()) {
            FabSpeedDialUtils.applySnackbarPositionFix(Snackbar.make(view, String.format(Locale.GERMAN,
                    "Copy %s to the %s", mapDir.getName(), mapDir.getParent()), Snackbar.LENGTH_INDEFINITE), mRideOffersActivity.fab).show();
            return;
        }

        // Use proper tile source
        if (USE_LOCAL_MAP) {
            // Local map file tile source
            tileSource = new MapFileTileSource();
            File mapFile = new File(mapDir, MAP_BASE_NAME + ".map");
            if (!mapFile.exists()) {
                FabSpeedDialUtils.applySnackbarPositionFix(Snackbar.make(view, String.format(Locale.GERMAN,
                        "There is %s missing in the %s!", mapFile.getName(), mapFile.getParent()), Snackbar.LENGTH_INDEFINITE), mRideOffersActivity.fab).show();
                return;
            }
            if (!((MapFileTileSource) tileSource).setMapFile(mapFile.getAbsolutePath())) {
                FabSpeedDialUtils.applySnackbarPositionFix(Snackbar.make(view, "Could not set map file!", Snackbar.LENGTH_INDEFINITE), mRideOffersActivity.fab).show();
                return;
            }
        } else {
            // OSci map tile source
            tileSource = OSciMap4TileSource.builder()
                    .httpFactory(new OkHttpEngine.OkHttpFactory())
                    .build();
        }

        // Vector layer
        VectorTileLayer tileLayer = mapView.map().setBaseMap(tileSource);

        // Building layer
        mapView.map().layers().add(new BuildingLayer(mapView.map(), tileLayer));

        // Label layer
        mapView.map().layers().add(new LabelLayer(mapView.map(), tileLayer));

        // Render theme
        mapView.map().setTheme(VtmThemes.DEFAULT);

        // Scale bar
        mMapScaleBar = new DefaultMapScaleBar(mapView.map());
        MapScaleBarLayer mapScaleBarLayer = new MapScaleBarLayer(mapView.map(), mMapScaleBar);
        mapScaleBarLayer.getRenderer().setPosition(GLViewport.Position.BOTTOM_LEFT);
        mapScaleBarLayer.getRenderer().setOffset(5 * CanvasAdapter.getScale(), 0);
        mapView.map().layers().add(mapScaleBarLayer);

        // Adjust map position if local map is used
        if (USE_LOCAL_MAP) {
            // Map position
            GeoPoint mapCenter = ((MapFileTileSource) tileSource).getMapInfo().boundingBox.getCenterPoint();
            mapView.map().setMapPosition(mapCenter.getLatitude(), mapCenter.getLongitude(), 1 << 8);
        } else {
            GeoPoint mapCenter = new GeoPoint(46.056946, 14.505752); // Ljubljana
            mapView.map().setMapPosition(mapCenter.getLatitude(), mapCenter.getLongitude(), 1 << 8);
        }

        // Enable routing for MAP_BASE_NAME
        compositeDisposable.add(loadGraphStorage(mapDir)
                .subscribe(graphHopper -> {
                    hopper = graphHopper;
                    // Markers layer
                    itemizedLayer = new ItemizedLayer(mapView.map(), (MarkerSymbol) null);
                    mapView.map().layers().add(itemizedLayer);

                    // Map events receiver
                    mapView.map().layers().add(new MapEventsReceiver(mapView.map()));

                    FabSpeedDialUtils.applySnackbarPositionFix(Snackbar.make(view, "Finished loading graph. Long press to define where to start and end the route.", Snackbar.LENGTH_LONG), mRideOffersActivity.fab).show();

                }, throwable -> {
                    Timber.e(throwable, "An error happened while creating graph!");
                }));
    }

    private boolean onLongPress(GeoPoint p) {
        if (mState != State.READY) {
            FabSpeedDialUtils.applySnackbarPositionFix(Snackbar.make(getView(), String.format(Locale.GERMAN,
                    getString(R.string.not_ready_try_again_later)), Snackbar.LENGTH_LONG), mRideOffersActivity.fab).show();
            return false;
        }

        if (start != null && end == null) {
            end = p;
            updateMarkerItems(markerItems.first, createMarkerItem(p, R.drawable.marker_location_end, "End"));
            mapView.map().updateMap(true);
            handleEndLocation();

        } else {
            start = p;
            end = null;
            // remove routing layers
            removePath();
            removeIsochroneLayers(isochroneLayers.first, isochroneLayers.second);
            updateMarkerItems(createMarkerItem(start, R.drawable.marker_location_start, "Start"), null);
            mapView.map().updateMap(true);
            handleStartLocation();
        }
        return true;
    }

    private void updateMarkerItems(MarkerInterface start, MarkerInterface end) {
        if(markerItems.first != null) {
            removeMarkerItems(markerItems.first);
        }
        if(markerItems.second != null) {
            removeMarkerItems(markerItems.second);
        }
        if(start != null) {
            itemizedLayer.addItem(start);
        }
         if(end != null) {
            itemizedLayer.addItem(end);
        }
        markerItems = Pair.create(start, end);
    }

    private void removeIsochroneLayers(PathLayer ...layersToRemove) {
        for (PathLayer layer : layersToRemove) {
            if(layer != null) {
                mapView.map().layers().remove(layer);
            }
        }
    }

    private boolean removeMarkerItems(MarkerInterface ...markerItemsToRemove) {
        for (MarkerInterface markerItem : markerItemsToRemove) {
            if(markerItem != null) {
                itemizedLayer.removeItem(markerItem);
            }
        }
        return false;
    }

    private void removePath() {
        if(pathLayer != null) {
            mapView.map().layers().remove(pathLayer);
        }
    }

    private void handleStartLocation() {
        mState = State.REVERSE_GEOCODING_RUNNING;
        compositeDisposable.add(executeNominatimReverseGeocodingQuery(start, 18)
                .flatMap(address -> {
                    Timber.d("Geolocation reversed, start location address: %s", address.getDisplayName());
                    mCreateRideOfferViewModel.setFromLocationAddress(address);
                    mRideOffersActivity.runOnUiThread(() -> {
                        updateSearchViewQuery(fromLocationSearchView, address.getDisplayName());
                    });
                    if(mCreateRideOfferViewModel.getTimeLimitStart() > 0) {
                        mState = State.FIND_ISOCHRONE_COORDINATES_RUNNING;
                        return findIsochroneCoordinatesObservable(start.getLatitude(), start.getLongitude(), mCreateRideOfferViewModel.getTimeLimitStart(), ExploreType.TIME);
                    }
                    return Single.just(new ArrayList<JsonFeature>());

                })
                .flatMap(jsonFeatures -> {
                    if(mState == State.FIND_ISOCHRONE_COORDINATES_RUNNING) {
                        Timber.d("Isochrone coordinates (around starting point) calculated, drawing polygon...");
                        isochroneLayers = Pair.create(createPathLayer(jsonFeatures, R.color.colorAccent), isochroneLayers.second);
                        mapView.map().layers().add(isochroneLayers.first);
                        mapView.map().updateMap(true);
                    }
                    if(start != null && end != null) {
                        mState = State.SHORTEST_PATH_RUNNING;
                        return calcPathObservable(start.getLatitude(), start.getLongitude(), end.getLatitude(), end.getLongitude())
                                .doOnSuccess(this::drawPath)
                                .flatMap(pathWrapper -> createPathTagsV2(pathWrapper));//createPathTags(createGeoPointList(pathWrapper.getPoints())));
                    }
                    return Single.just(new HashMap<String, Set<String>>());
                })
                .doFinally(() -> mState = State.READY)
                .subscribeOn(Schedulers.computation())
                .subscribe(countryDistrictsMap -> {
                        if(mState == State.SHORTEST_PATH_RUNNING) {
                            onPathTagsCreated(countryDistrictsMap);
                            Timber.d("handleStartLocation finished (with path)!");
                        } else {
                            Timber.d("handleStartLocation finished (without path)!");
                        }

                    }, throwable -> {
                        Timber.e(throwable, "handleStartLocation failed!");
                        mState = State.READY; // TODO: 2019-08-09 we should actually reset start location or retry
                    }
                )
        );
    }

    private void handleEndLocation() {
        mState = State.REVERSE_GEOCODING_RUNNING;
        compositeDisposable.add(executeNominatimReverseGeocodingQuery(end, 18)
                .flatMap(address -> {
                    Timber.d("Geolocation reversed, end location address: %s", address.getDisplayName());
                    mCreateRideOfferViewModel.setToLocationAddress(address);
                    mRideOffersActivity.runOnUiThread(() -> {
                        updateSearchViewQuery(toLocationSearchView, address.getDisplayName());
                    });
                    if (mCreateRideOfferViewModel.getTimeLimitEnd() > 0) {
                        mState = State.FIND_ISOCHRONE_COORDINATES_RUNNING;
                        return findIsochroneCoordinatesObservable(end.getLatitude(), end.getLongitude(), mCreateRideOfferViewModel.getTimeLimitEnd(), ExploreType.TIME);
                    }
                    return Single.just(new ArrayList<JsonFeature>());

                })
                .flatMap(jsonFeatures -> {
                    if (mState == State.FIND_ISOCHRONE_COORDINATES_RUNNING) {
                        Timber.d("Isochrone coordinates calculated (around end point), drawing polygon...");
                        isochroneLayers = Pair.create(isochroneLayers.first, createPathLayer(jsonFeatures, R.color.colorAccent));
                        mapView.map().layers().add(isochroneLayers.second);
                        mapView.map().updateMap(true);
                    }
                    if (start != null && end != null) {
                        mState = State.SHORTEST_PATH_RUNNING;
                        return calcPathObservable(start.getLatitude(), start.getLongitude(), end.getLatitude(), end.getLongitude())
                                .doOnSuccess(this::drawPath)
                                .flatMap(pathWrapper -> createPathTagsV2(pathWrapper)); //createPathTags(createGeoPointList(pathWrapper.getPoints())));
                    }
                    return Single.just(new HashMap<String, Set<String>>());
                })
                .doFinally(() -> mState = State.READY)
                .subscribeOn(Schedulers.computation())
                .subscribe(countryDistrictsMap -> {
                            if (mState == State.SHORTEST_PATH_RUNNING) {
                                onPathTagsCreated(countryDistrictsMap);
                                Timber.d("handleEndLocation finished (with path)!");
                            } else {
                                Timber.d("handleEndLocation finished (without path)!");
                            }

                        }, throwable -> {
                            Timber.e(throwable, "handleEndLocation failed!");
                            mState = State.READY; // TODO: 2019-08-09 we should actually reset start location or retry
                        }
                ));
    }

    private void drawPath(PathWrapper pathWrapper) {
        Timber.d("Path calculated, drawing path...");
        pathLayer = createPathLayer(pathWrapper);
        mapView.map().layers().add(pathLayer);
        mapView.map().updateMap(true);
        String responseAsJson = mGson.toJson(pathWrapper.getInstructions().createJson());
        Timber.d("Graph hopper routing response: %s", responseAsJson);
        Timber.d("Path points list size: %d", pathWrapper.getPoints().size());
        Timber.d("Path points: %s", Arrays.toString(pathLayer.getPoints().toArray()));
        Timber.d("Path bbox: %s", Arrays.toString(pathWrapper.calcBBox2D().toGeoJson().toArray()));
    }

    private void onPathTagsCreated(Map<String, Set<String>> countryDistrictsMap) {
        Timber.d("#%d path tags created!", countryDistrictsMap.size());
        mCreateRideOfferViewModel.setCountryStateDistrictsMap(countryDistrictsMap);
        for (String countryCode : countryDistrictsMap.keySet()) {
            Set<String> districts = countryDistrictsMap.get(countryCode);
            assert districts != null;
            for (String district : districts) {
                Timber.d("Tag: %s/%s", countryCode, district);
            }
        }
    }

    private Single<GraphHopper> loadGraphStorage(File mapDir) {
        Timber.d("loading graph (%s) ... ", Constants.VERSION);
        return Single.create(emitter -> {
            GraphHopper tmpHopp = new GraphHopper().forMobile();
            tmpHopp.load(mapDir.getAbsolutePath());
            Timber.d("found graph %s, nodes:%d", tmpHopp.getGraphHopperStorage().toString(), tmpHopp.getGraphHopperStorage().getNodes());
            emitter.onSuccess(tmpHopp);
        });
    }

    private PathLayer createPathLayer(PathWrapper response) {
        PointList pointList = response.getPoints();
        mCreateRideOfferViewModel.setPath(pointList);

        PathLayer pathLayer = new PathLayer(mapView.map(), getResources().getColor(R.color.navigation_path), 8);
        List<GeoPoint> geoPoints = createGeoPointList(pointList);

        pathLayer.setPoints(geoPoints);
        return pathLayer;
    }

    private List<GeoPoint> createGeoPointList(PointList pointList) {
        List<GeoPoint> geoPoints = new ArrayList<>();
        for (int i = 0; i < pointList.getSize(); i++) {
            geoPoints.add(new GeoPoint(pointList.getLatitude(i), pointList.getLongitude(i)));
        }
        return geoPoints;
    }

    private PathLayer createPathLayer(ArrayList<JsonFeature> jsonFeatures, @ColorRes int colorResId) {
        PathLayer pathLayer = new PathLayer(mapView.map(), getResources().getColor(colorResId), 4);
        List<GeoPoint> geoPoints = new ArrayList<>();
        for (JsonFeature jsonFeature : jsonFeatures) {
            Coordinate[] coordinates = jsonFeature.getGeometry().getCoordinates();
            Timber.d("Geometry coordinates: %d", coordinates.length);
            for (Coordinate coordinate : coordinates) {
                geoPoints.add(new GeoPoint(coordinate.y, coordinate.x));
            }
        }
        pathLayer.setPoints(geoPoints);
        return pathLayer;
    }

    @SuppressWarnings("deprecation")
    private MarkerItem createMarkerItem(GeoPoint geoPoint, @DrawableRes int markerResId, @Nullable String markerText) {
        String title = markerText == null ? "" : markerText;
        MarkerItem markerItem = new MarkerItem(title, "", geoPoint);
        markerItem.setMarker(createMarkerSymbol(markerResId));
        return markerItem;
    }

    private MarkerSymbol createMarkerSymbol(@DrawableRes int markerResId) {
        Bitmap bitmap = AndroidGraphics.drawableToBitmap(getResources().getDrawable(markerResId));
        return new MarkerSymbol(bitmap, 0.5f, 1);
    }

    public Single<PathWrapper> calcPathObservable(final double fromLat, final double fromLon,
                                                  final double toLat, final double toLon) {
        return Single.create(emitter -> {
            Timber.d("calculating path ... (thread: %s)", Thread.currentThread());
            GHRequest req = new GHRequest(fromLat, fromLon, toLat, toLon).
                    setAlgorithm(Parameters.Algorithms.DIJKSTRA_BI);
            req.getHints().
                    put(Parameters.Routing.INSTRUCTIONS, "true");
            GHResponse resp = hopper.route(req);
            emitter.onSuccess(resp.getBest()); // returns only one/best path
        });
    }

    /**
     * Used for high level filtering of orders/offers (two-level hierarchy: country_code, state_district is currently supported)
     */
    private Single<Map<String, Set<String>>> createPathTags(List<GeoPoint> geoPoints) {
        int incrementStep = (int) (geoPoints.size() * 0.1);
        List<Single<Address>> observables = new ArrayList<>();
        for (int i = 0; i < geoPoints.size(); i += incrementStep) {
            GeoPoint p = geoPoints.get(i);
            observables.add(executeNominatimReverseGeocodingQuery(p, 8));
        }
        List<Address> addresses = new ArrayList<>();
        return Single.concat(observables)
                .doOnNext(address -> Timber.d("Adding new address to the list..."))
                .collect(() -> {
                    Timber.d("Collect called!");
                    return addresses;
                }, List::add)
                .map(this::addressesToCountryStateDistrictMap);
    }

    // TODO: 2019-08-17 this is temporary fix for maps like Slovenia
    private Single<Map<String, Set<String>>> createPathTagsV2(PathWrapper pathWrapper) {
        BBox bbox = pathWrapper.calcBBox2D();
        return getPathOsmIds(bbox)
                .observeOn(Schedulers.io())
                .map(integers -> {
                    int n = integers.size();
                    List<String> lookupFilters = new ArrayList<>(n);
                    for (int i = 0; i < n; i++) {
                        lookupFilters.add("R" + integers.get(i));
                    }
//                    https://nominatim.openstreetmap.org/lookup?osm_ids=R541729,R1703400,R1703401,R1703402,R1703668,R1703687&format=json
                    return addressesToCountryStateDistrictMap(mNominatimClient.lookupAddress(lookupFilters));
                });

    }

    private Map<String, Set<String>> addressesToCountryStateDistrictMap(List<Address> addressList) {
        Map<String, Set<String>> countryDistrictsMap = new HashMap<>();
        for (Address address : addressList) {
            String countryCode = null;
            String stateDistrict = null;
            for (Element addressElement : address.getAddressElements()) {
                if (addressElement.getKey().equals("country_code")) {
                    countryCode = addressElement.getValue();
                } else if (addressElement.getKey().equals("state_district")) {
                    stateDistrict = addressElement.getValue();
                } else if (addressElement.getKey().equals("address29")) {
                    stateDistrict = addressElement.getValue();
                }
            }
            if (countryCode != null) {
                Set<String> districts = countryDistrictsMap.get(countryCode);
                if (districts == null) {
                    districts = new HashSet<>();
                    countryDistrictsMap.put(countryCode, districts);
                }

                if(stateDistrict != null) {
                    districts.add(stateDistrict);
                } // otherwise we should ask user to manually enter state districts when ride offer is successfully published
            }
        }
        return countryDistrictsMap;
    }

    /**
     * Used to retrieve state district OSM ids within the given bounding box
     * @param pathBBox
     * @return list of OSM relation ids
     */
    private Single<List<Integer>> getPathOsmIds(BBox pathBBox) {
        return Single.create(emitter -> {
            String overpassQuery = String.format(Locale.ENGLISH,"[out:csv(::\"id\")];(relation[\"admin_level\"=\"5\"](%f,%f,%f,%f););out tags;",
                    pathBBox.minLat, pathBBox.minLon, pathBBox.maxLat, pathBBox.maxLon);
            String url = "http://overpass-api.de/api/interpreter?data=" + overpassQuery;

            StringRequest request = new StringRequest(Request.Method.GET, url, response -> {
                Timber.d("createPathTagsV2 response: %s", response);
                String[] rows = response.split("\n");
                List<Integer> ids = new ArrayList<>();
                for (int i = 1; i < rows.length; i++) {
                    ids.add(Integer.parseInt(rows[i]));
                }
                emitter.onSuccess(ids);
            }, error -> {
                Timber.d(error, "createPathTagsV2 request failed!");
                emitter.onError(error);
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=" + super.getParamsEncoding();
                }
            };
            VolleySingleton.getInstance(getContext().getApplicationContext()).addToRequestQueue(request);
            emitter.setCancellable(request::cancel);
        });
    }

    /**
     * Used to find polygon (made of coordinates) around point of interest (fromLat, fromLon)
     * Note: coordinates are encoded within JsonFeature's geometry object
     *
     * @param fromLat
     * @param fromLon
     * @param limit       - either distance in meters or time in seconds
     * @param exploreType
     * @return
     */
    public Single<ArrayList<JsonFeature>> findIsochroneCoordinatesObservable(final double fromLat, final double fromLon,
                                                                             final int limit, ExploreType exploreType) {
        return Single.create(emitter -> {
            Timber.d("calculating isochrone cooridnates... (thread: %s)", Thread.currentThread());

            // get encoder from GraphHopper instance
            EncodingManager encodingManager = hopper.getEncodingManager();
            FlagEncoder encoder = encodingManager.getEncoder("car");

            // pick the closest point on the graph to the query point and generate a query graph
            QueryResult qr = hopper.getLocationIndex().findClosest(fromLat, fromLon, DefaultEdgeFilter.allEdges(encoder));

            Graph graph = hopper.getGraphHopperStorage();
            QueryGraph queryGraph = new QueryGraph(graph);
            queryGraph.lookup(Collections.singletonList(qr));

            // calculate isochrone from query graph
            PMap pMap = new PMap();
            Isochrone isochrone = new Isochrone(queryGraph, new FastestWeighting(encoder, pMap), false);

            if (exploreType == ExploreType.TIME) {
                Timber.d("Setting isochrone limit to %d min %d seconds...", limit / 60, limit % 60);
                isochrone.setTimeLimit(limit);
            } else {
                Timber.d("Setting isochrone limit to %d km %d meters...", limit / 1000, limit % 1000);
                isochrone.setDistanceLimit(limit);
            }

            List<List<Coordinate>> buckets = isochrone.searchGPS(qr.getClosestNode(), 1);
            Timber.d("Number of isochrone results: %d", buckets.size());

            DelaunayTriangulationIsolineBuilder delaunayTriangulationIsolineBuilder = new DelaunayTriangulationIsolineBuilder();
            GeometryFactory geometryFactory = new GeometryFactory();

            ArrayList<JsonFeature> features = new ArrayList<>();

            List<Coordinate[]> polygonShells = delaunayTriangulationIsolineBuilder.calcList(buckets, buckets.size() - 1);
            for (Coordinate[] polygonShell : polygonShells) {
                JsonFeature feature = new JsonFeature();
                HashMap<String, Object> properties = new HashMap<>();
                properties.put("bucket", features.size());
                feature.setProperties(properties);
                feature.setGeometry(geometryFactory.createPolygon(polygonShell));
                features.add(feature);
            }

            emitter.onSuccess(features); // returns only first isochrone result
        });
    }

    /**
     * Search for results with the given query.
     *
     * @param query
     */
    private Single<List<Address>> executeNominatimSearchQuery(@NonNull final String query) {
        return Single.<List<Address>>create(emitter -> {
            try {
                List<Address> resp = mNominatimClient.search(query);
                emitter.onSuccess(resp);
            } catch (IOException e) {
                emitter.onError(e);
            }
        }).subscribeOn(Schedulers.io());
    }

    /**
     * Reverse geocoding generates an address from a latitude and longitude.
     *
     * @param p point of interest
     */
    private Single<Address> executeNominatimReverseGeocodingQuery(@NonNull final GeoPoint p, final int zoom) {
        return Single.<Address>create(emitter -> {
            try {
                Address address = mNominatimClient.getAddress(p.getLongitude(), p.getLatitude(), zoom);
                emitter.onSuccess(address);
            } catch (IOException e) {
                emitter.onError(e);
            }
        }).subscribeOn(Schedulers.io());
    }

    class MapEventsReceiver extends Layer implements GestureListener {

        MapEventsReceiver(org.oscim.map.Map map) {
            super(map);
        }

        @Override
        public boolean onGesture(Gesture g, MotionEvent e) {
            if (g instanceof Gesture.LongPress) {
                GeoPoint p = mMap.viewport().fromScreenPoint(e.getX(), e.getY());
                return onLongPress(p);
            }
            return false;
        }
    }

    private enum ExploreType {
        TIME,       // in seconds
        DISTANCE    // in meters
    }

    private enum State {
        READY,
        SHORTEST_PATH_RUNNING,
        FIND_ISOCHRONE_COORDINATES_RUNNING,
        REVERSE_GEOCODING_RUNNING,
        SWAPPING_LOCATIONS,
    }
}
