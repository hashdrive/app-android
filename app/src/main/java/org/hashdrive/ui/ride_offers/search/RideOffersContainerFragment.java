package org.hashdrive.ui.ride_offers.search;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.hashdrive.R;
import org.hashdrive.database.Converters;
import org.hashdrive.database.ride.Ride;
import org.hashdrive.mqtt.RideOfferOuterClass;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.PrecisionModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class RideOffersContainerFragment extends Fragment {

    private RideOffersContainerFragment.OnFragmentInteractionListener mListener;

    @BindView(R.id.list_view_ride_offers)
    ListView rideOffersListView;

    public RideOffersContainerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Timber.d("onCreateView");
        View view = inflater.inflate(R.layout.fragment_ride_offers_container, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Timber.d("onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        // TODO: 2019-07-20 replace stub list with actual result returned by query
        List<Ride> list = new ArrayList<>();
        LineString stubStringLine = new LineString(new Coordinate[] { new Coordinate(1,3,4),  new Coordinate(1,6,4)}, new PrecisionModel(), 0);
        for (int i = 0; i < 15; i++) {
            Ride ride = Ride.createRide(RideOfferOuterClass.RideOffer
                    .newBuilder()
                    .setPrice(i+10)
                    .setLineStringWKT(Converters.fromLineStringToString(stubStringLine))
                    .setFreeSpots(i%4+1)
                    .build(), null);
            list.add(ride);
        }
        rideOffersListView.setAdapter(new RideOffersListAdapter(getContext(), list));
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        Timber.d("onAttach");
        super.onAttach(context);
        if (context instanceof RideOffersContainerFragment.OnFragmentInteractionListener) {
            mListener = (RideOffersContainerFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Timber.d("onDetach");
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    class RideOffersListAdapter extends ArrayAdapter<Ride> {

        private List<Ride> mRides;

        public RideOffersListAdapter(@NonNull Context context, List<Ride> rides) {
            super(context, R.layout.layout_ride_offer_list_item, rides);
            mRides = rides;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_ride_offer_list_item, parent, false);
            }

            ItemViewHolder viewHolder = new ItemViewHolder(convertView);
            Ride ride = getItem(position);

            // TODO: 2019-07-20 set departureTimeTextView and arrivalTimeTextView when from and to location attributes are added to the Ride entity
            viewHolder.priceTextView.setText(ride.getPrice() + "hbar");
            viewHolder.freeSpotsTextView.setText(ride.getFreeSpots() + "");
            return convertView;
        }

        @Nullable
        @Override
        public Ride getItem(int position) {
            return mRides.get(position);
        }

        @Override
        public int getPosition(@Nullable Ride item) {
            return mRides.indexOf(item);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ItemViewHolder {
            @BindView(R.id.tv_ride_offer_departure_time)
            TextView departureTimeTextView;

            @BindView(R.id.tv_ride_offer_arrival_time)
            TextView arrivalTimeTextView;

            @BindView(R.id.tv_ride_offer_price)
            TextView priceTextView;

            @BindView(R.id.tv_ride_offer_free_spots)
            TextView freeSpotsTextView;

            public ItemViewHolder(View convertView) {
                ButterKnife.bind(this, convertView);
            }
        }
    }


}
