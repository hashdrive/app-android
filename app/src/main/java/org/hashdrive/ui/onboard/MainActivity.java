package org.hashdrive.ui.onboard;

import android.net.Uri;
import android.os.Bundle;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.lifecycle.ViewModelStoreOwner;

import org.hashdrive.R;
import org.hashdrive.ui.auth.AuthActivity;
import org.hashdrive.ui.auth.LoginSessionViewModel;
import org.hashdrive.ui.splashboard.SplashFragment;

import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends AuthActivity implements ViewModelStoreOwner,
        CreateImportFragment.OnFragmentInteractionListener,
        SplashFragment.OnFragmentInteractionListener {

    private LoginSessionViewModel mLoginSessionViewModel;
    private MutableLiveData<LoginSessionViewModel.AuthenticationState> mAuthenticationState;
    private boolean mDidPause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("onCreate");
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupLoginSessionViewModel();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mDidPause = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mDidPause) {
            mLoginSessionViewModel.initAuthenticationState();
        }
    }

    private void setupLoginSessionViewModel() {
        mLoginSessionViewModel = ViewModelProviders.of(this).get(LoginSessionViewModel.class);
        mAuthenticationState = mLoginSessionViewModel.getAuthenticationState();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onAuthSetupSuccess() {
        Timber.d("onAuthSetupSuccess!");
        mAuthenticationState.setValue(LoginSessionViewModel.AuthenticationState.UNAUTHENTICATED);
    }

    @Override
    public void onAuthSetupFailed(boolean isCancelled) {
        Timber.d("onAuthSetupFailed - isCancelled: %b!", isCancelled);
        mAuthenticationState.setValue(LoginSessionViewModel.AuthenticationState.SETUP_FAILED);

    }

    @Override
    public void onAuthSuccess(int requestCode) {
        Timber.d("onAuthSuccess - requestCode: %d!", requestCode);
        mAuthenticationState.setValue(LoginSessionViewModel.AuthenticationState.AUTHENTICATED);
    }

    @Override
    public void onAuthFailed(int requestCode, boolean isCancelled) {
        Timber.d("onAuthFailed - requestCode: %d, isCancelled: %b!", requestCode, isCancelled);
        mAuthenticationState.setValue(LoginSessionViewModel.AuthenticationState.AUTHENTICATION_FAILED);
    }
}
