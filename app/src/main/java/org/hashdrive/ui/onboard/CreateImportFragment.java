package org.hashdrive.ui.onboard;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.card.MaterialCardView;

import org.hashdrive.R;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class CreateImportFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.card_view_type_motor_cycle) MaterialCardView mCreateWallet;
    @BindView(R.id.material_card_view_import_wallet) MaterialCardView mImportWallet;

    public CreateImportFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_import, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.card_view_type_motor_cycle)
    public void createWallet() {
        Timber.d("create Wallet");
        Navigation.findNavController(Objects.requireNonNull(getView())).navigate(CreateImportFragmentDirections.actionCreateImportFragmentToCreateWalletActivity());
    }

    @OnClick(R.id.material_card_view_import_wallet)
    public void importWallet() {
        Timber.d("import Wallet");
        Navigation.findNavController(Objects.requireNonNull(getView())).navigate(CreateImportFragmentDirections.actionCreateImportFragmentToImportWalletActivity());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()+ " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
