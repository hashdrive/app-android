package org.hashdrive.ui.splashboard;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;

import org.hashdrive.R;
import org.hashdrive.local_auth.AuthManager;
import org.hashdrive.ui.BaseFragment;
import org.hashdrive.ui.auth.LoginSessionViewModel;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import timber.log.Timber;


/**
 *
 * Activities that contain this fragment must implement the
 * {@link SplashFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class SplashFragment extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    private boolean mNavigatingInProgress;
    private final AuthManager.AuthType mPreferredAuthMethod = AuthManager.AuthType.PIN;  // TODO: 2019-07-02 this should be set by the user in security preferences

    public SplashFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    protected void handleLoginSessionAuthState(LoginSessionViewModel.AuthenticationState authenticationState) {
        switch (authenticationState) {
            case NOT_SETUP:
                Timber.d("Authentication method is not setup - executing setupAuth...");
                mainActivity.setupAuth(mPreferredAuthMethod);
                break;
            case SETUP_FAILED:
                Timber.e("Setup authentication method failed - retrying...");
                mainActivity.setupAuth(mPreferredAuthMethod);
                break;
            case UNAUTHENTICATED:
            case AUTHENTICATION_FAILED:
                Timber.d("User is authenticated - executing requestAuth...");
                mainActivity.requestAuth();
                break;
            case AUTHENTICATED:
                Timber.d("User authenticated, navigating to the home screen in 2 seconds...");
                if(!mNavigatingInProgress) {
                    mNavigatingInProgress = true;
                    compositeDisposable.add(Single.timer(2, TimeUnit.SECONDS).subscribe(l -> {
                        Navigation.findNavController(Objects.requireNonNull(getView())).navigate(SplashFragmentDirections.actionSplashFragmentToMainNavigationActivity());
                    }, throwable -> Timber.e(throwable, "Delayed navigation failed!")));
                    break;
                }
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
