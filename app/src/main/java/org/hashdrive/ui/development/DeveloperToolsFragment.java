package org.hashdrive.ui.development;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.hashdrive.BuildConfig;
import org.hashdrive.Constants;
import org.hashdrive.R;
import org.hashdrive.hederahg.Client;
import org.hashdrive.hederahg.account.AccountId;
import org.hashdrive.hederahg.crypto.ed25519.Ed25519PrivateKey;
import org.hashdrive.ui.BaseFragment;
import org.hashdrive.utils.AssetsLoader;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.paperdb.Paper;
import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class DeveloperToolsFragment extends BaseFragment {


    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private Web3j mWeb3;
    private DeveloperToolsActivity mDeveloperToolsActivity;
    private AccountId accountId; // used for better efficiency instead of account's public key
    private Ed25519PrivateKey privateKey; // needed for signing the transactions (can also be used to retrieve account's public key
    private Client mClient; // API for interacting with the Hedera Hashgraph network

    @BindView(R.id.text_input_edit_text_rpc_server)
    TextInputEditText rpcServerEditText;

    @OnClick(R.id.btn_get_client_version)
    public void onGetClientVersionButtonClicked(View view) {
        Flowable<Web3ClientVersion> clientVersionFlowable = mWeb3.web3ClientVersion().flowable();
        mCompositeDisposable.add(clientVersionFlowable.subscribeOn(Schedulers.io()).subscribe(web3ClientVersion -> {
                    Timber.d("web3ClientVersion is: %s", web3ClientVersion.getWeb3ClientVersion());
                    Snackbar.make(view, web3ClientVersion.getWeb3ClientVersion(), Snackbar.LENGTH_LONG).show();
                }, throwable -> Timber.e(throwable, "Getting web3ClientVersion failed!"))
        );
    }

    @OnClick(R.id.btn_create_new_account)
    public void onCreateNewAccountButtonClicked(View view) {
        Snackbar.make(view, "Open logcat.", Snackbar.LENGTH_SHORT).show();
        Ed25519PrivateKey newAccountPk = Ed25519PrivateKey.generate();
        Timber.d("New public private keypair generated (public key: %s, private key: %s)!", newAccountPk.getPublicKey().toString(), newAccountPk.toString());
        mCompositeDisposable.add(mClient.createAccountObservable(newAccountPk.getPublicKey(), 0)
                .subscribe(accountNumber -> Timber.d("Account created with number: %d", accountNumber),
                        throwable -> Timber.e(throwable, "Account creation failed!")
                )
        );
    }

    @OnClick(R.id.btn_get_account_balance)
    public void onGetAccountBalanceButtonClicked(View view) {
        Snackbar.make(view, "Open logcat.", Snackbar.LENGTH_SHORT).show();
        mCompositeDisposable.add(mClient.getAccountBalanceObservable(AccountId.fromString("0.0.20595"))
                .subscribe(balance -> Timber.d("Account (%s) balance is: %d", accountId.toString(), balance),
                        throwable -> Timber.e(throwable, "Account balance retrieval failed!")
                )
        );
    }

    @OnClick(R.id.btn_get_account_info)
    public void onGetAccountInfoButtonClicked(View view) {
        Snackbar.make(view, "Open logcat.", Snackbar.LENGTH_SHORT).show();
        mCompositeDisposable.add(mClient.getAccountInfoObservable(AccountId.fromString("0.0.20595"))
                .subscribe(accountInfo -> Timber.d("Account info retrieved: %s", accountInfo.toString()),
                        throwable -> Timber.e(throwable, "Retrieving account info failed!")
                )
        );
    }

    @OnClick(R.id.btn_transfer_crypto)
    public void onTransferCryptoButtonClicked(View view) {
        Snackbar.make(view, "Open logcat.", Snackbar.LENGTH_SHORT).show();
        mCompositeDisposable.add(mClient.transferCryptoObservable(AccountId.fromString("0.0.20610"), 1) // pk: 302e020100300506032b657004220420e2a43855668eff445006c09ec418468d59d22e96097b525cdff1340d1b070c8a
                .subscribe(transactionReceipt -> Timber.d("Transaction receipt retrieved with status: %s", transactionReceipt.getStatusValue()),
                        throwable -> Timber.e(throwable, "Retrieving transaction receipt failed!")
                )
        );
    }

    @OnClick(R.id.btn_mqtt)
    public void onMqttButtonClicked(View view) {
        Navigation.findNavController(Objects.requireNonNull(getView())).navigate(DeveloperToolsFragmentDirections.actionDeveloperToolsFragmentToMqttTestFragment());

    }

    @OnClick(R.id.btn_open_events_calendar)
    public void onOpenEventsCalendarClicked(View view) {
        Navigation.findNavController(Objects.requireNonNull(getView())).navigate(DeveloperToolsFragmentDirections.actionDeveloperToolsFragmentToEventsCalendarFragment());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_developer_tools, container, false);
        ButterKnife.bind(this, view);
        mDeveloperToolsActivity = (DeveloperToolsActivity) getActivity();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String rcpServerAddress = Paper.book().<String>read(Constants.RCP_SERVER_ADDRESS, "HTTP://192.168.25.105:7545");
        rpcServerEditText.setText(rcpServerAddress);
        initWeb3();
        initHH();
        initFabClickListener();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCompositeDisposable.clear();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void initWeb3 () {
        if (mWeb3 != null) {
            mWeb3.shutdown();
        }
        mWeb3 = Web3j.build(new HttpService(Paper.book().<String>read(Constants.RCP_SERVER_ADDRESS, "HTTP://192.168.25.105:7545")));
    }

    private void initHH () {
        String fileName = BuildConfig.USE_TESTNET ? "testnet_nodes.json" : "mainnet_nodes.json";
        Timber.d("Loading nodes from %s, please wait...", fileName);
        String jsonAsString = AssetsLoader.loadJSONFromAsset(fileName, getContext());
        Gson gson = new GsonBuilder().create();

        Type listType = new TypeToken<List<NodeDetails>>() {
        }.getType();
        List<NodeDetails> nodes = gson.fromJson(jsonAsString, listType);

        ImmutableMap.Builder<AccountId, String> nodesBuilder = ImmutableMap.builder();
        for (NodeDetails node : nodes) {
            Timber.d(node.toString());
            nodesBuilder.put(AccountId.fromString(node.getAccountId()), node.getAddress());
        }

        mClient = new Client(nodesBuilder.build());
        accountId = new AccountId(0, 0, 20595);
        privateKey = Ed25519PrivateKey.fromString("302e020100300506032b657004220420677570c53c7d8e9163fb5dd67d3a340f8cec6b50f6ee2feec64ebe76c52d588a");
        mClient.setOperator(accountId, privateKey);
        Timber.d("Account public (%s) private (%s) key!", privateKey.getPublicKey().toString(), privateKey.toString());
    }

    private void initFabClickListener() {
        mDeveloperToolsActivity.fab.setOnClickListener(v -> {
            Snackbar.make(v, "RCP server saved!", Snackbar.LENGTH_LONG).show();
            Timber.d("RPC server edit text: %s", rpcServerEditText.getText());
            Paper.book().write(Constants.RCP_SERVER_ADDRESS, Objects.requireNonNull(rpcServerEditText.getText()).toString());
            initWeb3();
        });
    }

    public class NodeDetails {

        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("accountId")
        @Expose
        private String accountId;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        @Override
        public String toString() {
            return "NodeDetails{" +
                    "address='" + address + '\'' +
                    ", accountId='" + accountId + '\'' +
                    '}';
        }
    }
}



