package org.hashdrive.ui.development;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.hashdrive.database.ride.Ride;

import java.util.List;
import java.util.Locale;

public class RideOffersListAdapter extends ArrayAdapter<Ride> {

    private List<Ride> mRides;

    public RideOffersListAdapter(@NonNull Context context, List<Ride> rides) {
        super(context, android.R.layout.simple_list_item_2, rides);
        mRides = rides;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_2, parent, false);
        }
        TextView tv1 = convertView.findViewById(android.R.id.text1);
        TextView tv2 = convertView.findViewById(android.R.id.text2);
        Ride ride = mRides.get(position);
        String offerAddress = ride.getOfferAddress();
        String offerAddressLastDigits = ((offerAddress.length() >= 4) ? offerAddress.substring(0, 3) : offerAddress);
        tv1.setText(String.format(Locale.GERMAN,"Vehicle: %s, free spots: %d, offer address: ...%s", ride.getVehicleType().name(), ride.getFreeSpots(), offerAddressLastDigits));
        tv2.setText(String.format(Locale.GERMAN,"%d tinnybars, %s (%s)", ride.getPrice(), ride.getStateDistrict(), ride.getCountryCode()));
        return convertView;
    }

    @Nullable
    @Override
    public Ride getItem(int position) {
        return mRides.get(position);
    }

    @Override
    public int getPosition(@Nullable Ride item) {
        return mRides.indexOf(item);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}
