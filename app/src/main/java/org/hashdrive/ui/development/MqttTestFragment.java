package org.hashdrive.ui.development;

import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.material.snackbar.Snackbar;
import com.hivemq.client.mqtt.MqttClientState;

import org.hashdrive.HashdriveApp;
import org.hashdrive.R;
import org.hashdrive.crypto.CryptoUtils;
import org.hashdrive.crypto.EDKeyPair;
import org.hashdrive.crypto.HGCSeed;
import org.hashdrive.crypto.KeyPair;
import org.hashdrive.database.Converters;
import org.hashdrive.database.DatabaseManager;
import org.hashdrive.database.ride.Ride;
import org.hashdrive.mqtt.MqttManager;
import org.hashdrive.mqtt.RideOfferOuterClass;
import org.hashdrive.ui.BaseFragment;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.PrecisionModel;
import org.threeten.bp.Instant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemLongClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class MqttTestFragment extends BaseFragment {

    private static final String MQTT_BROKER_HOST = "192.168.25.105"; // or use public broker (e.g.: broker.hivemq.com)

    private MqttManager mMqttClient;
    private DatabaseManager mDatabaseManager;
    private Disposable mMqttRideOffersSubscription;
    private Disposable mDbRideOffersSubscription;
    private RideOffersListAdapter mRideOffersListAdapter;
    private DeveloperToolsActivity mDeveloperToolsActivity;

    @BindView(R.id.btn_toggle_connect)
    Button toggleConnectButton;     // toggle connect/disconnect

    @BindView(R.id.btn_publish_message)
    Button publishMessageButton;    // toggle publish/delete

    @BindView(R.id.listview_offers)
    ListView offersListView;

    @OnItemLongClick(R.id.listview_offers)
    public void onRideOfferItemLongClicked(AdapterView<?> parent, View view, int position, long id) {
        Ride item = mRideOffersListAdapter.getItem(position);
        // only necessary fields were set
        mMqttClient.removeRetainedOffer(RideOfferOuterClass.RideOffer.newBuilder()
                .setVehicleType(RideOfferOuterClass.VehicleType.valueOf(item.getVehicleType().name()))
                .setCountryCode(item.getCountryCode())
                .setStateDistrict(item.getStateDistrict())
                .setOfferAddress(item.getOfferAddress())
                .build(), item.getSignature());
        mRideOffersListAdapter.remove(item);
    }

    @OnClick(R.id.btn_toggle_connect)
    public void onToggleConnectClicked(View view) {
        Timber.d("onToggleConnectClicked");
        if(mMqttClient.getState() == MqttClientState.CONNECTED) {
            toggleConnectButton.setText("Connect");
            mMqttClient.dispose();
        } else if(mMqttClient.getState() == MqttClientState.DISCONNECTED) {
            mMqttClient.connect();
            toggleConnectButton.setText("Disconnect");
        } else {
            Timber.d("Connecting, please wait...");
        }
    }

    @OnClick(R.id.btn_subscribe_to_message_topic)
    public void onSubscribeToMessageTopicsClicked(View view) {
        Timber.d("onSubscribeToMessageTopicsClicked");
        mMqttClient.subscribeToTopic("hashdrive/message")
                .subscribe(mqtt3Publish -> {
                    // this is just an example (other use debug log instead)
                    Snackbar.make(getView(), "Recieved message: " + new String(mqtt3Publish.getPayloadAsBytes()), Snackbar.LENGTH_LONG).show();;
                }, throwable -> Timber.e("Message topics subscription failed!"));
    }

    @OnClick(R.id.btn_publish_message)
    public void onPublishMessageTopicClicked(View view) {
        Timber.d("onPublishMessageTopicClicked");
        if(publishMessageButton.getText().equals("Publish message")) {
            mMqttClient.publishPayload("hashdrive/message", "Hello world".getBytes());
            publishMessageButton.setText("Remove message");
        } else {
            mMqttClient.removeRetainedMessage("hashdrive/message");
            publishMessageButton.setText("Publish message");
        }
    }

    @OnClick(R.id.btn_subscribe_to_offers)
    public void onSubscribeToOffersClicked(View view) {
        Timber.d("onSubscribeToOffersClicked");
        mMqttClient.subscribeToOffers("si", "Osrednjeslovenska");
    }

    @OnClick(R.id.btn_publish_offer)
    public void onPublishOfferClicked(View view) {
        Timber.d("onPublishOfferClicked");
        publishOffer();
    }

    public MqttTestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mqtt_test, container, false);
        ButterKnife.bind(this, view);
        mDeveloperToolsActivity = (DeveloperToolsActivity)getActivity();
        mMqttClient = new MqttManager(MQTT_BROKER_HOST);
        mDatabaseManager = HashdriveApp.getInstance().createDatabaseManager();
        mMqttRideOffersSubscription = mMqttClient.getRideOffersObservable("+", "+")
                .subscribe(rideOfferSignaturePair -> {
                    Timber.d("New ride offer received, performing insert/update...");
                    mDatabaseManager.addOrUpdateRideOffer(rideOfferSignaturePair.first, rideOfferSignaturePair.second);
                }, throwable -> Timber.e(throwable, "Observing ride offers failed!"));


        mRideOffersListAdapter = new RideOffersListAdapter(getContext(), new ArrayList<>());
        offersListView.setAdapter(mRideOffersListAdapter);

        List<String> countryCodes = new ArrayList<>();
        List<String> stateDistricts = new ArrayList<>();
        countryCodes.add("si");
        stateDistricts.add("Osrednjeslovenska");
        mDbRideOffersSubscription = mDatabaseManager.findRideOffersInArea(countryCodes, stateDistricts)
                .doOnNext(rides -> Timber.d("New rides found (%d) (before debounce)!", rides.size()))
                .debounce(3, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rides -> {
                    Timber.d("New rides found (after debounce), refreshing items in list view... (thread: %s)", Thread.currentThread());
                    mRideOffersListAdapter.clear();
                    mRideOffersListAdapter.addAll(rides);
                }, throwable -> Timber.e(throwable, "Observing ride in the area failed!"));

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(mMqttRideOffersSubscription != null && !mMqttRideOffersSubscription.isDisposed()) {
            mMqttRideOffersSubscription.dispose();
            mMqttRideOffersSubscription = null;
        }
        if(mDbRideOffersSubscription != null && !mDbRideOffersSubscription.isDisposed()) {
            mDbRideOffersSubscription.dispose();
            mDbRideOffersSubscription = null;
        }
        mMqttClient.dispose();
        mDatabaseManager = null;
    }


    private void publishOffer() {
        // stub driver account key pair
        // sUzzssFuvf7bXvOF7VAjHVA6wXbi6SPPgqOn0TnR0Nk=
        // Recovery words: [rally, guide, uncover, lizard, type, youth, hope, taxi, magnet, hedgehog, angle, deny, admit, gaze, swear, frog, cat, lab, pottery, exile, bean, trophy, drop, coin]
        // byte[] entropy = CryptoUtils.getSecureRandomData(32);
        byte[] entropy = Base64.decode("sUzzssFuvf7bXvOF7VAjHVA6wXbi6SPPgqOn0TnR0Nk=", Base64.DEFAULT);
        HGCSeed seed = new HGCSeed(entropy);
        Timber.d("Entropy: %s, \nRecovery words: %s", Base64.encodeToString(entropy, Base64.DEFAULT), Arrays.toString(seed.toWordsList().toArray()));


        KeyPair walleyKeyPair = new EDKeyPair(seed.getEntropy());
        byte[] publicKey = walleyKeyPair.getPublicKey();

        String publicKeyAsString = CryptoUtils.bytesToString(publicKey);
        LineString stubStringLine = new LineString(new Coordinate[] { new Coordinate(1,3,4),  new Coordinate(1,6,4)}, new PrecisionModel(), 0);
        RideOfferOuterClass.RideOffer rideOffer = RideOfferOuterClass.RideOffer.newBuilder()
                .setOfferAddress(UUID.randomUUID().toString())
                .setUpdatedAt(Instant.now().toEpochMilli())
                .setDriverAddress(publicKeyAsString)
                .setVehicleType(RideOfferOuterClass.VehicleType.CAR)
                .setPrice(10_000L)
                .setCountryCode("si")
                .setStateDistrict("Osrednjeslovenska")
                .setLineStringWKT(Converters.fromLineStringToString(stubStringLine))
                .setFreeSpots(4)
                .setTimeLimitStart(0)
                .setTimeLimitEnd(0)
                .setDistanceLimitStart(0)
                .setDistanceLimitEnd(0)
                .build();

        byte[] msg = rideOffer.toByteArray();
        byte[] sig = walleyKeyPair.signMessage(msg);
//        mDatabaseManager.addOrUpdateRideOffer(rideOffer, sig); this is done in #mMqttRideOffersSubscription
        mMqttClient.publishOffer(rideOffer, sig);
    }

}
