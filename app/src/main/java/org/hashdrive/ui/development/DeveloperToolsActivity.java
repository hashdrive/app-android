package org.hashdrive.ui.development;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.hashdrive.R;
import org.hashdrive.ui.mainboard.RidesCalendarFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class DeveloperToolsActivity extends AppCompatActivity implements
        DeveloperToolsFragment.OnFragmentInteractionListener,
        RidesCalendarFragment.OnFragmentInteractionListener {

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("onCreate");
        setContentView(R.layout.activity_developer_tools);
        ButterKnife.bind(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Timber.d("onDestroy!");
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}
