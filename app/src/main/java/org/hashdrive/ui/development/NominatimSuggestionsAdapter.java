package org.hashdrive.ui.development;

import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.provider.BaseColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import org.hashdrive.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.dudie.nominatim.model.Address;
import timber.log.Timber;

public class NominatimSuggestionsAdapter extends CursorAdapter {

    private List<Address> mAddresses;

    public NominatimSuggestionsAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mAddresses = new ArrayList<>();
    }

    public void setAddressList(List<Address> addresses) {
        this.mAddresses = addresses;
        swapCursor(createCursorFromResult(mAddresses));
    }

    public List<Address> getAddresses() {
        return mAddresses;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        Timber.d("newView called, creating a new suggestion view");
        return LayoutInflater.from(context).inflate(R.layout.item_location_suggestion, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Timber.d("bindView called, setting view's values...");
        LocationSuggestionViewHolder holder = new LocationSuggestionViewHolder(view);
        String address = cursor.getString(1);
        double longitude = cursor.getDouble(2);
        double latitude = cursor.getDouble(3);

        String[] displayedNameArray = address.split(",");

        StringBuilder sb = new StringBuilder();
        int lastElementIndex = displayedNameArray.length - 1;
        for (int i = 1; i < displayedNameArray.length; i++) {
            sb.append(displayedNameArray[i]);
            if(i != lastElementIndex) {
                sb.append(",");
            }
        }

        holder.locationTitleTextView.setText(displayedNameArray[0]);
        holder.locationDetailsTextView.setText(sb.toString());
    }

    private Cursor createCursorFromResult(List<Address> addresses)  {
        String[] menuCols = new String[] { BaseColumns._ID, SearchManager.SUGGEST_COLUMN_TEXT_1, "longitude", "latitude" };
        MatrixCursor cursor = new MatrixCursor(menuCols);
        int counter = 0;
        for (Address address : addresses) {
            cursor.addRow(new Object[] { counter, address.getDisplayName(), address.getLongitude(), address.getLatitude() });
        }
        return cursor;
    }


    class LocationSuggestionViewHolder {
        @BindView(R.id.tv_location_title)
        TextView locationTitleTextView;

        @BindView(R.id.tv_location_details)
        TextView locationDetailsTextView;

        public LocationSuggestionViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
