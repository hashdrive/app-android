package org.hashdrive.ui.importwalletboard;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.hashdrive.R;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SecureFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.buttonDigitPin) Button mDigitPinBtn;
    @BindView(R.id.buttonFingerPrint) Button mFingerPrintBtn;
    @BindView(R.id.buttonCancel) Button mCancelBtn;

    public SecureFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_secure, container, false);
        ButterKnife.bind(this, view);

        return view;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @OnClick(R.id.buttonDigitPin)
    public void actionCheckPin() {

    }

    @OnClick(R.id.buttonDigitPin)
    public void actionCheckFingerPrint() {

    }

    @OnClick(R.id.buttonCancel)
    public void actionCancel() {

    }

}
