package org.hashdrive.ui.importwalletboard;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;

import org.hashdrive.R;

public class ImportWalletActivity extends AppCompatActivity implements AccountVerificationFragment.OnFragmentInteractionListener,
        RecoveryFragment.OnFragmentInteractionListener,
        SecureFragment.OnFragmentInteractionListener,
        WalletImportedFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_wallet);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
