package org.hashdrive.ui.auth;

import android.app.KeyguardManager;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import androidx.appcompat.app.AppCompatActivity;

import org.hashdrive.HashdriveApp;
import org.hashdrive.database.DatabaseManager;
import org.hashdrive.local_auth.AuthManager;
import org.hashdrive.ui.auth.fingerprint.FingerprintActivity;
import org.hashdrive.ui.auth.pin.PinAuthActivity;

import timber.log.Timber;

public abstract class AuthActivity extends AppCompatActivity implements AuthManager.AuthListener {
    public static final int REQUEST_CODE_ENABLE = 11;
    public static final int REQUEST_CODE_UNLOCK = 12;
    public static final int REQUEST_CODE_FINGERPRINT = 221;
    public static final int SECURITY_SETTING_REQUEST_CODE = 233;
    private boolean forSetup;
    public AuthManager authManager = AuthManager.getDefaultInstance();
    public DatabaseManager databaseManager = HashdriveApp.getInstance().createDatabaseManager();

    @Override
    protected void onPause() {
        super.onPause();
        authManager.handleActivityDidPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        authManager.handleActivityDidResume();
    }

    public void requestAuth() {
        forSetup = false;
        Intent intent;
        switch (authManager.getAuthType()) {
            case PIN:
                Timber.d("Auth PIN (unlock)...");
                intent = new Intent(this, PinAuthActivity.class);
                intent.putExtra(PinAuthActivity.EXTRA_TYPE, PinAuthActivity.TYPE_UNLOCK);
                startActivityForResult(intent, REQUEST_CODE_UNLOCK);
                break;

            case FINGER:
                Timber.d("Auth FINGER (unlock)...");
                intent = new Intent(this, FingerprintActivity.class);
                intent.putExtra(FingerprintActivity.EXTRA_TYPE, FingerprintActivity.TYPE_UNLOCK);
                startActivityForResult(intent, REQUEST_CODE_FINGERPRINT);
                break;
        }
    }

    public void setupAuth(AuthManager.AuthType authType) {
        forSetup = true;
        Intent intent;
        switch (authType) {
            case PIN:
                Timber.d("Setup PIN...");
                intent = new Intent(this, PinAuthActivity.class);
                intent.putExtra(PinAuthActivity.EXTRA_TYPE, PinAuthActivity.TYPE_ENABLE);
                startActivityForResult(intent, REQUEST_CODE_ENABLE);
                break;
            case FINGER:
                Timber.d("Setup FINGER auth...");
                intent = new Intent(this, FingerprintActivity.class);
                intent.putExtra(FingerprintActivity.EXTRA_TYPE, FingerprintActivity.TYPE_ENABLE);
                startActivityForResult(intent, REQUEST_CODE_FINGERPRINT);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            if (requestCode == REQUEST_CODE_ENABLE || (requestCode == REQUEST_CODE_FINGERPRINT && forSetup))
                onAuthSetupFailed(true);
            else
                onAuthFailed(requestCode, true);
            return;
        }

        switch (requestCode){
            case REQUEST_CODE_ENABLE:
                onAuthSetupSuccess();
                break;

            case REQUEST_CODE_UNLOCK:
                onAuthSuccess(requestCode);
                break;

            case REQUEST_CODE_FINGERPRINT:
                //If screen lock authentication is success update text
                if (forSetup) {
                    onAuthSetupSuccess();
                } else  {
                    onAuthSuccess(requestCode);
                }

                break;

            case SECURITY_SETTING_REQUEST_CODE:
                //When user is enabled Security settings then we don't get any kind of RESULT_OK
                //So we need to check whether device has enabled screen lock or not
                if (isDeviceSecure()) {
                    //If screen lock enabled show toast and start intent to authenticate user
                    //Toast.makeText(this, getResources().getString(R.string.device_is_secure), Toast.LENGTH_SHORT).show();
                    authenticateApp();
                } else {
                    //If screen lock is not enabled just update text
                    //textView.setText(getResources().getString(R.string.security_device_cancelled));
                }
                break;
        }
    }


    //method to authenticate app
    private void authenticateApp() {

        //Get the instance of KeyGuardManager
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

        //Check if the device version is greater than or equal to Lollipop(21)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //Create an intent to open device screen lock screen to authenticate
            //Pass the Screen Lock screen Title and Description
            Intent i = keyguardManager.createConfirmDeviceCredentialIntent("UnlockHashdriveApp", "UnlockHashdriveAppDesc");
            try {
                //Start activity for result
                startActivityForResult(i, REQUEST_CODE_FINGERPRINT);
            } catch (Exception e) {

                //If some exception occurs means Screen lock is not set up please set screen lock
                //Open Security screen directly to enable patter lock
                Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
                try {

                    //Start activity for result
                    startActivityForResult(intent, SECURITY_SETTING_REQUEST_CODE);
                } catch (Exception ex) {

                    //If app is unable to find any Security settings then user has to set screen lock manually
                    //textView.setText(getResources().getString(R.string.setting_label));
                }
            }
        }
    }

    /**
     * method to return whether device has screen lock enabled or not
     **/
    private boolean isDeviceSecure() {
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

        //this method only work whose api level is greater than or equal to Jelly_Bean (16)
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && keyguardManager.isKeyguardSecure();

        //You can also use keyguardManager.isDeviceSecure(); but it requires API Level 23
    }

}
