/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package org.hashdrive.ui.auth.fingerprint;

import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.hashdrive.R;
import org.hashdrive.local_auth.AuthManager;
import org.hashdrive.local_auth.SecureStorage;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;


public class FingerprintActivity extends AppCompatActivity {

    public static final String EXTRA_TYPE = "EXTRA_TYPE";
    public static final String TYPE_UNLOCK = "TYPE_UNLOCK";
    public static final String TYPE_ENABLE = "TYPE_ENABLE";

    public FingerprintActivityHelper helper = new FingerprintActivityHelper();
    FingerprintAuthenticationDialogFragment fDialog = null;
    private boolean forSetup;

    private Disposable mPerformAuthDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint);
        forSetup = getIntent().getStringExtra(EXTRA_TYPE).equals(TYPE_ENABLE);
        byte[] iv = SecureStorage.getIV(SecureStorage.KEY_DATA_ENCRYPTION_KEY_FINGERPRINT);
        boolean setupSucceeded = helper.setup(this, forSetup, iv, false); // createKey now part of #setup

        if(!setupSucceeded) {
            Toast.makeText(getApplicationContext(), helper.errorMsg, Toast.LENGTH_LONG).show(); // TODO: 2019-06-30 improve
            finish();
            return;
        }
        startAuth();
    }

    /**
     * Proceed the purchase operation
     *
     * @param withFingerprint {@code true} if the purchase was made by using a fingerprint
     * @param cryptoObject the Crypto object
     */
    public void onAuthSuccess(boolean withFingerprint,
            @Nullable final FingerprintManager.CryptoObject cryptoObject) {
        if (withFingerprint) {
            // If the user has authenticated with fingerprint, verify that using cryptography and
            // then show the confirmation message.
            assert cryptoObject != null;

            final View hgcLogo = findViewById(R.id.text_hgc_icon);
            hgcLogo.setVisibility(View.INVISIBLE);

            mPerformAuthDisposable = performAuthObservable(cryptoObject)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe(type -> {
                        Timber.d("Auth %s succeeded!", type);
                        setResult(RESULT_OK);
                        finish();

                    }, throwable -> {
                        Timber.e(throwable, "Auth failed, closing activity...");
                        finish();
                    });

        }
    }

    private Single<String> performAuthObservable(@Nullable final FingerprintManager.CryptoObject cryptoObject) {
        return Single.create(emitter -> {
            if (helper.isForSetup()) {
                AuthManager.getDefaultInstance().setFingerprintAuth(cryptoObject.getCipher());
                emitter.onSuccess(TYPE_ENABLE);
            } else {
                boolean isVerified = AuthManager.getDefaultInstance().verifyFingerPrintAuth(cryptoObject.getCipher());
                if(isVerified) {
                    emitter.onSuccess(TYPE_UNLOCK);
                } else {
                    emitter.onError(new Exception(TYPE_UNLOCK));
                }
            }
        });
    }

    private void startAuth() {
        // Set up the crypto object for later. The object will be authenticated by use
        // of the fingerprint.
        final String DIALOG_FRAGMENT_TAG = FingerprintAuthenticationDialogFragment.class.getSimpleName();

        if (helper.initCipher()) {
            // Show the fingerprint dialog. The user has the option to use the fingerprint with
            // crypto, or you can fall back to using a server-side verified password.
            FingerprintAuthenticationDialogFragment fragment
                    = new FingerprintAuthenticationDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("forSetup", forSetup);
            fragment.setArguments(bundle);
            fragment.setCryptoObject(new FingerprintManager.CryptoObject(helper.mCipher));
            fragment.setStage(
                    FingerprintAuthenticationDialogFragment.Stage.FINGERPRINT);
            fragment.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
            fDialog = fragment;
        } else {
            // This happens if the lock screen has been disabled or or a fingerprint got
            // enrolled. Thus show the dialog to authenticate with their password first
            // and ask the user if they want to authenticate with fingerprints in the
            // future
            FingerprintAuthenticationDialogFragment fragment
                    = new FingerprintAuthenticationDialogFragment();
            fragment.setCryptoObject(new FingerprintManager.CryptoObject(helper.mCipher));
            fragment.setStage(
                    FingerprintAuthenticationDialogFragment.Stage.NEW_FINGERPRINT_ENROLLED);
            fragment.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
            fDialog = fragment;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mPerformAuthDisposable != null && !mPerformAuthDisposable.isDisposed()) {
            mPerformAuthDisposable.dispose();
        }
        mPerformAuthDisposable = null;
    }
}
