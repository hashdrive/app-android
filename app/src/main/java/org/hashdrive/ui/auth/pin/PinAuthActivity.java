package org.hashdrive.ui.auth.pin;

import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;

import org.hashdrive.R;
import org.hashdrive.local_auth.AuthManager;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class PinAuthActivity extends AppCompatActivity {

    public static final String TAG = "PinLockView";
    public static final String EXTRA_TYPE = "EXTRA_TYPE";
    public static final String TYPE_UNLOCK = "TYPE_UNLOCK";
    public static final String TYPE_ENABLE = "TYPE_ENABLE";

    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    private String previousPin;
    private boolean forSetup = false;
    private int pinLength = 4;
    private TextView mTextViewHgcIcon, mTextViewProfileName;

    private Disposable mPerformAuthDisposable;

    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(final String pin) {
            if (forSetup) {
                if (previousPin != null) {
                    if (!previousPin.equals(pin)) {
                        showError("Previous pin and new pin did not match. Please try again.");
                        previousPin = null;
                        resetPinView();

                    } else {

                        final ProgressBar progressBar = findViewById(R.id.progress_bar);
                        progressBar.setVisibility(View.VISIBLE);
                        mTextViewHgcIcon.setVisibility(View.INVISIBLE);

                        mPerformAuthDisposable = performAuthObservable(pin)
                                .delay(1, TimeUnit.SECONDS)
                                .subscribe(type -> {
                                    Timber.d("Auth %s succeeded!", type);
                                    setResult(RESULT_OK);
                                    finish();
                                }, throwable -> {
                                    Timber.e(throwable, "%s failed, closing activity...", (forSetup ? "Auth setup" : "Auth"));
                                    resetPinView();
                                    progressBar.setVisibility(View.GONE);
                                    mTextViewHgcIcon.setVisibility(View.VISIBLE);
                                });
                    }
                } else {
                    previousPin = pin;
                    showMessage("Please enter your PIN again.");
                    resetPinView();
                }
            } else {

                final ProgressBar progressBar = findViewById(R.id.progress_bar);
                progressBar.setVisibility(View.VISIBLE);
                mTextViewHgcIcon.setVisibility(View.INVISIBLE);

                mPerformAuthDisposable = performAuthObservable(pin)
                        .delay(1, TimeUnit.SECONDS)
                        .subscribe(type -> {
                            Timber.d("Auth %s succeeded!", type);
                            setResult(RESULT_OK);
                            finish();
                        }, throwable -> {
                            Timber.e(throwable, "%s failed!", (forSetup ? "Auth setup" : "Auth"));
                            showError(throwable.getMessage());
                            resetPinView();
                            progressBar.setVisibility(View.GONE);
                            mTextViewHgcIcon.setVisibility(View.VISIBLE);
                        });
            }
        }

        private Single<String> performAuthObservable(@Nullable final String pin) {
            return Single.create(emitter -> {
                if (forSetup) {
                    AuthManager.getDefaultInstance().setPIN(pin);
                    emitter.onSuccess(TYPE_ENABLE);
                } else {
                    boolean isVerified = AuthManager.getDefaultInstance().verifyPIN(pin);
                    if (isVerified) {
                        emitter.onSuccess(TYPE_UNLOCK);
                    } else {
                        emitter.onError(new Exception("Wrong PIN. Please try again."));
                    }
                }
            });
        }

        @Override
        public void onEmpty() {

        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            TextView errorTV = findViewById(R.id.error_message);
            errorTV.setVisibility(View.GONE);
        }
    };

    private void showMessage(String string) {
        TextView errorTV = findViewById(R.id.error_message);
        TextView messageTV = findViewById(R.id.message);
        errorTV.setVisibility(View.GONE);
        messageTV.setVisibility(View.VISIBLE);
        messageTV.setText(string);
    }

    private void showError(String string) {
        TextView errorTV = findViewById(R.id.error_message);
        TextView messageTV = findViewById(R.id.message);
        errorTV.setVisibility(View.VISIBLE);
        messageTV.setVisibility(View.GONE);
        errorTV.setText(string);
    }

    private void resetPinView() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPinLockView.resetPinLockView();
            }
        }, 300);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pin_auth);
        if (getIntent().getStringExtra(EXTRA_TYPE).equals(TYPE_ENABLE))
            forSetup = true;

        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);

        mPinLockView.attachIndicatorDots(mIndicatorDots);
        mPinLockView.setPinLockListener(mPinLockListener);

        mTextViewHgcIcon = (TextView) findViewById(R.id.text_hgc_icon);
        mTextViewProfileName = findViewById(R.id.profile_name);
        //mPinLockView.setCustomKeySet(new int[]{2, 3, 1, 5, 9, 6, 7, 0, 8, 4});
        //mPinLockView.enableLayoutShuffling();

        mPinLockView.setPinLength(4);
        mPinLockView.setTextColor(Color.WHITE);

        mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);
        mIndicatorDots.setPinLength(pinLength);

        mTextViewProfileName.setVisibility(View.GONE);
        showMessage(getResources().getString(forSetup ? R.string.create_pin : R.string.enter_pin));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPerformAuthDisposable != null && !mPerformAuthDisposable.isDisposed()) {
            mPerformAuthDisposable.dispose();
        }
        mPerformAuthDisposable = null;
    }
}
