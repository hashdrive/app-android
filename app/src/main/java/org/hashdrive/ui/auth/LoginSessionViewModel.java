package org.hashdrive.ui.auth;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.hashdrive.HashdriveApp;
import org.hashdrive.database.DatabaseManager;
import org.hashdrive.local_auth.AuthManager;

import timber.log.Timber;

public class LoginSessionViewModel extends ViewModel {

    private final MutableLiveData<AuthenticationState> mAuthenticationState;
    private AuthManager mAuthManager;
    private DatabaseManager mDatabaseManager;

    public LoginSessionViewModel() {
        Timber.d("Creating login session view model instance...");
        mDatabaseManager = HashdriveApp.getInstance().createDatabaseManager();
        mAuthenticationState = new MutableLiveData<>();
        mAuthManager = AuthManager.getDefaultInstance();

        initAuthenticationState();
    }

    public void initAuthenticationState() {
        LoginSessionViewModel.AuthenticationState currentState;
        if(mAuthManager.hasAuth()) {
            currentState = AuthenticationState.AUTHENTICATED;
        } else if (mAuthManager.getAuthType() != AuthManager.AuthType.UNKNOWN) {
            currentState = AuthenticationState.UNAUTHENTICATED;
        } else {
            currentState = AuthenticationState.NOT_SETUP;
        }
        mAuthenticationState.setValue(currentState);
    }

    @Override
    protected void onCleared() {
        Timber.d("Login session view model onCleared called!");
        super.onCleared();
        mDatabaseManager = null;
        mAuthManager = null;
    }

    public MutableLiveData<AuthenticationState> getAuthenticationState() {
        return mAuthenticationState;
    }

    public enum AuthenticationState {
        NOT_SETUP,             // The user needs to setup authentication method
        SETUP_FAILED,          // Setup failed (e.g. user terminated finger setup by clicking on cancel button)
        UNAUTHENTICATED,       // The user needs to authenticate
        AUTHENTICATED,         // The user has authenticated successfully
        AUTHENTICATION_FAILED  // Authentication failed
    }
}
