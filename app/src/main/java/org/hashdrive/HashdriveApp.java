package org.hashdrive;

import android.app.Application;

import com.google.common.io.BaseEncoding;

import org.hashdrive.crypto.CryptoUtils;
import org.hashdrive.crypto.EDKeyChain;
import org.hashdrive.crypto.HGCSeed;
import org.hashdrive.crypto.KeyChain;
import org.hashdrive.crypto.KeyPair;
import org.hashdrive.database.AppDatabase;
import org.hashdrive.database.DatabaseManager;
import org.hashdrive.database.account.Account;
import org.hashdrive.database.wallet.Wallet;
import org.hashdrive.local_auth.AuthManager;

import java.util.Map;
import java.util.WeakHashMap;

import io.paperdb.Paper;
import timber.log.Timber;

public class HashdriveApp extends Application {

    private static HashdriveApp mInstance;
    private AppDatabase mAppDatabase;
    private AuthManager mAuthManager;
    private DatabaseManager mDatabaseManager;
    private Map<Long, String> publicKeyMap = new WeakHashMap<>(); // used to cache account index to public key mappings

    public static HashdriveApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.d("onCreate");
        initTimber();
        Paper.init(this);
        initDatabase();
        initAuthManager();
        mInstance = this;
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        Timber.d("onTerminate");
        mInstance = null;
        mAuthManager = null;
    }

    private void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());

        } else {
//            Timber.plant(new CrashReportingTree());
            throw new RuntimeException("Add Timber tree for release!");
        }
    }

    private void initDatabase() {
        Timber.d("Initializing database...");
        mAppDatabase = AppDatabase.createOrGetAppDatabase(getApplicationContext());
        mDatabaseManager = createDatabaseManager();
    }

    public DatabaseManager createDatabaseManager() {
        return new DatabaseManager(mAppDatabase);
    }

    private void initAuthManager() {
        Timber.d("Initializing auth manager...");
        mAuthManager = AuthManager.getDefaultInstance();
    }

    public KeyChain getKeyChain() {
        HGCSeed seed = mAuthManager.getSeed();
        Wallet masterWallet = mDatabaseManager.getMasterWallet();
        if(masterWallet != null) {
            switch (masterWallet.getHGCKeyType()) {
                case ED25519:
                    return new EDKeyChain(seed);
                case RSA3072:
                    Timber.d("RSA3072 currently not supported!");
                    break;
                case ECDSA384:
                    Timber.d("ECDSA384 currently not supported!");
                    break;
            }
        }
        return null;
    }

    public KeyPair keyForAccount(Account account) {
        return getKeyChain().keyAtIndex(account.getAccountIndex());
    }

    public String publicKeyString(Account account) {
        String publicKey = publicKeyMap.get(account.getAccountIndex());
        if (publicKey == null) {
            KeyPair keyPair = keyForAccount(account);
            if(keyPair == null) {
                return null;
            } else {
                publicKey = CryptoUtils.bytesToString(keyPair.getPublicKey());
                publicKeyMap.put(account.getAccountIndex(), publicKey);
            }
        }
        return publicKey;
    }

    public String publicKeyStringShort(Account account) {
        String publicKey = publicKeyString(account);
        return ((publicKey == null)? null : publicKeyLastCharacter(publicKey, 6));
    }

    public String privateKeyString(Account account) {
        KeyPair keyPair = keyForAccount(account);
        return CryptoUtils.bytesToString(keyPair.getPrivateKey());
    }

    public String publicKeyLastCharacter(String key, int value) {
        return (key.length() > value? key.substring(key.length() - value) : key);
    }

}
