package org.hashdrive.mqtt;

import android.util.Base64;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.protobuf.InvalidProtocolBufferException;
import com.hivemq.client.mqtt.MqttClientState;
import com.hivemq.client.mqtt.datatypes.MqttQos;
import com.hivemq.client.mqtt.mqtt3.Mqtt3Client;
import com.hivemq.client.mqtt.mqtt3.Mqtt3RxClient;
import com.hivemq.client.mqtt.mqtt3.message.publish.Mqtt3Publish;

import net.i2p.crypto.eddsa.EdDSAPublicKey;
import net.i2p.crypto.eddsa.spec.EdDSANamedCurveTable;
import net.i2p.crypto.eddsa.spec.EdDSAParameterSpec;
import net.i2p.crypto.eddsa.spec.EdDSAPublicKeySpec;

import org.hashdrive.crypto.CryptoUtils;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import timber.log.Timber;

public class MqttManager {

    //    0:hashdrive/1:ride/2:%s/3:%s/4:%s/5:offer/6:%s/7:sig/8:%s
    private static final int OFFER_TOPIC_LEVEL_VEHICLE_TYPE = 2;
    private static final int OFFER_TOPIC_LEVEL_COUNTRY_CODE = 3;
    private static final int OFFER_TOPIC_LEVEL_STATE_DISTRICT = 4;
    private static final int OFFER_TOPIC_LEVEL_OFFER_ADDRESS = 6;
    private static final int OFFER_TOPIC_LEVEL_SIGNATURE = 8;

    private final Mqtt3RxClient client;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private PublishSubject<Pair<RideOfferOuterClass.RideOffer, byte[]>> rideOfferPublishSubject = PublishSubject.create();

    public MqttManager(String host) {
        client = Mqtt3Client.builder()
                .identifier(UUID.randomUUID().toString())
                .serverHost(host)
                .buildRx();
    }

    public void connect() {
        compositeDisposable.add(client.connect()
                .subscribe(mqtt3ConnAck -> Timber.d("Mqtt client connected!"), throwable -> Timber.e(throwable, "Connecting to the mqtt client failed!")));
    }

    public @org.jetbrains.annotations.NotNull MqttClientState getState() {
        return client.getState();
    }

    public Observable<Mqtt3Publish> subscribeToTopic(String topicFilter) {
        return Observable.create(emitter -> {
            Disposable disposable = client.subscribeStreamWith()
                    .topicFilter(topicFilter)
                    .qos(MqttQos.AT_LEAST_ONCE)
                    .applySubscribe()
                    .subscribe(mqtt3Publish -> {
                                Timber.d("Mqtt3 publishPayload called from topic: %s", mqtt3Publish.getTopic());
                                String msg = new String(mqtt3Publish.getPayloadAsBytes());
                                Timber.d("Msg: %s", msg);
                                emitter.onNext(mqtt3Publish);
                            },
                            throwable -> Timber.e(throwable, "Subscription failed!"));
            emitter.setDisposable(disposable);
            compositeDisposable.add(disposable);
        });
    }

    public void publishPayload(@NonNull String topic, @Nullable byte[] payload) {
        Mqtt3Publish msg = Mqtt3Publish.builder()
                .topic(topic)
                .qos(MqttQos.AT_LEAST_ONCE)
                .retain(true)
                .payload(payload)
                .build();
        compositeDisposable.add(
                client.publish(Flowable.just(msg))
                        .subscribe(mqtt3PublishResult -> Timber.d("Message published!"), throwable -> Timber.e(throwable, "publishPayload failed!"))
        );
    }

    public void removeRetainedMessage(String topic) {
        publishPayload(topic, null);
    }

    public void publishOffer(RideOfferOuterClass.RideOffer ride, byte[] signature) {
        final byte[] payload = ride.toByteArray();
        String topic = String.format("hashdrive/ride/%s/%s/%s/offer/%s/sig/%s", ride.getVehicleType().name(), ride.getCountryCode(), ride.getStateDistrict(),
                ride.getOfferAddress(), Base64.encodeToString(signature, Base64.NO_PADDING | Base64.URL_SAFE | Base64.NO_WRAP));
        Timber.d("Publishing to topic: %s, payload: %s", topic, Arrays.toString(payload));
        publishPayload(topic, payload);
    }

    public void removeRetainedOffer(RideOfferOuterClass.RideOffer ride, byte[] signature) {
        String topic = String.format("hashdrive/ride/%s/%s/%s/offer/%s/sig/%s", ride.getVehicleType().name(), ride.getCountryCode(), ride.getStateDistrict(),
                ride.getOfferAddress(), Base64.encodeToString(signature, Base64.NO_PADDING | Base64.URL_SAFE | Base64.NO_WRAP));
        removeRetainedMessage(topic);
    }

    public void subscribeToOffers(@NonNull String countryCode, @Nullable String stateDistrict) {
        String topicFilter = String.format("hashdrive/ride/%s/%s/%s/offer/%s/sig/%s", "+", countryCode, (stateDistrict == null ? "+" : stateDistrict), "+", "#");
        Timber.d("Subscribing to %s", topicFilter);
        compositeDisposable.add(
                client.subscribeStreamWith()
                        .topicFilter(topicFilter)
                        .qos(MqttQos.AT_LEAST_ONCE)
                        .applySubscribe()
                        .subscribe(this::handleRideOffer, throwable -> Timber.e(throwable, "Subscription to offers failed!"))
        );
    }

    private void handleRideOffer(Mqtt3Publish mqtt3Publish) {
        try {
            Timber.d("Mqtt3 publishPayload called from topic: %s", mqtt3Publish.getTopic());
            List<String> topicLevels = mqtt3Publish.getTopic().getLevels();


            byte[] signature = Base64.decode(topicLevels.get(OFFER_TOPIC_LEVEL_SIGNATURE), Base64.NO_PADDING | Base64.URL_SAFE | Base64.NO_WRAP);
            byte[] payload = mqtt3Publish.getPayloadAsBytes();
            RideOfferOuterClass.RideOffer ride = RideOfferOuterClass.RideOffer.parseFrom(payload);

            Preconditions.checkArgument(topicLevels.get(OFFER_TOPIC_LEVEL_VEHICLE_TYPE).equals(ride.getVehicleType().name()), "Vehicle types don't match!");
            Preconditions.checkArgument(topicLevels.get(OFFER_TOPIC_LEVEL_COUNTRY_CODE).equals(ride.getCountryCode()), "Country codes don't match!");
            Preconditions.checkArgument(topicLevels.get(OFFER_TOPIC_LEVEL_STATE_DISTRICT).equals(ride.getStateDistrict()), "State districts don't match!");
            Preconditions.checkArgument(topicLevels.get(OFFER_TOPIC_LEVEL_OFFER_ADDRESS).equals(ride.getOfferAddress()), "Offer addresses don't match!");

            //payload[0] = (byte) (payload[0] ^ 0xFF); // corrupt message demonstration
            EdDSAParameterSpec spec = EdDSANamedCurveTable.getByName(EdDSANamedCurveTable.ED_25519);
            EdDSAPublicKey publicKey = new EdDSAPublicKey(new EdDSAPublicKeySpec(CryptoUtils.stringToBytes(ride.getDriverAddress()), spec));
            boolean isMessageAuthentic = CryptoUtils.verifySignature(publicKey, payload, signature); // verifies that this message was originally posted by offer's creator (driver)

            Preconditions.checkState(isMessageAuthentic, "Message is not authentic!"); // driver needs to republish it

            Timber.d("Ride details: %s, signature: %s", ride, topicLevels.get(OFFER_TOPIC_LEVEL_SIGNATURE));
            rideOfferPublishSubject.onNext(Pair.create(ride, signature));


        } catch (InvalidProtocolBufferException e) {
            Timber.e(e, "Parse ride offer payload failed!");
        } catch (Exception e) {
            Timber.e(e, "Handle ride offer failed!");
        }
    }

    public void dispose() {
        if (client.getState().isConnected()) {
            Disposable disconnectDisposable = client.disconnect()
                    .subscribe(() -> Timber.d("Mqtt client disconnected!"),
                            throwable -> Timber.e(throwable, "Mqtt client disconnect failed!"));
        }
        compositeDisposable.clear();
    }

    public Observable<Pair<RideOfferOuterClass.RideOffer, byte[]>> getRideOffersObservable(@NonNull String countryCode, @Nullable String stateDistrict) {
        return rideOfferPublishSubject;
//                .filter(rideOfferPair -> rideOfferPair.first.getCountryCode().equals(countryCode) && rideOfferPair.first.getStateDistrict().equals(stateDistrict));
    }
}
