package org.hashdrive.utils;

import android.content.Context;
import android.os.Environment;

import androidx.annotation.NonNull;

import com.google.common.base.Preconditions;

import java.io.File;

import timber.log.Timber;

/**
 * Based on https://developer.android.com/training/data-storage/files.html
 */
public class StorageHelper {

    private static final String HASHDRIVE_DIR = "hashdrive";
    private static final String MAPS_DIR = "maps";

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * Returns app's internal files directory (data/data/org.hashdrive/files)
     * @param context
     * @return
     */
    public static File getInternalStorageDir(@NonNull Context context) {
        return context.getFilesDir();
    }

    /**
     * Returns app's external storage directory (in path emulated/Documents/Hashdrive) stored on the primary external storage (sdcard).
     * @param context
     * @return
     * @throws Exception
     */
    public static File getExternalFilesDir(@NonNull  Context context) {
        File documentsDir = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        File baseDir = new File(documentsDir, HASHDRIVE_DIR);
        if(!baseDir.exists()) {
            Timber.d("External files base dir (Documents/hashdrive) does not exist, creating directory...");
            boolean success = baseDir.mkdir();
            Preconditions.checkState(success);
        }
        return baseDir;
    }

    /**
     * Returns app's external storage directory (in path emulated/Documents/hashdrive/maps) stored on the primary external storage (sdcard).
     * @param context
     * @return
     * @throws Exception
     */
    public static File getExternalMapsDir(@NonNull  Context context) {
        File baseDir = new File(getExternalFilesDir(context), MAPS_DIR);
        if(!baseDir.exists()) {
            Timber.d("External files base dir (Documents/hashdrive/maps) does not exist, creating directory...");
            boolean success = baseDir.mkdir();
            Preconditions.checkState(success);
        }
        return baseDir;
    }


}
