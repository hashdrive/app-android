package org.hashdrive.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

import timber.log.Timber;

public class AssetsLoader {

    public static String loadJSONFromAsset(String fileName, Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            Timber.e(ex, "Reading from file %s failed!", fileName);
            json = null;
        }
        return json;
    }

}
