package org.hashdrive.utils;

import com.google.android.material.snackbar.Snackbar;

import io.github.kobakei.materialfabspeeddial.FabSpeedDial;
import timber.log.Timber;

public class FabSpeedDialUtils {

    // https://stackoverflow.com/a/35141898
    public static Snackbar applySnackbarPositionFix(Snackbar snackbar, FabSpeedDial fabSpeedDial) {
        return snackbar.setCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar transientBottomBar, int event) {
                super.onDismissed(transientBottomBar, event);
                Timber.d("Setting translation Y back to 0.0f");
                fabSpeedDial.setTranslationY(0.0f);
                fabSpeedDial.show();
            }
        });
    }
}
