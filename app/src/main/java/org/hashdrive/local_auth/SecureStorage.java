package org.hashdrive.local_auth;

import androidx.annotation.NonNull;

import com.google.common.base.Preconditions;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import io.paperdb.Paper;
import timber.log.Timber;

public class SecureStorage {

    public static final @NonNull String KEY_AUTH_TYPE = "key_auth_type";
    public static final @NonNull String KEY_USER_NAME = "key_user_name";
    public static final @NonNull String KEY_INTENT_URL = "key_intent_url";
    public static final @NonNull String KEY_BRANCH_PARAMS = "key_barch_params";
    public static final @NonNull String KEY_ASKED_FOR_QUERY_COST_WARNING = "key_asked_for_query_cost_warning";
    public static final @NonNull String KEY_HAS_SHOWN_BIP39_MNEMONIC = "key_has_shown_bip39_mnemonic";

    public static final @NonNull String KEY_DATA_ENCRYPTION_KEY_FINGERPRINT = "key-data-encryption-key-fingerprint";
    public static final @NonNull String KEY_DATA_ENCRYPTION_KEY_PIN = "key-data-encryption-key-pin";
    public static final @NonNull String KEY_SEED = "key-seed";

    private static EncryptionResult encrypt(byte[] message, Cipher cipher) {
        EncryptionResult result = null;
        try {
            result = new EncryptionResult(cipher.doFinal(message), cipher.getIV());
        } catch (BadPaddingException e) {
            Timber.e(e, "Encryption failed - Bad padding!");
        } catch (IllegalBlockSizeException e) {
            Timber.e(e, "Encryption failed - Illegal block size!");
        }

        return result;
    }

    private static byte[] decrypt(byte[] encryptedMessage, Cipher cipher) {
        byte[] result = null;
        try {
            result = cipher.doFinal(encryptedMessage);
        } catch (BadPaddingException e) {
            Timber.e(e, "Decryption failed - Bad padding!");
        } catch (IllegalBlockSizeException e) {
            Timber.e(e, "Decryption failed - Illegal block size!");
        }

        return result;
    }

    public static boolean hasValue(String key) {
        String keyMsg = key + "-m";
        return Paper.book().exist(keyMsg);
    }


    public static void clearValue(String key) {
        String keyMsg = key + "-m";
        String keyIV = key + "-iv";
        Paper.book().delete(keyMsg);
        Paper.book().delete(keyIV);
    }

    public static byte[] getValue(String key, Cipher cipher) {
        byte[] result = null;
        String keyMsg = key + "-m";
        byte[] msg = Paper.book().read(keyMsg);
        if(msg != null) {
            result = decrypt(msg, cipher);
        }

        return result;
    }

    public static byte[] getIV(String key) {
        byte[] result = null;
        String keyIV = key + "-iv";
        byte[] iv = Paper.book().read(keyIV);
        if(iv != null) {
            result = iv;
        }

        return result;
    }

    public static void setValue(String key, byte[] msg, Cipher cipher) {
        EncryptionResult result = encrypt(msg, cipher);

        Preconditions.checkNotNull(result);
        Preconditions.checkNotNull(result.getEncryptedData());
        Preconditions.checkNotNull(result.getIV());

        String keyMsg = key + "-m";
        String keyIV = key + "-iv";
        Paper.book().write(keyMsg, result.getEncryptedData());
        Paper.book().write(keyIV, result.getIV());
    }

    static class EncryptionResult{
        private byte[] encryptedData;
        private byte[] iv;

        public EncryptionResult(byte[] encryptedData, byte[] iv) {
            this.encryptedData = encryptedData;
            this.iv = iv;
        }

        public byte[] getEncryptedData() {
            return encryptedData;
        }

        public byte[] getIV() {
            return iv;
        }
    }
}
