package org.hashdrive.local_auth;

import org.hashdrive.crypto.CryptoUtils;
import org.hashdrive.crypto.HGCSeed;
import org.spongycastle.asn1.pkcs.PBKDF2Params;
import org.spongycastle.crypto.digests.SHA512Digest;
import org.spongycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.spongycastle.crypto.params.KeyParameter;
import org.threeten.bp.Instant;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import timber.log.Timber;

public class AuthManager {

    private static AuthManager mInstance;
    private static final int MAX_ALLOWED_BACKGROUND_STAYTIME_SECONDS = 120;

    private Instant appOnPauseTime;
    private Instant appOnResumeTime;
    private AuthType authType;
    private AESEncyption aes;

    public static synchronized AuthManager getDefaultInstance() {
        if (mInstance == null) {
            mInstance = new AuthManager();
        }
        return mInstance;
    }

    public void handleActivityDidPause() {
        appOnPauseTime = Instant.now();
    }

    public void handleActivityDidResume() {
        appOnResumeTime = Instant.now();
    }

    public boolean hasAuth() {
        long backgroundStayTime = -1L;
        if (appOnPauseTime != null && appOnResumeTime != null) {
            backgroundStayTime = appOnResumeTime.getEpochSecond() - appOnPauseTime.getEpochSecond();
        }
        return aes != null && backgroundStayTime >= 0 && backgroundStayTime <= MAX_ALLOWED_BACKGROUND_STAYTIME_SECONDS;
    }

    boolean hasFingerprintSetup() {
        return SecureStorage.hasValue(getSecureStorageKey(AuthType.FINGER));
    }

    boolean hasPinSetup() {
        return SecureStorage.hasValue(getSecureStorageKey(AuthType.PIN));
    }

    public AuthType getAuthType() {
        if (authType != null) {
            return authType;
        }

        if (hasFingerprintSetup()) {
            return AuthType.FINGER;
        } else if (hasPinSetup()) {
            return AuthType.PIN;
        } else {
            return AuthType.UNKNOWN;
        }
    }

    private void saveDataEncryptionKey(Cipher enCipher, byte[] key, AuthType authType) {
        SecureStorage.setValue(getSecureStorageKey(authType), key, enCipher);
    }

    // TODO: 2019-06-29 handle calls null checks
    private String getSecureStorageKey(AuthType authType) {
        switch (authType) {
            case PIN:
                return SecureStorage.KEY_DATA_ENCRYPTION_KEY_PIN;
            case FINGER:
                return SecureStorage.KEY_DATA_ENCRYPTION_KEY_FINGERPRINT;
        }
        return null;
    }

    private AESEncyption getRandomDataEncryptionKey() {
        return new AESEncyption(CryptoUtils.getSecureRandomData(AESEncyption.keySizeInBytes));
    }

    private void setEnCipher(Cipher cipher, AuthType a) {
        if (aes == null) {
            aes = getRandomDataEncryptionKey();
            saveDataEncryptionKey(cipher, aes.key, a);
            authType = a;
        }
    }

    private boolean setDrCipher(Cipher cipher, AuthType a) {
        byte[] aBytes = SecureStorage.getValue(getSecureStorageKey(a), cipher);
        if (aBytes != null) {
            aes = new AESEncyption(aBytes);
            return true;
        }
        return false;
    }


    public void setPIN(String pin) {
        byte[] key = deriveAESKey(pin, AESEncyption.keySize);
        AESEncyption pinAes = new AESEncyption(key);

        setEnCipher(pinAes.encryptionCipher, AuthType.PIN);
        disableFingerprintAuth();
    }

    void disableFingerprintAuth() {
        SecureStorage.clearValue(getSecureStorageKey(AuthType.FINGER));
    }

    public boolean verifyPIN(String pin) {
        byte[] key = deriveAESKey(pin, AESEncyption.keySize);
        AESEncyption pinAes = new AESEncyption(key);
        Cipher cipher = pinAes.decryptionCipher;
        boolean isValid = setDrCipher(cipher, AuthType.PIN);
//        if (!isValid) {
//             Check for migration
//            byte[] sb = SecureStorage.getValue(SecureStorage.KEY_HGC_SEED, cipher); I removed KEY_HGC_SEED because it looks like this is no longer used (in newer version of HHG wallet)
//            if(sb != null) {
//                setPIN(pin)
//                saveSeed(it);
//                return true;
//            }
//        }
        return isValid;
    }

    public void setFingerprintAuth(Cipher cipher) {
        setEnCipher(cipher, AuthType.FINGER);
        disablePinAuth();
    }

    void disablePinAuth() {
        SecureStorage.clearValue(getSecureStorageKey(AuthType.PIN));
    }

    public boolean verifyFingerPrintAuth(Cipher cipher) {
        return setDrCipher(cipher, AuthType.FINGER);
    }


    byte[] deriveAESKey(String passwordStr, int length) {
        byte[] password = passwordStr.getBytes();
        byte[] salt = new byte[]{-1};
        PBKDF2Params params = new PBKDF2Params(salt, 10000, length);

        PKCS5S2ParametersGenerator gen = new PKCS5S2ParametersGenerator(new SHA512Digest()); // // TODO: 2019-06-29 there are 2 dep. get rid of one (i think that it is not madgog but the other one)
        gen.init(password, params.getSalt(), params.getIterationCount().intValue());
        return ((KeyParameter) gen.generateDerivedParameters(length)).getKey();
    }

// getters/setters exposing AESEncyption instance (aes) removed

    public HGCSeed getSeed() {
        if (aes != null && aes.key != null) {
            byte[] data = SecureStorage.getValue(SecureStorage.KEY_SEED, aes.getDecryptionCipher());
            if (data != null) {
                return new HGCSeed(data);
            }
        }
        return null;
    }

    boolean saveSeed(byte[] seedBytes) {
        if(aes != null && aes.key != null) {
            SecureStorage.setValue(SecureStorage.KEY_SEED, seedBytes, aes.getEncryptionCipher());
            return true;
        }
        return false;
    }

    private class AESEncyption {
        final String cypherInstance = "AES/CBC/PKCS5Padding";
        final String initializationVector = "8119745113154120";
        static final int keySize = 256;
        static final int keySizeInBytes = 32;

        final byte[] key;
        final Cipher encryptionCipher;
        final Cipher decryptionCipher;

        public AESEncyption(byte[] key) {
            this.key = key;
            encryptionCipher = getEncryptionCipher();
            decryptionCipher = getDecryptionCipher();
            assert encryptionCipher != null && decryptionCipher != null;
        }

        private Cipher getEncryptionCipher() {
            final SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            try {
                Cipher cipher = Cipher.getInstance(cypherInstance);
                cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(initializationVector.getBytes()));
                return cipher;
            } catch (NoSuchAlgorithmException e) {
                Timber.e(e, "Get encryption cipher failed - No such algorithm exception!");
            } catch (NoSuchPaddingException e) {
                Timber.e(e, "Get encryption cipher failed - No such padding exception!");
            } catch (InvalidAlgorithmParameterException e) {
                Timber.e(e, "Get encryption cipher failed - Invalid algorithm parameter exception!");
            } catch (InvalidKeyException e) {
                Timber.e(e, "Get encryption cipher failed - Invalid key exception!");
            }
            return null;
        }

        private Cipher getDecryptionCipher() {
            final SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            try {
                Cipher cipher = Cipher.getInstance(cypherInstance);
                cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(initializationVector.getBytes()));
                return cipher;
            } catch (NoSuchAlgorithmException e) {
                Timber.e(e, "Get decryption cipher failed - No such algorithm exception!");
            } catch (NoSuchPaddingException e) {
                Timber.e(e, "Get decryption cipher failed - No such padding exception!");
            } catch (InvalidAlgorithmParameterException e) {
                Timber.e(e, "Get decryption cipher failed - Invalid algorithm parameter exception!");
            } catch (InvalidKeyException e) {
                Timber.e(e, "Get decryption cipher failed - Invalid key exception!");
            }
            return null;
        }


        byte[] encrypt(byte[] message) {
            try {
                byte[] result;
                result = encryptionCipher.doFinal(message);
                return result;
            } catch (BadPaddingException e) {
                Timber.e(e, "Encryption failed - Bad padding exception!");
            } catch (IllegalBlockSizeException e) {
                Timber.e(e, "Encryption failed - Illegal block size exception!");
            }

            return null;
        }

        byte[] decrypt(byte[] encryptedMessage) {
            try {
                byte[] result;
                result = decryptionCipher.doFinal(encryptedMessage);
                return result;
            } catch (BadPaddingException e) {
                Timber.e(e, "Decryption failed - Bad padding exception!");
            } catch (IllegalBlockSizeException e) {
                Timber.e(e, "Decryption failed - Illegal block size exception!");
            }

            return null;
        }
    }

    public enum AuthType {
        PIN("pin"),
        FINGER("finger"),
        UNKNOWN("unknown");

        String type;

        AuthType(String value) {
            this.type = value;
        }

        public String getValue() {
            return type;
        }
    }

    public interface AuthListener {
        void onAuthSetupSuccess();
        void onAuthSetupFailed(boolean isCancelled);
        void onAuthSuccess(int requestCode);
        void onAuthFailed(int requestCode, boolean isCancelled);
    }
}
