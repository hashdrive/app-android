package org.hashdrive.crypto;


import androidx.annotation.NonNull;

import com.google.common.io.BaseEncoding;

import net.i2p.crypto.eddsa.EdDSAEngine;
import net.i2p.crypto.eddsa.EdDSAPublicKey;
import net.i2p.crypto.eddsa.spec.EdDSANamedCurveTable;
import net.i2p.crypto.eddsa.spec.EdDSAParameterSpec;

import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Hex;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Signature;

public class CryptoUtils {
    @NonNull
    public static byte[] getSecureRandomData(int length){
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[length];
        random.nextBytes(bytes);
        return bytes;
    }

    @NonNull
    public static byte[] sha256Digest(@NonNull byte[] message) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] hash = digest.digest(message);
        return hash;
    }

    @NonNull
    public static byte[] sha384Digest(@NonNull byte[] message) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-384");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] hash = digest.digest(message);
        return hash;
    }

    @NonNull
    public static byte[] deriveKey(@NonNull byte[] seed, long index, int length) {

        byte[] password = new byte[seed.length + Long.BYTES];
        for (int i = 0; i < seed.length; i++) {
            password[i] = seed[i];
        }
        byte[] indexData = longToBytes(index);
        int c = 0;
        for (int i = indexData.length-1; i >=0 ; i--) {
            password[seed.length + c] = indexData[i];
            c++;
        }

        byte[] salt = new byte[]{-1};

        String passwordStr = Hex.toHexString(password);
        PBKDF2Params params = new PBKDF2Params(salt,2048, length*8);

        PKCS5S2ParametersGenerator gen = new PKCS5S2ParametersGenerator(new SHA512Digest());
        gen.init(password, params.getSalt(), params.getIterationCount().intValue());

        byte[] derivedKey = ((KeyParameter)gen.generateDerivedParameters(length*8)).getKey();

        return derivedKey;
    }

    @NonNull
    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

    public static String bytesToString(byte[] bytes) {
        return BaseEncoding.base16().lowerCase().encode(bytes);
    }

    public static byte[] stringToBytes(String encodedString) {
        return BaseEncoding.base16().lowerCase().decode(encodedString);
    }

    public static boolean verifySignature(@NonNull EdDSAPublicKey publicKey, @NonNull byte[] message, byte[] signature) {
        EdDSAParameterSpec spec = EdDSANamedCurveTable.getByName(EdDSANamedCurveTable.ED_25519);
        try {
            Signature sgr = new EdDSAEngine(MessageDigest.getInstance(spec.getHashAlgorithm()));
            sgr.initVerify(publicKey);
            sgr.update(message);
            return sgr.verify(signature);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
