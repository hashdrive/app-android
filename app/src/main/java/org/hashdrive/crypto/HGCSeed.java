package org.hashdrive.crypto;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import org.hashdrive.crypto.bip39.Mnemonic;
import org.hashdrive.crypto.bip39.MnemonicException;

import java.util.List;


public class HGCSeed {

    public static int bip39WordListSize = 24;
    private byte[] entropy; // 32 Bytes

    public HGCSeed(byte[] entropy) {
        this.entropy = entropy;
    }

    public HGCSeed(List<String> mnemonic) throws Exception {
        if (mnemonic.size() == HGCSeed.bip39WordListSize) {
            this.entropy = new Mnemonic().toEntropy(mnemonic);
        } else  {
            Reference reference = new Reference(TextUtils.join(" ", mnemonic));
            this.entropy = reference.toBytes();
        }
    }

    @NonNull
    public List<String> toWordsList(){
        try {
            return new Mnemonic().toMnemonic(entropy);
        } catch (MnemonicException.MnemonicLengthException e) {
            e.printStackTrace();
            return null;
        }
    }

    public byte[] getEntropy() {
        return entropy;
    }
}
