package org.hashdrive.crypto;

import androidx.annotation.NonNull;

public interface KeyChain {
    @NonNull
    KeyPair keyAtIndex(long index);
}