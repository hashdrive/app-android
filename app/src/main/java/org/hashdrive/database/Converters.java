package org.hashdrive.database;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import com.google.common.base.Preconditions;
import com.google.protobuf.InvalidProtocolBufferException;
import com.hedera.hashgraph.sdk.proto.TransactionID;

import org.bouncycastle.util.encoders.Hex;
import org.hashdrive.database.ride.VehicleType;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.io.WKTWriter;
import org.threeten.bp.Instant;

import java.util.Locale;

import timber.log.Timber;

public class Converters {
    @Nullable
    @TypeConverter
    public static String fromInstantToString(@Nullable Instant value) {
        if (value == null) {
            return null;
        }
        return String.format(Locale.GERMAN, "%d,%d", value.getEpochSecond(), value.getNano());
    }

    @Nullable
    @TypeConverter
    public static Instant toInstantFromString(@Nullable String value /** epoch seconds with nanoseconds adjustment, separated with comma **/) {
        if (value == null) {
            return null;
        }
        String[] epochSecondsNanoAdjPair = value.split(",");
        Preconditions.checkState(epochSecondsNanoAdjPair.length == 2);
        return Instant.ofEpochSecond(Long.parseLong(epochSecondsNanoAdjPair[0]), Long.parseLong(epochSecondsNanoAdjPair[1]));
    }

    @Nullable
    @TypeConverter
    public static String fromTransactionIdToString(@Nullable TransactionID value) {
        if (value == null) return null;
        try {
            return Hex.toHexString(value.toByteArray()).toLowerCase();

        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    @TypeConverter
    public static TransactionID toTransactionIDFromString(@Nullable String value) {
        if (value == null) return null;
        try {
            return TransactionID.parseFrom(Hex.decode(value));
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @NonNull
    @TypeConverter
    public static Integer fromVehicleTypeToInteger(@NonNull VehicleType vehicleType) {
        return vehicleType.ordinal();
    }

    @NonNull
    @TypeConverter
    public static VehicleType toVehicleTypeFromInteger(@NonNull Integer value) {
        for (VehicleType vehicleType : VehicleType.values()) {
            if (vehicleType.ordinal() == value) {
                return vehicleType;
            }
        }
        return VehicleType.CAR; // should not happen
    }

    @Nullable
    @TypeConverter
    public static LineString toLineStringFromString(@NonNull String wellKnownText ) {
        try {
            Timber.d("toLineStringFromString...");
            return new WKTReader().read(wellKnownText).getFactory().createLineString();
        } catch (ParseException e) {
            Timber.e(e, "Could not parse wellKnownText!");
        }
        Timber.w("Returning null from toLineStringFromString!");
        return null;
    }

    @NonNull
    @TypeConverter
    public static String fromLineStringToString(@NonNull LineString geometry) {
        return new WKTWriter().write(geometry);
    }

}
