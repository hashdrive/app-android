package org.hashdrive.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import org.hashdrive.database.account.Account;
import org.hashdrive.database.account.AccountDao;
import org.hashdrive.database.contract.Contact;
import org.hashdrive.database.contract.ContactDao;
import org.hashdrive.database.node.Node;
import org.hashdrive.database.node.NodeDao;
import org.hashdrive.database.request.PayRequest;
import org.hashdrive.database.request.PayRequestDao;
import org.hashdrive.database.ride.Ride;
import org.hashdrive.database.ride.RideDao;
import org.hashdrive.database.ride.VehicleType;
import org.hashdrive.database.smart_contract.SmartContract;
import org.hashdrive.database.smart_contract.SmartContractDao;
import org.hashdrive.database.transaction.TxnRecord;
import org.hashdrive.database.transaction.TxnRecordDao;
import org.hashdrive.database.wallet.Wallet;
import org.hashdrive.database.wallet.WalletDao;

@Database(entities = {
        Wallet.class,
        Account.class,
        Contact.class,
        PayRequest.class,
        TxnRecord.class,
        SmartContract.class,
        Node.class,
        Ride.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract WalletDao walletDao();

    public abstract AccountDao accountDao();

    public abstract ContactDao contactDao();

    public abstract PayRequestDao payRequestDao();

    public abstract TxnRecordDao txnRecordDao();

    public abstract NodeDao nodeDao();

    public abstract SmartContractDao smartContractDao();

    public abstract RideDao rideDao();

    public static AppDatabase createOrGetAppDatabase(@NonNull Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "hashdrive-database")
//                .addMigrations(MIGRATION_1_2)
                .allowMainThreadQueries()
                .build();
    }

//    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE Account "
//                    + " ADD COLUMN lastBalanceCheck INTEGER");
//        }
//    };

}
