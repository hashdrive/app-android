package org.hashdrive.database.ride;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface RideDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(Ride ride);

    @Update
    void update(Ride... rides);

    @Delete
    void delete(Ride... rides);

    @Query("SELECT * FROM Ride ORDER BY updatedAt DESC")
    List<Ride> getAllRides();

    @Query("SELECT * FROM Ride WHERE offerAddress=:offerAddress")
    Ride findRideByOfferAddress(String offerAddress);


    @Query("SELECT * FROM Ride WHERE countryCode IN(:countryCodes) AND stateDistrict IN(:stateDistricts) LIMIT 30")
    Flowable<List<Ride>> findRideOffersInArea(@NonNull List<String> countryCodes, @NonNull List<String> stateDistricts);

}
