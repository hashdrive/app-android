package org.hashdrive.database.ride;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.hashdrive.database.Converters;
import org.hashdrive.mqtt.RideOfferOuterClass;
import org.locationtech.jts.geom.LineString;
import org.threeten.bp.Instant;

import java.util.Arrays;

@Entity
public class Ride {

    @NonNull
    @PrimaryKey
    private String offerAddress;
    @NonNull
    private Instant updatedAt;
    @NonNull
    private String driverAddress;
    @NonNull
    private VehicleType vehicleType;
    @NonNull
    private Long price;
    @NonNull
    private String countryCode;
    @NonNull
    private String stateDistrict;
    @NonNull
    private LineString lineString;
    @NonNull
    private Integer freeSpots;
    @Nullable
    private Integer timeLimitStart;     // in minutes
    @Nullable
    private Integer timeLimitEnd;
    @Nullable
    private Integer distanceLimitStart; // in meters
    @Nullable
    private Integer distanceLimitEnd;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] rideOffer;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] signature;           // message signature (so that it can be republished/validated)

    public Ride() {
    }

    private Ride(@NonNull String offerAddress, @NonNull Instant updatedAt, @NonNull String driverAddress, @NonNull VehicleType vehicleType, @NonNull Long price, @NonNull String countryCode, @NonNull String stateDistrict, @NonNull LineString lineString, @NonNull Integer freeSpots, @Nullable Integer timeLimitStart, @Nullable Integer timeLimitEnd, @Nullable Integer distanceLimitStart, @Nullable Integer distanceLimitEnd, byte[] rideOffer, byte[] signature) {
        this.offerAddress = offerAddress;
        this.updatedAt = updatedAt;
        this.driverAddress = driverAddress;
        this.vehicleType = vehicleType;
        this.price = price;
        this.countryCode = countryCode;
        this.stateDistrict = stateDistrict;
        this.lineString = lineString;
        this.freeSpots = freeSpots;
        this.timeLimitStart = timeLimitStart;
        this.timeLimitEnd = timeLimitEnd;
        this.distanceLimitStart = distanceLimitStart;
        this.distanceLimitEnd = distanceLimitEnd;
        this.rideOffer = rideOffer;
        this.signature = signature;
    }

    public void updateFields(RideOfferOuterClass.RideOffer rideOffer, byte[] signature) {
        this.updatedAt = Instant.ofEpochMilli(rideOffer.getUpdatedAt());
        this.driverAddress = rideOffer.getDriverAddress();
        this.vehicleType = VehicleType.valueOf(rideOffer.getVehicleType().name());
        this.price = rideOffer.getPrice();
        this.countryCode = rideOffer.getCountryCode();
        this.stateDistrict = rideOffer.getStateDistrict();
        this.lineString = Converters.toLineStringFromString(rideOffer.getLineStringWKT());
        this.freeSpots = rideOffer.getFreeSpots();
        this.timeLimitStart = rideOffer.getTimeLimitStart();
        this.timeLimitEnd = rideOffer.getTimeLimitEnd();
        this.distanceLimitStart = rideOffer.getDistanceLimitStart();
        this.distanceLimitEnd = rideOffer.getDistanceLimitEnd();
        this.rideOffer = rideOffer.toByteArray();
        this.signature = signature;
    }

    public static Ride createRide(RideOfferOuterClass.RideOffer rideOffer, byte[] signature) {
        return new Ride(rideOffer.getOfferAddress(), Instant.ofEpochMilli(rideOffer.getUpdatedAt()), rideOffer.getDriverAddress(), VehicleType.valueOf(rideOffer.getVehicleType().name()), rideOffer.getPrice(), rideOffer.getCountryCode(), rideOffer.getStateDistrict(), Converters.toLineStringFromString(rideOffer.getLineStringWKT()), rideOffer.getFreeSpots(), rideOffer.getTimeLimitStart(), rideOffer.getTimeLimitEnd(), rideOffer.getDistanceLimitStart(), rideOffer.getDistanceLimitEnd(), rideOffer.toByteArray(), signature);
    }

    @Override
    public String toString() {
        return "Ride{" +
                "offerAddress='" + offerAddress + '\'' +
                ", updatedAt=" + updatedAt +
                ", driverAddress='" + driverAddress + '\'' +
                ", vehicleType=" + vehicleType +
                ", price=" + price +
                ", countryCode='" + countryCode + '\'' +
                ", stateDistrict='" + stateDistrict + '\'' +
                ", lineString=" + lineString +
                ", freeSpots=" + freeSpots +
                ", timeLimitStart=" + timeLimitStart +
                ", timeLimitEnd=" + timeLimitEnd +
                ", distanceLimitStart=" + distanceLimitStart +
                ", distanceLimitEnd=" + distanceLimitEnd +
                ", signature=" + Arrays.toString(signature) +
                '}';
    }

    @NonNull
    public String getOfferAddress() {
        return offerAddress;
    }

    public void setOfferAddress(@NonNull String offerAddress) {
        this.offerAddress = offerAddress;
    }

    @NonNull
    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(@NonNull Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    @NonNull
    public String getDriverAddress() {
        return driverAddress;
    }

    public void setDriverAddress(@NonNull String driverAddress) {
        this.driverAddress = driverAddress;
    }

    @NonNull
    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(@NonNull VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    @NonNull
    public Long getPrice() {
        return price;
    }

    public void setPrice(@NonNull Long price) {
        this.price = price;
    }

    @NonNull
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(@NonNull String countryCode) {
        this.countryCode = countryCode;
    }

    @NonNull
    public String getStateDistrict() {
        return stateDistrict;
    }

    public void setStateDistrict(@NonNull String stateDistrict) {
        this.stateDistrict = stateDistrict;
    }

    @NonNull
    public LineString getLineString() {
        return lineString;
    }

    public void setLineString(@NonNull LineString lineString) {
        this.lineString = lineString;
    }

    @NonNull
    public Integer getFreeSpots() {
        return freeSpots;
    }

    public void setFreeSpots(@NonNull Integer freeSpots) {
        this.freeSpots = freeSpots;
    }

    @Nullable
    public Integer getTimeLimitStart() {
        return timeLimitStart;
    }

    public void setTimeLimitStart(@Nullable Integer timeLimitStart) {
        this.timeLimitStart = timeLimitStart;
    }

    @Nullable
    public Integer getTimeLimitEnd() {
        return timeLimitEnd;
    }

    public void setTimeLimitEnd(@Nullable Integer timeLimitEnd) {
        this.timeLimitEnd = timeLimitEnd;
    }

    @Nullable
    public Integer getDistanceLimitStart() {
        return distanceLimitStart;
    }

    public void setDistanceLimitStart(@Nullable Integer distanceLimitStart) {
        this.distanceLimitStart = distanceLimitStart;
    }

    @Nullable
    public Integer getDistanceLimitEnd() {
        return distanceLimitEnd;
    }

    public void setDistanceLimitEnd(@Nullable Integer distanceLimitEnd) {
        this.distanceLimitEnd = distanceLimitEnd;
    }

    public byte[] getRideOffer() {
        return rideOffer;
    }

    public void setRideOffer(byte[] rideOffer) {
        this.rideOffer = rideOffer;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }
}
