package org.hashdrive.database.ride;

public enum VehicleType {
    CAR(0), MOTORCYCLE(1), VAN(2), BOAT(3), JETSKI(4);

    private int id;

    VehicleType(int id) {
        this.id = id;
    }
}
