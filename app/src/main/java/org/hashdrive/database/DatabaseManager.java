package org.hashdrive.database;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.InvalidProtocolBufferException;
import com.hedera.hashgraph.sdk.proto.Transaction;
import com.hedera.hashgraph.sdk.proto.TransactionBody;
import com.hedera.hashgraph.sdk.proto.TransactionID;
import com.hedera.hashgraph.sdk.proto.TransactionRecord;

import org.hashdrive.database.account.Account;
import org.hashdrive.database.account.AccountDao;
import org.hashdrive.database.contract.Contact;
import org.hashdrive.database.contract.ContactDao;
import org.hashdrive.database.node.Node;
import org.hashdrive.database.node.NodeDao;
import org.hashdrive.database.request.PayRequest;
import org.hashdrive.database.request.PayRequestDao;
import org.hashdrive.database.ride.Ride;
import org.hashdrive.database.ride.RideDao;
import org.hashdrive.database.transaction.TxnRecord;
import org.hashdrive.database.transaction.TxnRecordDao;
import org.hashdrive.database.wallet.Wallet;
import org.hashdrive.database.wallet.WalletDao;
import org.hashdrive.hederahg.account.AccountId;
import org.hashdrive.mqtt.RideOfferOuterClass;
import org.threeten.bp.Instant;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import timber.log.Timber;

public class DatabaseManager {
    private AppDatabase mAppDatabase;

    public DatabaseManager(AppDatabase mAppDatabase) {
        this.mAppDatabase = mAppDatabase;
    }

    public boolean isWalletSetup() {
        return getMasterWallet() != null;
    }

    public void createMasterWallet(@NonNull Wallet.HGCKeyType type) {
        WalletDao walletDao = mAppDatabase.walletDao();
        List<Wallet> allWallets = walletDao.getAllWallets();
        if (allWallets.isEmpty()) {
            long walletId = walletDao.insert(Wallet.createWallet(type));
            createNewAccount("Default Account");
        }
    }

    @Nullable
    public Wallet getMasterWallet() {
        WalletDao walletDao = mAppDatabase.walletDao();
        List<Wallet> allWallets = walletDao.getAllWallets();
        if (allWallets != null && allWallets.size() > 0) {
            return allWallets.get(0);
        }
        return null;
    }

    @NonNull
    public List<Account> getAllAccounts() {
        Wallet wallet = getMasterWallet();
        AccountDao accountDao = mAppDatabase.accountDao();
        List<Account> accounts = accountDao.findAccountForWallet(wallet.getWalletId());
        return accounts;
    }

    @NonNull
    public Account createNewAccount(@NonNull String accountName) {
        WalletDao walletDao = mAppDatabase.walletDao();
        AccountDao accountDao = mAppDatabase.accountDao();

        Wallet wallet = getMasterWallet();
        Account account = null;
        if (wallet != null) {
            long index = wallet.getTotalAccounts();
            accountDao.insert(Account.createAccount(wallet.getWalletId(), wallet.getKeyType(), index, accountName));
            wallet.setTotalAccounts(wallet.getTotalAccounts() + 1);
            walletDao.update(wallet);
            account = accountDao.findAccountForIndex(index).get(0);
        }
        return account;
    }

    public void saveAccount(@NonNull Account account) {
        AccountDao accountDao = mAppDatabase.accountDao();
        accountDao.update(account);
    }

    @Nullable
    public List<Contact> getAllContacts() {
        ContactDao contactDao = mAppDatabase.contactDao();
        return contactDao.getAllContacts();
    }

    @Nullable
    public Contact getContact(@NonNull String accountID) {
        ContactDao contactDao = mAppDatabase.contactDao();
        List<Contact> contacts = contactDao.findContactForAccountId(accountID);
        if (contacts != null && !contacts.isEmpty()) {
            return contacts.get(0);
        }
        return null;
    }

    public void createContact(@NonNull String accountID, @Nullable String name, boolean isVerified) {
        if (accountID == null) return;
        if (name == null) name = "";

        ContactDao contactDao = mAppDatabase.contactDao();
        Contact contact = getContact(accountID);
        if (contact == null) {
            contact = new Contact(accountID, name, isVerified);
            contactDao.insert(contact);
        } else {
            if (name != null && contact.getName() != null) {
                if (name.equals(contact.getName()) && isVerified) {
                    contact.setVerified(true);
                } else {
                    contact.setVerified(isVerified);
                }
                contact.setName(name);
                saveContact(contact);
            }
        }
    }

    public void saveContact(@NonNull Contact contact) {
        ContactDao contactDao = mAppDatabase.contactDao();
        contactDao.update(contact);
    }

    @Nullable
    public List<PayRequest> getAllRequests() {
        PayRequestDao payRequestDao = mAppDatabase.payRequestDao();
        return payRequestDao.getAllPayRequests();
    }

    public PayRequest getPayRequest(@NonNull String accountID, long amount, @Nullable String name, @Nullable String notes) {
        PayRequestDao payRequestDao = mAppDatabase.payRequestDao();
        List<PayRequest> payRequests = payRequestDao.findPayRequest(accountID, name, amount, notes);
        if (payRequests != null && !payRequests.isEmpty()) {
            return payRequests.get(0);
        }
        return null;
    }

    public void createPayRequest(@NonNull String accountID, long amount, @Nullable String name, @Nullable String notes) {
        PayRequestDao payRequestDao = mAppDatabase.payRequestDao();
        PayRequest payRequest = getPayRequest(accountID, amount, name, notes);
        if (payRequest == null) {
            payRequest = new PayRequest(0L, accountID, name, notes, amount, Instant.now());
            payRequestDao.insert(payRequest);

        } else {
            payRequest.setImportDate(Instant.now());
            savePayRequest(payRequest);
        }
    }

    public void savePayRequest(@NonNull PayRequest request) {
        PayRequestDao payRequestDao = mAppDatabase.payRequestDao();
        payRequestDao.update(request);
    }

    public void deletePayRequest(@NonNull PayRequest request) {
        PayRequestDao payRequestDao = mAppDatabase.payRequestDao();
        payRequestDao.delete(request);
    }

    public TxnRecord createTransaction(@NonNull Transaction txn, String fromAccount) {
        TxnRecordDao dao = mAppDatabase.txnRecordDao();
        TransactionID txnId;
        try {
            txnId = TransactionBody.parseFrom(txn.getBodyBytes()).getTransactionID();
            TxnRecord record = getTransaction(txnId);
            if (record == null) {
                record = new TxnRecord();
                record.setTxnId(txnId);
                record.setTxn(txn.toByteArray());
                record.setCreatedDate(Instant.now());
                record.setFromAccId(fromAccount);
                dao.insert(record);
                return dao.findRecordForTxnId(Converters.fromTransactionIdToString(record.getTxnId())).get(0);
            } else {
                return record;
            }
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException("Create transaction operation should ALWAYS either create a new or update an existing transaction!");
    }

    public TxnRecord createTransaction(@NonNull TransactionRecord transactionRecord, String fromAccount) {
        TxnRecordDao dao = mAppDatabase.txnRecordDao();
        TransactionID txnId = transactionRecord.getTransactionID();
        TxnRecord record = getTransaction(txnId);
        if (record == null) {
            record = new TxnRecord();
            record.setTxnId(txnId);
            record.setRecord( transactionRecord.toByteArray());
            record.setCreatedDate(Instant.now());
            record.setFromAccId(fromAccount);
            dao.insert(record);
            return dao.findRecordForTxnId(Converters.fromTransactionIdToString(record.getTxnId())).get(0);
        } else {
            return record;
        }
    }

    public TxnRecord getTransaction(@NonNull TransactionID txnId) {
        String txnIdStr = Converters.fromTransactionIdToString(txnId);
        TxnRecordDao dao = mAppDatabase.txnRecordDao();
        List<TxnRecord> list = dao.findRecordForTxnId(txnIdStr);
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public void updateTransaction(TxnRecord record) {
        TxnRecordDao dao = mAppDatabase.txnRecordDao();
        dao.update(record);
    }

    public List<TxnRecord> getAllTxnRecord(@Nullable Account fromAccount) {

        TxnRecordDao dao = mAppDatabase.txnRecordDao();
        List<TxnRecord> records;
        if (fromAccount == null) {
            records = dao.getAllRecords();
        } else {
            if (fromAccount.getAccountId() != null) {
                records = dao.findRecordForAccountId(fromAccount.getAccountId().toString());
            } else {
                records = new ArrayList<>();
            }
        }

        if (records != null && !records.isEmpty()) {
            List<Account> accounts = getAllAccounts();
            for (TxnRecord record : records) {
                record.parseProperties();
                if (record.getFromAccId() != null) {
                    Account account = getAccount(accounts, record.getFromAccId());
                    if (account != null) {
                        record.setFromAccount( account.getContact());
                    } else {
                        Contact contact = getContact(record.getFromAccId());
                        if (contact != null) {
                            record.setFromAccount( contact);
                        }
                    }
                }

                if (record.getToAccountId() != null) {
                    Account account = getAccount(accounts, record.getToAccountId());
                    if (account != null) {
                        record.setToAccount(account.getContact());
                    } else {
                        Contact contact = getContact(record.getToAccountId());
                        if (contact != null) {
                            record.setToAccount( contact);
                        }
                    }
                    if (fromAccount != null && fromAccount.getAccountId() != null) {
                        if (record.getToAccountId().equals(fromAccount.getAccountId().toString())) {
                            record.setPositive(true);
                        }
                    }
                }
            }
        }

        return records;
    }

    public void deleteTxnRecord(TxnRecord record) {
        TxnRecordDao dao = mAppDatabase.txnRecordDao();
        dao.delete(record);
    }

    private static Account getAccount(List<Account> accounts, String accountId) {
        try {
            AccountId accountID = AccountId.fromString(accountId);
            for (Account account : accounts) {
                AccountId accId = account.getAccountId();
                if (accId != null && accId.toString().equals(accountID.toString())) {
                    return account;
                }
            }
        } catch (IllegalArgumentException e) {
            Timber.e(e, "getAccount failed!");
        }
        return null;
    }

    public Node getNode(@NonNull String host) {
        NodeDao dao = mAppDatabase.nodeDao();
        List<Node> list = dao.findNodeForHost(host);
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    public void createNode(@NonNull Node node) {
        NodeDao dao = mAppDatabase.nodeDao();
        Node record = getNode(node.getHost());
        if (record == null) {
            dao.insert(node);
        }
    }

    public void updateNode(@NonNull Node node) {
        NodeDao dao = mAppDatabase.nodeDao();
        dao.update(node);
    }

    public List<Node> getAllNodes(Boolean activeOnly) {
        NodeDao dao = mAppDatabase.nodeDao();
        if (activeOnly)
            return dao.findActiveNodes();
        else
            return dao.getAllNodes();
    }

    public Ride getRideOffer(String rideOfferAddress) {
        RideDao dao = mAppDatabase.rideDao();
        return dao.findRideByOfferAddress(rideOfferAddress);
    }

    public void addOrUpdateRideOffer(RideOfferOuterClass.RideOffer rideOffer, byte[] signature) {
        Ride ride = getRideOffer(rideOffer.getOfferAddress());
        if(ride != null) {
            if(ride.getUpdatedAt().toEpochMilli() < rideOffer.getUpdatedAt()) {
                Timber.d("Updating existing ride offer...");
                // update ride record only if rideOffer is newer
                ride.updateFields(rideOffer, signature);
                mAppDatabase.rideDao().update(ride);
            } else {
                Timber.d("Existing ride offer is already up to date!");
            }
        } else {
            Timber.d("Creating a new ride offer...");
            ride = Ride.createRide(rideOffer, signature);
            mAppDatabase.rideDao().insert(ride);
        }
    }

    public Flowable<List<Ride>> findRideOffersInArea(@NonNull List<String> countryCodes, @NonNull List<String> stateDistricts) {
        RideDao dao = mAppDatabase.rideDao();
        return dao.findRideOffersInArea(countryCodes, stateDistricts);
    }
}
