package org.hashdrive.database.contract;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Contact {
    @PrimaryKey
    @NonNull
    private String accountId;
    private String name; // optional
    private Boolean isVerified = false;

    public Contact() {
    }

    @Ignore
    public Contact(@NonNull String accountId, String name, Boolean isVerified) {
        this.accountId = accountId;
        this.name = name;
        this.isVerified = isVerified;
    }

    @NonNull
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(@NonNull String accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getVerified() {
        return isVerified;
    }

    public void setVerified(Boolean verified) {
        isVerified = verified;
    }
}
