package org.hashdrive.database.contract;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ContactDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(Contact contact);

    @Update
    void update(Contact... contacts);

    @Delete
    void delete(Contact... contacts);

    @Query("SELECT * FROM Contact")
    List<Contact> getAllContacts();

    @Query("SELECT * FROM Contact WHERE accountId=:accountId")
    List<Contact> findContactForAccountId(String accountId);
}
