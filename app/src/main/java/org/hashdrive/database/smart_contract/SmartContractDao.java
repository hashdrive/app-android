package org.hashdrive.database.smart_contract;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SmartContractDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(SmartContract contract);

    @Update
    void update(SmartContract... contracts);

    @Delete
    void delete(SmartContract... contracts);

    @Query("SELECT * FROM SmartContract")
    List<SmartContract> getAllContracts();

    @Query("SELECT * FROM SmartContract WHERE accountIndex=:accountIndex")
    List<SmartContract> findContactsForAccountAtIndex(Long accountIndex);

}
