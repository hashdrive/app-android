package org.hashdrive.database.smart_contract;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import org.hashdrive.database.account.Account;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = Account.class,
        parentColumns = "accountIndex",
        childColumns = "accountIndex", onDelete = CASCADE),
        primaryKeys = {"accountIndex", "contractID"})

public class SmartContract {
    @NonNull
    public long accountIndex;
    public long balance;

    @NonNull
    public String contractID;
    public String name;
    public String symbol;
    public int decimals;
}

