package org.hashdrive.database.transaction;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.hedera.hashgraph.sdk.proto.AccountAmount;
import com.hedera.hashgraph.sdk.proto.AccountID;
import com.hedera.hashgraph.sdk.proto.CryptoTransferTransactionBody;
import com.hedera.hashgraph.sdk.proto.Response;
import com.hedera.hashgraph.sdk.proto.ResponseCodeEnum;
import com.hedera.hashgraph.sdk.proto.Transaction;
import com.hedera.hashgraph.sdk.proto.TransactionBody;
import com.hedera.hashgraph.sdk.proto.TransactionID;
import com.hedera.hashgraph.sdk.proto.TransactionReceipt;
import com.hedera.hashgraph.sdk.proto.TransactionRecord;
import com.hedera.hashgraph.sdk.proto.TransferList;

import org.hashdrive.database.contract.Contact;
import org.threeten.bp.Instant;

import timber.log.Timber;

@Entity
public class TxnRecord {

    @NonNull
    @PrimaryKey
    private TransactionID txnId;

    private String fromAccId = null;
    private String toAccId = null;
    private Instant createdDate = null;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] txn = null;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] receipt;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] record = null;

    @Ignore
    private Contact fromAccount = null;

    @Ignore
    private Contact toAccount = null;

    @Ignore
    private Long amount = 0L;

    @Ignore
    private Long fee = 0L;

    @Ignore
    private String toAccountId = null;

    @Ignore
    private String notes = "";

    @Ignore
    private Boolean isPositive = false;

    @Ignore
    private Status status = Status.UNKNOWN;

    public TxnRecord() {
    }

    @NonNull
    public TransactionID getTxnId() {
        return txnId;
    }

    public void setTxnId(@NonNull TransactionID txnId) {
        this.txnId = txnId;
    }

    public String getFromAccId() {
        return fromAccId;
    }

    public void setFromAccId(String fromAccId) {
        this.fromAccId = fromAccId;
    }

    public String getToAccId() {
        return toAccId;
    }

    public void setToAccId(String toAccId) {
        this.toAccId = toAccId;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public byte[] getTxn() {
        return txn;
    }

    public void setTxn(byte[] txn) {
        this.txn = txn;
    }

    public byte[] getReceipt() {
        return receipt;
    }

    public void setReceipt(byte[] receipt) {
        this.receipt = receipt;
    }

    public byte[] getRecord() {
        return record;
    }

    public void setRecord(byte[] record) {
        this.record = record;
    }

    public Contact getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Contact fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Contact getToAccount() {
        return toAccount;
    }

    public void setToAccount(Contact toAccount) {
        this.toAccount = toAccount;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getFee() {
        return fee;
    }

    public void setFee(Long fee) {
        this.fee = fee;
    }

    public String getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(String toAccountId) {
        this.toAccountId = toAccountId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Boolean getPositive() {
        return isPositive;
    }

    public void setPositive(Boolean positive) {
        isPositive = positive;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    public void setTxn(Transaction transaction) {
        txn = transaction.toByteArray();
    }

    public void setReceipt(TransactionReceipt receiptResponse) {
        receipt = receiptResponse.toByteArray();
    }

    public void setRecord(Response recordResponse) {
        receipt = recordResponse.toByteArray();
    }

    public void parseProperties() {
        if (record == null) {
            parseTxn();
            parseReceipt();
        } else {
            parseRecord();
        }
    }

    private void parseRecord() {
        if (record == null)
            return;
        TransactionRecord transactionRecord = null;


        try {
            transactionRecord = TransactionRecord.parseFrom(record);
        } catch (InvalidProtocolBufferException e) {
            Timber.e(e, "Parse transaction record failed!");
        }


        if (transactionRecord != null) {
            fee = transactionRecord.getTransactionFee();
            notes = transactionRecord.getMemo();
            TransactionReceipt receiptRes = transactionRecord.getReceipt();

            if (receiptRes != null) {
                if (receiptRes.getStatus() == ResponseCodeEnum.SUCCESS) {
                    status = Status.SUCCESS;
                } else if (receiptRes.getStatus() == ResponseCodeEnum.UNRECOGNIZED || receiptRes.getStatus() == ResponseCodeEnum.UNKNOWN) {
                    // do nothing
                } else {
                    status = Status.FAILED;
                }
            }


            TransferList transferList = transactionRecord.getTransferList();
            if (transferList != null) {
                for (AccountAmount accountAmount : transferList.getAccountAmountsList()) {
                    if (accountAmount.getAmount() > 0) {
                        toAccountId = getAccountIdAsString(accountAmount.getAccountID());
                        amount = accountAmount.getAmount();
                    } else {
                        fromAccId = getAccountIdAsString(accountAmount.getAccountID());
                    }
                }
            }

        }
    }

    private void parseTxn() {
        if (txn == null)
            return;

        Transaction transaction = null;

        try {
            transaction = Transaction.parseFrom(txn);
        } catch (InvalidProtocolBufferException e) {
            Timber.e(e, "Parse transaction failed!");
        }


        if (transaction != null) {
            TransactionBody transactionBody = getTxnBody(transaction);

            fee = transactionBody.getTransactionFee();
            notes = transactionBody.getMemo();
            CryptoTransferTransactionBody cryptoTransfer = transactionBody.getCryptoTransfer();
            if (cryptoTransfer != null) {
                TransferList transferList = cryptoTransfer.getTransfers();
                for (AccountAmount accountAmount : transferList.getAccountAmountsList()) {
                    if (accountAmount.getAmount() > 0) {
                        toAccountId = getAccountIdAsString(accountAmount.getAccountID());
                        amount = accountAmount.getAmount();
                    } else {
                        fromAccId = getAccountIdAsString(accountAmount.getAccountID());
                    }
                }
            }
        }
    }

    private void parseReceipt() {
        if (receipt == null)
            return;

        TransactionReceipt receiptRes = null;

        try {
            receiptRes = TransactionReceipt.parseFrom(receipt);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }


        if (receiptRes != null) {
            if (receiptRes.getStatus() == ResponseCodeEnum.SUCCESS) {
                status = Status.SUCCESS;
            } else if (receiptRes.getStatus() == ResponseCodeEnum.UNRECOGNIZED || receiptRes.getStatus() == ResponseCodeEnum.UNKNOWN) {
                // do nothing
            } else {
                status = Status.FAILED;
            }
        }
    }


    TransactionBody getTxnBody(Transaction transaction) {
        ByteString bodyBytes = transaction.getBodyBytes();
        if (bodyBytes != null && bodyBytes.size() > 0) {
            try {
                return TransactionBody.parseFrom(bodyBytes);
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }
        return transaction.getBody();
    }

    String getAccountIdAsString(AccountID accountID) {
        return String.format("%s.%s.%s", accountID.getRealmNum(), accountID.getShardNum(), accountID.getAccountNum());
    }


    enum Status {
        UNKNOWN, FAILED, SUCCESS
    }

}
