package org.hashdrive.database.transaction;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface TxnRecordDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(TxnRecord record);

    @Update
    void update(TxnRecord ...records);

    @Delete
    void delete(TxnRecord ...records);

    @Query("SELECT * FROM TxnRecord ORDER BY createdDate desc")
    List<TxnRecord> getAllRecords();

    @Query("SELECT * FROM TxnRecord WHERE fromAccId=:accId ORDER BY createdDate desc")
    List<TxnRecord> findRecordForAccountId(String accId);

    @Query("SELECT * FROM TxnRecord WHERE txnId=:txnId")
    List<TxnRecord> findRecordForTxnId(String txnId);

    @Query("DELETE FROM TxnRecord")
    void deleteAll();
}
