package org.hashdrive.database.node;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.hashdrive.hederahg.account.AccountId;
import org.threeten.bp.Instant;

@Entity
public class Node {
    @PrimaryKey(autoGenerate = true)
    private Long nodeId = 0L;
    private Long realmNum = 0L;
    private Long shardNum = 0L;
    private Long accountNum = 0L;
    private String host = ""; // optional
    private int port = 0;
    private String status = ""; // optional
    private Instant lastCheckAt = null; // optional
    private Boolean disabled = false;

    public Node() {
    }

    private void setAccountID(Long realmId, Long shardId, Long accountId) {
        this.realmNum = realmId;
        this.shardNum = shardId;
        this.accountNum = accountId;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public Long getRealmNum() {
        return realmNum;
    }

    public void setRealmNum(Long realmNum) {
        this.realmNum = realmNum;
    }

    public Long getShardNum() {
        return shardNum;
    }

    public void setShardNum(Long shardNum) {
        this.shardNum = shardNum;
    }

    public Long getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(Long accountNum) {
        this.accountNum = accountNum;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getLastCheckAt() {
        return lastCheckAt;
    }

    public void setLastCheckAt(Instant lastCheckAt) {
        this.lastCheckAt = lastCheckAt;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    void setAccountID(AccountId accountID) {
        setAccountID(accountID.getRealmNum(), accountID.getShardNum(), accountID.getAccountNum());
    }

    AccountId accountID() {
        return new AccountId(realmNum, shardNum, accountNum);
    }

}
