package org.hashdrive.database.node;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface NodeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(Node node);

    @Update
    void update(Node... nodes);

    @Delete
    void delete(Node... nodes);

    @Query("SELECT * FROM Node")
    List<Node> getAllNodes();

    @Query("SELECT * FROM Node WHERE disabled = 0")
    List<Node> findActiveNodes();

    @Query("SELECT * FROM Node WHERE host=:host")
    List<Node> findNodeForHost(String host);
}
