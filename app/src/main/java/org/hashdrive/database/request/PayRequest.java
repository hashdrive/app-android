package org.hashdrive.database.request;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.threeten.bp.Instant;

@Entity
public class PayRequest {
    @PrimaryKey(autoGenerate = true)
    private Long requestId;
    private String accountId;
    private String name; // optional
    private String notes; // optional
    private Long amount = 0L;
    private Instant importDate;

    public PayRequest() {
    }

    @Ignore
    public PayRequest(Long requestId, String accountId, String name, String notes, Long amount, Instant importDate) {
        this.requestId = requestId;
        this.accountId = accountId;
        this.name = name;
        this.notes = notes;
        this.amount = amount;
        this.importDate = importDate;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Instant getImportDate() {
        return importDate;
    }

    public void setImportDate(Instant importDate) {
        this.importDate = importDate;
    }
}
