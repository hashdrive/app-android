package org.hashdrive.database.request;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PayRequestDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(PayRequest payRequest);

    @Update
    void update(PayRequest... payRequests);

    @Delete
    void delete(PayRequest... payRequests);

    @Query("SELECT * FROM PayRequest ORDER BY importDate DESC")
    List<PayRequest> getAllPayRequests();

    @Query("SELECT * FROM PayRequest WHERE accountId=:accountId")
    List<PayRequest> findPayRequest(String accountId);

    @Query("SELECT * FROM PayRequest WHERE accountId=:accountId AND name=:name AND amount=:amount AND notes=:notes")
    List<PayRequest> findPayRequest(String accountId, String name, Long amount, String notes);

}
