package org.hashdrive.database.wallet;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Wallet {
    @PrimaryKey(autoGenerate = true)
    private Long walletId = 0L;
    private Long totalAccounts = 0L;
    private String keyType = "";

    public Wallet() {
    }

    @Ignore
    public Wallet(Long walletId, Long totalAccounts, String keyType) {
        this.walletId = walletId;
        this.totalAccounts = totalAccounts;
        this.keyType = keyType;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public void setTotalAccounts(Long totalAccounts) {
        this.totalAccounts = totalAccounts;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }

    public Long getWalletId() {
        return walletId;
    }

    public Long getTotalAccounts() {
        return totalAccounts;
    }

    public String getKeyType() {
        return keyType;
    }

    public enum HGCKeyType {
        ECDSA384, ED25519, RSA3072
    }

    public static Wallet createWallet(HGCKeyType type) {
        return new Wallet(0L, 0L, getKeyTypeAsString(type));
    }

    public HGCKeyType getHGCKeyType() {
        return keyTypeFrom(keyType);
    }

    public static HGCKeyType keyTypeFrom(String keyType) {
        switch (keyType) {
            case "ED25519":
                return HGCKeyType.ED25519;
            case "RSA3072":
                return HGCKeyType.RSA3072;
            case "ECDSA384":
                return HGCKeyType.ECDSA384;
        }

        return HGCKeyType.ECDSA384;
    }

    static String getKeyTypeAsString(HGCKeyType type) {
        if (type == HGCKeyType.ED25519) {
            return "ED25519";
        } else if (type == HGCKeyType.RSA3072) {
            return "RSA3072";
        } else {
            return "ECDSA384";
        }
    }

}
