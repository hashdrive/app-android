package org.hashdrive.database.wallet;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface WalletDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(Wallet wallet);

    @Update
    void update(Wallet... wallets);

    @Delete
    void delete(Wallet... wallets);

    @Query("SELECT * FROM Wallet")
    List<Wallet> getAllWallets();

    @Query("SELECT * FROM Wallet WHERE walletId=:walletId")
    List<Wallet> findForWalletId(Long walletId);
}
