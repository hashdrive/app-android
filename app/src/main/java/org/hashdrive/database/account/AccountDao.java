package org.hashdrive.database.account;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import org.hashdrive.database.account.Account;

import java.util.List;

@Dao
public interface AccountDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Account account);

    @Update
    void update(Account... accounts);

    @Delete
    void delete(Account... accounts);

    @Query("SELECT * FROM Account")
    List<Account> getAllAccounts();

    @Query("SELECT * FROM Account WHERE walletId=:walletId")
    List<Account> findAccountForWallet(Long walletId);

    @Query("SELECT * FROM Account WHERE accountIndex=:index")
    List<Account> findAccountForIndex(Long index);
}
