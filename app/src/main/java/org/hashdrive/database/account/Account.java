package org.hashdrive.database.account;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import org.hashdrive.database.contract.Contact;
import org.hashdrive.database.wallet.Wallet;
import org.hashdrive.hederahg.account.AccountId;
import org.threeten.bp.Instant;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = {@ForeignKey(entity = Wallet.class, parentColumns = {"walletId"}, childColumns = {"walletId"}, onDelete = CASCADE)},
        indices = {@Index("accountIndex"), @Index("walletId")})
public class Account {
    private Long walletId = 0L;

    private String keyType = "";
    @PrimaryKey
    private Long accountIndex = 0L;
    private String name = "";
    private Long balance = 0L;
    private Instant lastBalanceCheck = null;
    private Long realmNum = 0L;
    private Long shardNum = 0L;
    private Long accountNum = 0L;
    private Boolean isArchived = false;
    private Boolean isHidden = false;

    public Account() {
    }

    @Ignore
    public Account(Long walletId, String keyType, Long accountIndex, String name) {
        this.walletId = walletId;
        this.keyType = keyType;
        this.accountIndex = accountIndex;
        this.name = name;
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public String getKeyType() {
        return keyType;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }

    public Long getAccountIndex() {
        return accountIndex;
    }

    public void setAccountIndex(Long accountIndex) {
        this.accountIndex = accountIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Instant getLastBalanceCheck() {
        return lastBalanceCheck;
    }

    public void setLastBalanceCheck(Instant lastBalanceCheck) {
        this.lastBalanceCheck = lastBalanceCheck;
    }

    public Long getRealmNum() {
        return realmNum;
    }

    public void setRealmNum(Long realmNum) {
        this.realmNum = realmNum;
    }

    public Long getShardNum() {
        return shardNum;
    }

    public void setShardNum(Long shardNum) {
        this.shardNum = shardNum;
    }

    public Long getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(Long accountNum) {
        this.accountNum = accountNum;
    }

    public Boolean getArchived() {
        return isArchived;
    }

    public void setArchived(Boolean archived) {
        isArchived = archived;
    }

    public Boolean getHidden() {
        return isHidden;
    }

    public void setHidden(Boolean hidden) {
        isHidden = hidden;
    }

    public AccountId getAccountId() {
        if(accountNum == 0) {
            return null;
        } else {
            return new AccountId(shardNum, realmNum, accountNum);
        }
    }

    public Contact getContact() {
        AccountId accountId = getAccountId();
        return (accountId == null? null : new Contact(getAccountId().toString(), name, true));
    }

    @Ignore
    public static Account createAccount(Long walletId, String keyType, Long accountIndex, String name) {
        return new Account(walletId, keyType, accountIndex, name);
    }
}
