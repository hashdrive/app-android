# Android application - Hashdrive 

A dApp for p2p ride sharing (sharing economy, without intermediaries) where drivers can make offers and users can join rides.

> Notice that this application is at en early stage of development.

## The idea

Everybody can use dApp and its services.

There are primarily two roles: driver and passenger.

Driver can create an offer with specific parameters such as:

- type of the vehicle (car, motorcycle, van, boat),
- starting point and destination,
- price (per passenger),
- number of free spots.


Passenger can:

- search through ride offers, 
- join the ride,
- make an offer (in form of additional payment) for specific route with custom parameters such as: pickup and drop-off location.

## Goal (MVP)

The application should enable users to:

- interact with the existing offers (published in form of [smart contract](https://bitbucket.org/hashdrive/truffle-project/src/master/) on Hedera network),
- evaluate drivers and passengers based on thiers reputation (ride history),
- find offers nearby (if the location service is enabled),
- see interesting stats (average cost per mile/km in the area, average route distance in the area, etc).

## Future

In the future dapp could also include other services (e.g.: guided trips where travellers can place orders/inquiries for their preferred destinations).

For documentation use [wiki](https://bitbucket.org/hashdrive/app-android/wiki/Home).

For task/issues management use [issue tracker](https://bitbucket.org/hashdrive/app-android/issues).