package org.hashdrive.hederahg;

import com.google.common.annotations.VisibleForTesting;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.hedera.hashgraph.sdk.proto.CryptoServiceGrpc;
import com.hedera.hashgraph.sdk.proto.FileServiceGrpc;
import com.hedera.hashgraph.sdk.proto.ResponseCodeEnum;
import com.hedera.hashgraph.sdk.proto.SignatureMap;
import com.hedera.hashgraph.sdk.proto.SignaturePair;
import com.hedera.hashgraph.sdk.proto.SignaturePairOrBuilder;
import com.hedera.hashgraph.sdk.proto.SmartContractServiceGrpc;
import com.hedera.hashgraph.sdk.proto.TransactionBody;
import com.hedera.hashgraph.sdk.proto.TransactionBodyOrBuilder;
import com.hedera.hashgraph.sdk.proto.TransactionResponse;

import org.bouncycastle.util.encoders.Hex;
import org.hashdrive.hederahg.account.AccountId;
import org.hashdrive.hederahg.crypto.ed25519.Ed25519PrivateKey;
import org.hashdrive.hederahg.crypto.ed25519.Ed25519Signature;

import java.util.HashSet;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import io.grpc.Channel;
import io.grpc.MethodDescriptor;

public final class Transaction extends HederaCall<com.hedera.hashgraph.sdk.proto.Transaction, TransactionResponse, TransactionId, Transaction> {

    private final io.grpc.MethodDescriptor<com.hedera.hashgraph.sdk.proto.Transaction, com.hedera.hashgraph.sdk.proto.TransactionResponse> methodDescriptor;
    final com.hedera.hashgraph.sdk.proto.Transaction.Builder inner;
    final com.hedera.hashgraph.sdk.proto.SignatureMap.Builder sigMapBuilder;
    final com.hedera.hashgraph.sdk.proto.AccountID nodeAccountId;
    final com.hedera.hashgraph.sdk.proto.TransactionID transactionId;

    @Nullable
    private final Client client;

    private static final int MAX_RETRY_ATTEMPTS = 100;
    private static final int RECEIPT_RETRY_DELAY = 100;

    private static final int PREFIX_LEN = 6;

    Transaction(
        @Nullable Client client,
        com.hedera.hashgraph.sdk.proto.Transaction.Builder inner,
        com.hedera.hashgraph.sdk.proto.AccountID nodeAccountId,
        com.hedera.hashgraph.sdk.proto.TransactionID transactionId,
        MethodDescriptor<com.hedera.hashgraph.sdk.proto.Transaction, TransactionResponse> methodDescriptor)
    {
        super();
        this.client = client;
        this.inner = inner;
        this.nodeAccountId = nodeAccountId;
        this.transactionId = transactionId;
        this.methodDescriptor = methodDescriptor;
        this.sigMapBuilder = SignatureMap.newBuilder();
    }

    public static Transaction fromBytes(Client client, byte[] bytes) throws InvalidProtocolBufferException {
        com.hedera.hashgraph.sdk.proto.Transaction inner = com.hedera.hashgraph.sdk.proto.Transaction.parseFrom(bytes);
        TransactionBody body = TransactionBody.parseFrom(inner.getBodyBytes());

        return new Transaction(client, inner.toBuilder(), body.getNodeAccountID(), body.getTransactionID(), methodForTxnBody(body));
    }

    @VisibleForTesting
    public static Transaction fromBytes(byte[] bytes) throws InvalidProtocolBufferException {
        com.hedera.hashgraph.sdk.proto.Transaction inner = com.hedera.hashgraph.sdk.proto.Transaction.parseFrom(bytes);
        TransactionBody body = TransactionBody.parseFrom(inner.getBodyBytes());

        return new Transaction(null, inner.toBuilder(), body.getNodeAccountID(), body.getTransactionID(), methodForTxnBody(body));
    }

    public Transaction sign(Ed25519PrivateKey privateKey) {
        ByteString pubKey = ByteString.copyFrom(
                privateKey.getPublicKey()
                        .toBytes());

        SignatureMap.Builder sigMap = sigMapBuilder;

        for (int i = 0; i < sigMap.getSigPairCount(); i++) {
            ByteString pubKeyPrefix = sigMap.getSigPair(i)
                    .getPubKeyPrefix();

            if (pubKey.startsWith(pubKeyPrefix)) {
                throw new IllegalArgumentException("transaction already signed with key: " + privateKey.toString());
            }
        }

        byte[] signature = Ed25519Signature.forMessage(
                privateKey,
                inner.getBodyBytes()
                        .toByteArray())
                .toBytes();

        sigMap.addSigPair(
            SignaturePair.newBuilder()
                .setPubKeyPrefix(pubKey)
                .setEd25519(ByteString.copyFrom(signature))
                .build());

        return this;
    }

    public TransactionId getId() {
        return new TransactionId(transactionId);
    }

    public TransactionReceipt queryReceipt() throws HederaException {
        return new TransactionReceiptQuery(Objects.requireNonNull(client))
            .setTransactionId(getId())
            .execute();
    }

    public void queryReceiptAsync(Consumer<TransactionReceipt> onReceipt, Consumer<HederaThrowable> onError) {
        // TODO: 2019-06-23 replace with rx-java call
//        new TransactionReceiptQuery(Objects.requireNonNull(client))
//            .setTransactionId(getId())
//            .executeAsync(onReceipt, onError);
    }

    @Override
    public com.hedera.hashgraph.sdk.proto.Transaction toProto() {
        validate();
        return inner.build();
    }

    public com.hedera.hashgraph.sdk.proto.Transaction toProto(boolean requireSignature) {
        validate(requireSignature);
        return inner.build();
    }

    @Override
    protected MethodDescriptor<com.hedera.hashgraph.sdk.proto.Transaction, TransactionResponse> getMethod() {
        return methodDescriptor;
    }

    @Override
    protected Channel getChannel() {
        Objects.requireNonNull(client, "Transaction.client must be non-null in regular use");

        Node channel = client.getNodeForId(new AccountId(nodeAccountId));
        Objects.requireNonNull(channel, "Transaction.nodeAccountId not found on Client");

        return channel.getChannel();
    }

    @Override
    public final void validate() {
        SignatureMap sigMap = inner.getSigMap();

        if (sigMap.getSigPairCount() < 2) {
            if (sigMap.getSigPairCount() == 0) {
                addValidationError("Transaction requires at least one signature");
            } // else contains one signature which is fine
        } else {
            HashSet<ByteString> publicKeys = new HashSet<>();

            for (int i = 0; i < sigMap.getSigPairCount(); i++) {
                SignaturePairOrBuilder sig = sigMap.getSigPairOrBuilder(i);
                ByteString pubKeyPrefix = sig.getPubKeyPrefix();

                if (!publicKeys.add(pubKeyPrefix)) {
                    addValidationError("duplicate signing key: " + Hex.toHexString(getPrefix(pubKeyPrefix).toByteArray()) + "...");
                }
            }
        }

        checkValidationErrors("Transaction failed validation");
    }

    protected void validate(boolean requireSignature) {
        if (requireSignature) {
            validate();
            return;
        }

        checkValidationErrors("Transaction failed validation");
    }

    @Override
    protected TransactionId mapResponse(TransactionResponse response) throws HederaException {
        HederaException.throwIfExceptional(response.getNodeTransactionPrecheckCode());
        return new TransactionId(transactionId);
    }

    private <T> T executeAndWaitFor(CheckedFunction<TransactionReceipt, T> mapReceipt)
            throws HederaException, HederaNetworkException
    {
        // kickoff the transaction
        execute();

        for (int attempt = 0; attempt < MAX_RETRY_ATTEMPTS; attempt++) {
            TransactionReceipt receipt = queryReceipt();
            ResponseCodeEnum receiptStatus = receipt.getStatus();

            // if we're fetching a record it returns `RECORD_NOT_FOUND` instead of `UNKNOWN`
            if (receiptStatus == ResponseCodeEnum.UNKNOWN) {
                // If the receipt is UNKNOWN this means that the server has not finished
                // processing the transaction
                try {
                    Thread.sleep(RECEIPT_RETRY_DELAY * attempt);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            } else {
                HederaException.throwIfExceptional(receiptStatus);
                return mapReceipt.apply(receipt);
            }
        }

        throw new HederaException(ResponseCodeEnum.UNKNOWN);
    }

    /**
     * Execute this transaction and wait for its receipt to be generated.
     *
     * If the receipt does not become available after a few seconds, {@link HederaException} is thrown.
     *
     * @return the receipt for the transaction
     * @throws HederaException for any response code that is not {@link ResponseCodeEnum#SUCCESS}
     * @throws HederaNetworkException
     * @throws RuntimeException if an {@link java.lang.InterruptedException} is thrown while waiting for the receipt.
     */
    public final TransactionReceipt executeForReceipt() throws HederaException, HederaNetworkException {
        return executeAndWaitFor(receipt -> receipt);
    }

    /**
     * Execute this transaction and wait for its record to be generated.
     *
     * If the record does not become available after a few seconds, {@link HederaException} is thrown.
     *
     * @return the receipt for the transaction
     * @throws HederaException for any response code that is not {@link ResponseCodeEnum#SUCCESS}
     * @throws HederaNetworkException
     * @throws RuntimeException if an {@link java.lang.InterruptedException} is thrown while waiting for the receipt.
     */
    public TransactionRecord executeForRecord() throws HederaException, HederaNetworkException {
        return executeAndWaitFor(
            receipt -> new TransactionRecordQuery(getClient()).setTransactionId(getId()).execute());
    }

    public void executeForReceiptAsync(Consumer<TransactionReceipt> onSuccess, Consumer<HederaThrowable> onError) {
        AsyncReceiptHandler handler = new AsyncReceiptHandler(onSuccess, onError);

        executeAsync(id -> handler.tryGetReceipt(), onError);
    }

    /**
     * Equivalent to {@link #executeForReceiptAsync(Consumer, Consumer)} but providing {@code this}
     * to the callback for additional context.
     */
    public final void executeForReceiptAsync(BiConsumer<Transaction, TransactionReceipt> onSuccess, BiConsumer<Transaction, HederaThrowable> onError) {
//        TODO: 2019-06-23 replace with rx-java call
//        executeForReceiptAsync(r -> onSuccess.accept(this, r), e -> onError.accept(this, e));
    }

    public void executeForRecordAsync(Consumer<TransactionRecord> onSuccess, Consumer<HederaThrowable> onError) {
        final TransactionRecordQuery recordQuery = new TransactionRecordQuery(getClient())
                .setTransactionId(getId())
                .setPaymentDefault();
//        TODO: 2019-06-23 replace with rx-java call

//        AsyncReceiptHandler handler = new AsyncReceiptHandler((receipt) ->
//                recordQuery.executeAsync(onSuccess, onError),
//                onError);
//
//        executeAsync(id -> handler.tryGetReceipt(), onError);
    }

    /**
     * Equivalent to {@link #executeForRecordAsync(Consumer, Consumer)} but providing {@code this}
     * to the callback for additional context.
     */
    public final void executeForRecordAsync(BiConsumer<Transaction, TransactionRecord> onSuccess, BiConsumer<Transaction, HederaThrowable> onError) {
//        TODO: 2019-06-23 replace with rx-java call
//        executeForRecordAsync(r -> onSuccess.accept(this, r), e -> onError.accept(this, e));
    }
    
    public byte[] toBytes() {
        return toProto().toByteArray();
    }

    public byte[] toBytes(boolean requiresSignature) {
        return toProto(requiresSignature).toByteArray();
    }

    private Client getClient() {
        return Objects.requireNonNull(client);
    }

    private static ByteString getPrefix(ByteString byteString) {
        if (byteString.size() <= PREFIX_LEN) {
            return byteString;
        }

        return byteString.substring(0, PREFIX_LEN);
    }

    private static MethodDescriptor<com.hedera.hashgraph.sdk.proto.Transaction, TransactionResponse> methodForTxnBody(TransactionBodyOrBuilder body) {
        switch (body.getDataCase()) {
        case SYSTEMDELETE:
            return FileServiceGrpc.getSystemDeleteMethod();
        case SYSTEMUNDELETE:
            return FileServiceGrpc.getSystemUndeleteMethod();
        case CONTRACTCALL:
            return SmartContractServiceGrpc.getContractCallMethodMethod();
        case CONTRACTCREATEINSTANCE:
            return SmartContractServiceGrpc.getCreateContractMethod();
        case CONTRACTUPDATEINSTANCE:
            return SmartContractServiceGrpc.getUpdateContractMethod();
        case CONTRACTDELETEINSTANCE:
            return SmartContractServiceGrpc.getDeleteContractMethod();
        case CRYPTOADDCLAIM:
            return CryptoServiceGrpc.getAddClaimMethod();
        case CRYPTOCREATEACCOUNT:
            return CryptoServiceGrpc.getCreateAccountMethod();
        case CRYPTODELETE:
            return CryptoServiceGrpc.getCryptoDeleteMethod();
        case CRYPTODELETECLAIM:
            return CryptoServiceGrpc.getDeleteClaimMethod();
        case CRYPTOTRANSFER:
            return CryptoServiceGrpc.getCryptoTransferMethod();
        case CRYPTOUPDATEACCOUNT:
            return CryptoServiceGrpc.getUpdateAccountMethod();
        case FILEAPPEND:
            return FileServiceGrpc.getAppendContentMethod();
        case FILECREATE:
            return FileServiceGrpc.getCreateFileMethod();
        case FILEDELETE:
            return FileServiceGrpc.getDeleteFileMethod();
        case FILEUPDATE:
            return FileServiceGrpc.getUpdateFileMethod();
        case DATA_NOT_SET:
            throw new IllegalArgumentException("method not set");
        default:
            throw new IllegalArgumentException("unsupported method");
        }
    }

    private final class AsyncReceiptHandler {
        private final Consumer<TransactionReceipt> onReceipt;
        private final Consumer<HederaThrowable> onError;

        private int attemptsLeft = MAX_RETRY_ATTEMPTS;
        private volatile boolean receiptEmitted = false;

        private AsyncReceiptHandler(
            Consumer<TransactionReceipt> onReceipt, Consumer<HederaThrowable> onError)
        {
            this.onReceipt = onReceipt;
            this.onError = onError;
        }

        private void tryGetReceipt() {
            attemptsLeft -= 1;
            queryReceiptAsync(this::handleReceipt, onError);
        }

        private void handleReceipt(TransactionReceipt receipt) {
//          TODO: 2019-06-23 remove after using rx-java for async handling
            throw new RuntimeException("Receipt not handled!");
//            if (receipt.getStatus() == ResponseCodeEnum.UNKNOWN) {
//                if (attemptsLeft == 0) {
//                    onError.accept(new HederaException(ResponseCodeEnum.UNKNOWN));
//                } else {
//                    GlobalEventExecutor.INSTANCE.schedule(this::tryGetReceipt, RECEIPT_RETRY_DELAY,
//                        TimeUnit.MILLISECONDS);
//                }
//            } else {
//                if (receiptEmitted) throw new IllegalStateException();
//                receiptEmitted = true;
//
//                onReceipt.accept(receipt);
//            }
        }
    }
}
