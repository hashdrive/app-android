package org.hashdrive.hederahg;

import org.hashdrive.hederahg.account.AccountId;

import com.google.common.collect.ImmutableList;
import com.google.protobuf.ByteString;
import com.hedera.hashgraph.sdk.proto.AccountAmount;
import com.hedera.hashgraph.sdk.proto.AccountAmountOrBuilder;

import javax.annotation.Nullable;
import org.threeten.bp.Instant;

import java.util.Iterator;
import java.util.List;

public final class TransactionRecord {
    private final com.hedera.hashgraph.sdk.proto.TransactionRecord inner;

    TransactionRecord(com.hedera.hashgraph.sdk.proto.TransactionRecord inner) {
        this.inner = inner;
    }

    public TransactionId getTransactionId() {
        return new TransactionId(inner.getTransactionID());
    }

    public long getTransactionFee() {
        return inner.getTransactionFee();
    }

    public TransactionReceipt getReceipt() {
        return new TransactionReceipt(inner.getReceipt());
    }

    @Nullable
    public byte[] getTransactionHash() {
        ByteString hash = inner.getTransactionHash();
        // proto specifies hash is not provided if the transaction failed due to a duplicate ID
        return !hash.isEmpty() ? hash.toByteArray() : null;
    }

    @Nullable
    public Instant getConsensusTimestamp() {
        return inner.hasConsensusTimestamp() ? TimestampHelper.timestampTo(inner.getConsensusTimestamp()) : null;
    }

    @Nullable
    public String getMemo() {
        String memo = inner.getMemo();
        return !memo.isEmpty() ? memo : null;
    }

    @Nullable
    public FunctionResult getCallResult() {
        return inner.getContractCallResult().hasContractID() ? new FunctionResult(inner.getContractCallResult()) : null;
    }

    @Nullable
    public FunctionResult getCreateResult() {
        return inner.getContractCreateResult().hasContractID() ? new FunctionResult(inner.getContractCreateResult()) : null;
    }

    @Nullable
    public List<Transfer> getTransfers() {
        return inner.getTransferList().getAccountAmountsCount() > 0 ? buildTransferList(): null;
    }

    private ImmutableList buildTransferList() {
        Iterator<AccountAmount> iter = inner.getTransferList().getAccountAmountsList().iterator();
        ImmutableList.Builder transfersBuilder = new ImmutableList.Builder();
        while(iter.hasNext()) {
            transfersBuilder.add(new Transfer(iter.next()));
        }
        return transfersBuilder.build();
    }

    public final static class Transfer {
        public final AccountId account;
        public final long amount;

        private Transfer(AccountAmountOrBuilder accountAmount) {
            account = new AccountId(accountAmount.getAccountID());
            amount = accountAmount.getAmount();
        }
    }
}
