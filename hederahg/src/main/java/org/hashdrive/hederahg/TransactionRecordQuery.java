package org.hashdrive.hederahg;

import com.hedera.hashgraph.sdk.proto.*;

import io.grpc.MethodDescriptor;

public class TransactionRecordQuery extends QueryBuilder<TransactionRecord, TransactionRecordQuery> {
    private final TransactionGetRecordQuery.Builder builder = TransactionGetRecordQuery.newBuilder();
    private final QueryHeader.Builder headerBuilder = QueryHeader.newBuilder();

    public TransactionRecordQuery(Client client) {
        super(client);
    }

    TransactionRecordQuery() {
        super(null);
    }

    @Override
    protected QueryHeader.Builder getHeaderBuilder() {
        return headerBuilder;
    }

    public TransactionRecordQuery setTransactionId(TransactionId transaction) {
        builder.setTransactionID(transaction.toProto());
        return this;
    }

    @Override
    protected MethodDescriptor<Query, Response> getMethod() {
        return CryptoServiceGrpc.getGetTxRecordByTxIDMethod();
    }

    @Override
    protected org.hashdrive.hederahg.TransactionRecord fromResponse(Response raw) {
        return new TransactionRecord(
                raw.getTransactionGetRecord()
                    .getTransactionRecord());
    }

    @Override
    protected void doValidate() {
        require(builder.hasTransactionID(), ".setTransactionId() required");
    }
}
