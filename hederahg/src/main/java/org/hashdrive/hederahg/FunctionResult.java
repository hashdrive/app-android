package org.hashdrive.hederahg;

import com.google.common.collect.ImmutableList;
import com.google.protobuf.ByteString;
import org.hashdrive.hederahg.contraact.ContractId;
import com.hedera.hashgraph.sdk.proto.ContractFunctionResultOrBuilder;
import com.hedera.hashgraph.sdk.proto.ContractLoginfoOrBuilder;

import javax.annotation.Nullable;
import java.util.List;
import java.util.stream.Collectors;

public final class FunctionResult {
    private final ContractFunctionResultOrBuilder inner;

    public FunctionResult(ContractFunctionResultOrBuilder inner) {
        this.inner = inner;
    }

    public ContractId getContractId() {
        return new ContractId(inner.getContractID());
    }

    // The call result as the Solidity-encoded bytes, does NOT get the function result as bytes
    // RFC: do we want to remove this in favor of the strong getters below?
    public byte[] getCallResult() {
        return getByteString().toByteArray();
    }

    @Nullable
    public String getErrorMessage() {
        String errMsg = inner.getErrorMessage();
        return !errMsg.isEmpty() ? errMsg : null;
    }

    public byte[] getBloomFilter() {
        return inner.getBloom()
            .toByteArray();
    }

    public long getGasUsed() {
        return inner.getGasUsed();
    }

    public List<LogInfo> getLogs() {
        return inner.getLogInfoList()
            .stream()
            .map(LogInfo::new)
            .collect(Collectors.toList());
    }

    // get the first (or only) returned value as a string
    public String getString() {
        return getString(0);
    }

    // get the nth returned value as a string
    public String getString(int valIndex) {
        return getBytes(valIndex).toStringUtf8();
    }

    // get the first (or only) returned value as an int
    public int getInt() {
        return getInt(0);
    }

    // get the nth returned value as an int
    public int getInt(int valIndex) {
        // int will be the last 4 bytes in the "value"
        return getIntValueAt(valIndex * 32);
    }

    private int getIntValueAt(int valueOffset) {
        // **NB** `.asReadOnlyByteBuffer()` on a substring reads from the start of the parent, not the substring (bug)
        return getByteString().asReadOnlyByteBuffer()
            .getInt(valueOffset + 28);
    }

    // get a dynamic byte array with the offset at the given valIndex
    private ByteString getBytes(int valIndex) {
        int offset = getInt(valIndex);
        int len = getIntValueAt(offset);
        return getByteString().substring(offset + 32, offset + 32 + len);
    }

    private ByteString getByteString() {
        return inner.getContractCallResult();
    }

    // this only appears in this API so
    public final static class LogInfo {
        public final ContractId contractId;
        public final byte[] bloomFilter;
        public final List<byte[]> topics;
        public final byte[] data;

        private LogInfo(ContractLoginfoOrBuilder logInfo) {
            contractId = new ContractId(logInfo.getContractID());
            bloomFilter = logInfo.getBloom()
                .toByteArray();
            topics = buildTopics(logInfo.getTopicList());
            data = logInfo.getData()
                .toByteArray();
        }

        private List<byte[]> buildTopics(List<ByteString> topicList) {
            ImmutableList.Builder<byte[]> topicsBuilder = ImmutableList.builder();
            for (ByteString bytes : topicList) {
                topicsBuilder.add(bytes.toByteArray());
            }
            return topicsBuilder.build();
        }
    }
}
