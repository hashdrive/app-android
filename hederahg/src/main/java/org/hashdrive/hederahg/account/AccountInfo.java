package org.hashdrive.hederahg.account;

import org.hashdrive.hederahg.DurationHelper;
import org.hashdrive.hederahg.TimestampHelper;

import com.google.common.collect.ImmutableList;

import org.hashdrive.hederahg.crypto.Key;
import org.threeten.bp.Instant;

import com.hedera.hashgraph.sdk.proto.CryptoGetInfoResponse;
import com.hedera.hashgraph.sdk.proto.Response;
import org.threeten.bp.Duration;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;

public class AccountInfo {
    private final CryptoGetInfoResponse.AccountInfo inner;

    public AccountId getAccountId() {
        return new AccountId(inner.getAccountID());
    }

    public String getContractAccountId() {
        return inner.getContractAccountID();
    }

    public boolean isDeleted() {
        return inner.getDeleted();
    }

    @Nullable
    public AccountId getProxyAccountId() {
        return inner.hasProxyAccountID() ? new AccountId(inner.getProxyAccountID()) : null;
    }

    public long getProxyReceived() {
        return inner.getProxyReceived();
    }

    public Key getKey() {
        return Key.fromProtoKey(inner.getKey());
    }

    public long getBalance() {
        return inner.getBalance();
    }

    public long getGenerateSendRecordThreshold() {
        return inner.getGenerateSendRecordThreshold();
    }

    public long getGenerateReceiveRecordThreshold() {
        return inner.getGenerateReceiveRecordThreshold();
    }

    public boolean isReceiverSignatureRequired() {
        return inner.getReceiverSigRequired();
    }

    public Instant getExpirationTime() {
        return TimestampHelper.timestampTo(inner.getExpirationTime());
    }

    public Duration getAutoRenewPeriod() {
        return DurationHelper.durationTo(inner.getAutoRenewPeriod());
    }

    public List<Claim> getClaims() {
        return buildClaims();
    }

    private List<Claim> buildClaims() {
        Iterator<com.hedera.hashgraph.sdk.proto.Claim> iter = inner.getClaimsList().iterator();
        ImmutableList.Builder<Claim> claimsListBuilder = ImmutableList.builder();
        while(iter.hasNext()) {
            claimsListBuilder.add(new Claim(iter.next()));
        }
        return claimsListBuilder.build();
    }

    public AccountInfo(Response response) {
        if (response.getCryptoGetInfo() == null) {
            throw new IllegalArgumentException("query response was not `CryptoGetInfoResponse`");
        }

        CryptoGetInfoResponse infoResponse = response.getCryptoGetInfo();
        inner = infoResponse.getAccountInfo();

        if (!inner.hasKey()) {
            throw new IllegalArgumentException("query response missing key");
        }
    }
}
