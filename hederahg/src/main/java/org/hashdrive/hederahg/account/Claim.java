package org.hashdrive.hederahg.account;

import com.google.common.collect.ImmutableList;
import com.hedera.hashgraph.sdk.proto.AccountID;

import org.hashdrive.hederahg.Entity;
import org.hashdrive.hederahg.crypto.Key;

import java.util.Iterator;
import java.util.List;

public final class Claim implements Entity {
    private final com.hedera.hashgraph.sdk.proto.Claim inner;

    public Claim(com.hedera.hashgraph.sdk.proto.Claim inner) {
        this.inner = inner;
    }

    public AccountId getAcccount() {
        AccountID account = this.inner.getAccountID();

        return new AccountId(account.getShardNum(), account.getRealmNum(), account.getAccountNum());
    }

    public byte[] getHash() {
        return this.inner.getHash()
            .toByteArray();
    }

    public List<Key> getKeys() {
        return buildKeys();
    }

    private List<Key> buildKeys() {
        Iterator<com.hedera.hashgraph.sdk.proto.Key> iter = this.inner.getKeys().getKeysList().iterator();
        ImmutableList.Builder<Key> keysBuilder = ImmutableList.builder();
        while(iter.hasNext()) {
            keysBuilder.add(Key.fromProtoKey(iter.next()));
        }
        return keysBuilder.build();
    }
}
