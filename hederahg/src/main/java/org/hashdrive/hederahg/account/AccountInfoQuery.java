package org.hashdrive.hederahg.account;

import org.hashdrive.hederahg.Client;
import org.hashdrive.hederahg.QueryBuilder;
import com.hedera.hashgraph.sdk.proto.*;
import io.grpc.MethodDescriptor;

// `CryptoGetInfoQuery`
public final class AccountInfoQuery extends QueryBuilder<AccountInfo, AccountInfoQuery> {
    private final com.hedera.hashgraph.sdk.proto.CryptoGetInfoQuery.Builder builder = CryptoGetInfoQuery.newBuilder();
    private final QueryHeader.Builder queryHeaderBuilder = QueryHeader.newBuilder();


    public AccountInfoQuery(Client client) {
        super(client);
    }

    AccountInfoQuery() {
        super(null);
    }

    @Override
    protected QueryHeader.Builder getHeaderBuilder() {
        return queryHeaderBuilder;
    }

    public AccountInfoQuery setAccountId(AccountId account) {
        builder.setAccountID(account.toProto());
        return this;
    }

    @Override
    protected void doValidate() {
        require(builder.hasAccountID(), ".setAccountId() required");
    }

    @Override
    protected MethodDescriptor<Query, Response> getMethod() {
        return CryptoServiceGrpc.getGetAccountInfoMethod();
    }

    @Override
    protected AccountInfo fromResponse(Response raw) {
        return new AccountInfo(raw);
    }
}
