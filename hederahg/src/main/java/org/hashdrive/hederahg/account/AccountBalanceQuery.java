package org.hashdrive.hederahg.account;

import org.hashdrive.hederahg.QueryBuilder;
import com.hedera.hashgraph.sdk.proto.*;

import org.hashdrive.hederahg.Client;

import io.grpc.MethodDescriptor;

import javax.annotation.Nullable;

// `CryptoGetAccountBalanceQuery`
public final class AccountBalanceQuery extends QueryBuilder<Long, AccountBalanceQuery> {
    private final CryptoGetAccountBalanceQuery.Builder builder = CryptoGetAccountBalanceQuery.newBuilder();


    private final QueryHeader.Builder headerBuilder = QueryHeader.newBuilder().setResponseType(ResponseType.ANSWER_ONLY);// todo .setPayment(Transaction.newBuilder().setSigMap(SignatureMap.newBuilder().setSigPair(0, SignaturePair.newBuilder().set)));

    public AccountBalanceQuery(@Nullable Client client) {
        super(client);
    }

    @Override
    protected QueryHeader.Builder getHeaderBuilder() {
        return headerBuilder;
    }

    public AccountBalanceQuery setAccountId(AccountId account) {
        builder.setAccountID(account.toProto());
        inner.setCryptogetAccountBalance(builder.build()); // also update inner query builder
        return this;
    }

    @Override
    protected void doValidate() {
        require(builder.hasAccountID(), ".setAccountId() required");
    }

    @Override
    protected MethodDescriptor<Query, Response> getMethod() {
        return CryptoServiceGrpc.getCryptoGetBalanceMethod();
    }

    @Override
    public Long fromResponse(Response raw) {
        return raw.getCryptogetAccountBalance()
            .getBalance();
    }
}
