package org.hashdrive.hederahg.account;

import org.hashdrive.hederahg.Client;
import org.hashdrive.hederahg.TransactionBuilder;
import com.hedera.hashgraph.sdk.proto.*;
import io.grpc.MethodDescriptor;

import javax.annotation.Nonnegative;
import javax.annotation.Nullable;

public final class CryptoTransferTransaction extends TransactionBuilder<CryptoTransferTransaction> {
    private final TransferList.Builder transferListBuilder = TransferList.newBuilder();

    public CryptoTransferTransaction(@Nullable Client client) {
        super(client);

    }

    public CryptoTransferTransaction addSender(AccountId senderId, @Nonnegative long value) {
        return addTransfer(senderId, value * -1L);
    }

    public CryptoTransferTransaction addRecipient(AccountId recipientId, @Nonnegative long value) {
        return addTransfer(recipientId, value);
    }

    public CryptoTransferTransaction addTransfer(AccountId accountId, long value) {
        transferListBuilder.addAccountAmounts(
            AccountAmount.newBuilder()
                .setAccountID(accountId.toProto())
                .setAmount(value)
                .build());
        bodyBuilder.setCryptoTransfer(CryptoTransferTransactionBody.newBuilder().setTransfers(transferListBuilder.build())); // important to update super's class property #bodyBuilder
        return this;
    }

    @Override
    protected void doValidate() {
        require(transferListBuilder.getAccountAmountsList(), "at least one transfer required");

        long sum = 0;

        for (AccountAmount acctAmt : transferListBuilder.getAccountAmountsList()) {
            sum += acctAmt.getAmount();
        }

        if (sum != 0) {
            addValidationError(String.format("transfer transaction must have zero sum; transfer balance: %d tinybar", sum));
        }
    }

    @Override
    protected MethodDescriptor<Transaction, TransactionResponse> getMethod() {
        return CryptoServiceGrpc.getCryptoTransferMethod();
    }
}
