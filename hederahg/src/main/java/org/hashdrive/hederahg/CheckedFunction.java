package org.hashdrive.hederahg;

@FunctionalInterface
interface CheckedFunction<T, R> {
    R apply(T t) throws HederaException, HederaNetworkException;
}
