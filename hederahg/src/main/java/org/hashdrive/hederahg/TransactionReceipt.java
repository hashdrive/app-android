package org.hashdrive.hederahg;

import org.hashdrive.hederahg.account.AccountId;
//import org.hashdrive.hederahg.contraact.ContractId;
//import com.hedera.hashgraph.sdk.file.FileId;
import com.hedera.hashgraph.sdk.proto.Response;
import com.hedera.hashgraph.sdk.proto.ResponseCodeEnum;

public final class TransactionReceipt {
    private final com.hedera.hashgraph.sdk.proto.TransactionReceipt inner;

    TransactionReceipt(Response response) {
        if (response.getTransactionGetReceipt() == null) {
            throw new IllegalArgumentException("response was not `TransactionGetReceipt`");
        }

        inner = response.getTransactionGetReceipt()
            .getReceipt();
    }

    TransactionReceipt(com.hedera.hashgraph.sdk.proto.TransactionReceipt inner) {
        this.inner = inner;
    }

    public ResponseCodeEnum getStatus() {
        return inner.getStatus();
    }

    public int getStatusValue() {
        return inner.getStatusValue();
    }

    public AccountId getAccountId() {
        if (!inner.hasAccountID()) {
            throw new IllegalStateException("receipt does not contain an account ID");
        }

        return new AccountId(inner.getAccountID());
    }

//    TODO: 2019-06-23 uncomment when ready
//    public FileId getFileId() {
//        if (!inner.hasFileID()) {
//            throw new IllegalStateException("receipt does not contain a file ID");
//        }
//
//        return new FileId(inner.getFileID());
//    }
//
//    public ContractId getContractId() {
//        if (!inner.hasContractID()) {
//            throw new IllegalStateException("receipt does not contain a contract ID");
//        }
//
//        return new ContractId(inner.getContractID());
//    }

    public com.hedera.hashgraph.sdk.proto.TransactionReceipt toProto() {
        return inner;
    }
}
