package org.hashdrive.hederahg;

import com.google.common.collect.ImmutableMap;
import com.google.protobuf.ByteString;
import com.hedera.hashgraph.sdk.proto.AccountAmount;
import com.hedera.hashgraph.sdk.proto.CryptoCreateTransactionBody;
import com.hedera.hashgraph.sdk.proto.CryptoGetAccountBalanceQuery;
import com.hedera.hashgraph.sdk.proto.CryptoGetInfoQuery;
import com.hedera.hashgraph.sdk.proto.CryptoTransferTransactionBody;
import com.hedera.hashgraph.sdk.proto.Query;
import com.hedera.hashgraph.sdk.proto.QueryHeader;
import com.hedera.hashgraph.sdk.proto.RealmID;
import com.hedera.hashgraph.sdk.proto.Response;
import com.hedera.hashgraph.sdk.proto.ResponseType;
import com.hedera.hashgraph.sdk.proto.RxCryptoServiceGrpc;
import com.hedera.hashgraph.sdk.proto.ShardID;
import com.hedera.hashgraph.sdk.proto.SignatureMap;
import com.hedera.hashgraph.sdk.proto.SignaturePair;
import com.hedera.hashgraph.sdk.proto.TransactionBody;
import com.hedera.hashgraph.sdk.proto.TransactionGetReceiptQuery;
import com.hedera.hashgraph.sdk.proto.TransactionID;
import com.hedera.hashgraph.sdk.proto.TransferList;

import org.hashdrive.hederahg.account.AccountBalanceQuery;
import org.hashdrive.hederahg.account.AccountCreateTransaction;
import org.hashdrive.hederahg.account.AccountId;
import org.hashdrive.hederahg.account.AccountInfo;
import org.hashdrive.hederahg.account.CryptoTransferTransaction;
import org.hashdrive.hederahg.crypto.Key;
import org.hashdrive.hederahg.crypto.ed25519.Ed25519PrivateKey;
import org.hashdrive.hederahg.crypto.ed25519.Ed25519Signature;
import org.threeten.bp.Duration;
import org.threeten.bp.Instant;

import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnegative;
import javax.annotation.Nullable;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public final class Client {
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private final Random random = new Random();
    private Map<AccountId, Node> channels;

    static final long DEFAULT_MAX_TXN_FEE = 100_000;

    // todo: transaction fees should be defaulted to whatever the transaction fee schedule is
    private long maxTransactionFee = DEFAULT_MAX_TXN_FEE;

    @Nullable
    private AccountId operatorId;

    @Nullable
    private Ed25519PrivateKey operatorKey;

    public Client(Map<AccountId, String> nodes) {
        if (nodes.isEmpty()) {
            throw new IllegalArgumentException("List of nodes must not be empty");
        }
        channels = buildAccountIdNodeMap(nodes);
    }

    private Map<AccountId, Node> buildAccountIdNodeMap(Map<AccountId, String> nodes) {
        ImmutableMap.Builder<AccountId, Node> immutableMapBuilder = ImmutableMap.builder();
        for (Map.Entry<AccountId, String> accountIdStringPair : nodes.entrySet()) {
            immutableMapBuilder.put(accountIdStringPair.getKey(), new Node(accountIdStringPair.getKey(), accountIdStringPair.getValue()));
        }
        return immutableMapBuilder.build();
    }


    public Client setMaxTransactionFee(@Nonnegative long maxTransactionFee) {
        if (maxTransactionFee <= 0) {
            throw new IllegalArgumentException("maxTransactionFee must be > 0");
        }

        this.maxTransactionFee = maxTransactionFee;
        return this;
    }

    public Client setOperator(AccountId operatorId, Ed25519PrivateKey operatorKey) {
        this.operatorId = operatorId;
        this.operatorKey = operatorKey;
        return this;
    }

    public long getMaxTransactionFee() {
        return maxTransactionFee;
    }

    @Nullable
    AccountId getOperatorId() {
        return operatorId;
    }

    @Nullable
    Ed25519PrivateKey getOperatorKey() {
        return operatorKey;
    }

    Node pickNode() {
        if (channels.isEmpty()) {
            throw new IllegalStateException("List of channels has become empty");
        }

        int r = random.nextInt(channels.size());
        Iterator<Node> channelIter = channels.values()
                .iterator();

        for (int i = 1; i < r; i++) {
            channelIter.next();
        }

        return channelIter.next();
    }

    Node getNodeForId(AccountId node) {
        Node selectedChannel = channels.get(node);

        if (selectedChannel == null) {
            throw new IllegalArgumentException("Node Id does not exist");
        }

        return selectedChannel;
    }

    /**
     * Creates default transaction payment
     *  - using {@link #operatorId}
     *  - hardcoded cost (100_000 tinybars)
     * @return txPayment
     */
    private org.hashdrive.hederahg.Transaction createDefaultTxPayment(Node pickedNode) {
        int cost = 100_000;
        AccountId operatorId = getOperatorId();
        AccountId nodeId = pickedNode.accountId;

        return new CryptoTransferTransaction(this)
                .setNodeAccountId(nodeId)
                .setTransactionId(new TransactionId(operatorId))
                .addSender(operatorId, cost)
                .addRecipient(nodeId, cost)
                .build();
    }

    private TransactionBody.Builder createDefaultTxBodyBuilder(Node pickedNode) {
        return TransactionBody.newBuilder()
                .setTransactionID(TransactionID.newBuilder()
                        .setTransactionValidStart(TimestampHelper.timestampFrom(Instant.now()))
                        .setAccountID(operatorId.toProto())
                        .build())
                .setNodeAccountID(pickedNode.accountId.toProto())
                .setTransactionValidDuration(DurationHelper.durationFrom(Duration.ofMinutes(2))) // consensus must be reached within 2 minutes
                .setTransactionFee(getMaxTransactionFee());
    }

    private SignatureMap createDefaultTxSignatureMap(TransactionBody transactionBody) {
        ByteString pubKey = ByteString.copyFrom(operatorKey.getPublicKey().toBytes());
        byte[] signature = Ed25519Signature.forMessage(operatorKey, transactionBody.toByteArray()).toBytes();

        return SignatureMap.newBuilder()
                .addSigPair(SignaturePair.newBuilder()
                        .setPubKeyPrefix(pubKey)
                        .setEd25519(ByteString.copyFrom(signature)).build())
                .build();
    }

    /**
     * Used to obtain receipt for tx with transactionID (note: this query is free)
     * @param transactionID
     * @return
     */
    private Single<Response> getTransactionReceiptQueryObservable(TransactionID transactionID) {
        QueryHeader queryHeader = QueryHeader.newBuilder()
                .setResponseType(ResponseType.ANSWER_ONLY)
                .build();

        TransactionGetReceiptQuery transactionGetReceiptQuery = TransactionGetReceiptQuery.newBuilder()
                .setHeader(queryHeader)
                .setTransactionID(transactionID)
                .build();

        Query txGetReceiptQuery = Query.newBuilder().setTransactionGetReceipt(transactionGetReceiptQuery).build();

        Timber.d("Sending txGetReceiptQuery %s", txGetReceiptQuery.toString());
        return RxCryptoServiceGrpc.newRxStub(pickNode().getChannel())
                .getTransactionReceipts(txGetReceiptQuery)
                .doOnSuccess(response -> Timber.d("Query response: %s", response.toString()));
    }

    //
    // Simplified interface intended for high-level, opinionated operation
    //

    // TODO: 2019-06-23 uncomment and fix errors

    public AccountId createAccount(Key publicKey, long initialBalance) throws HederaException, HederaNetworkException {
        TransactionReceipt receipt = new AccountCreateTransaction(this).setKey(publicKey)
                .setInitialBalance(initialBalance)
                .executeForReceipt();

        return receipt.getAccountId();
    }


    /**
     * Based on {{{@link AccountCreateTransaction}}}
     *  - cost hardcoded to 100_000 tinybars
     *  - response type: answer only
     *
     * @param publicKey
     * @param initialBalance
     * @return
     */
    public Single<Long> createAccountObservable(Key publicKey, long initialBalance) {
        Node pickedNode = pickNode();

        CryptoCreateTransactionBody cryptoCreateTransactionBody = CryptoCreateTransactionBody.newBuilder()
                .setShardID(ShardID.newBuilder().setShardNum(getOperatorId().getShardNum()).build())
                .setRealmID(RealmID.newBuilder().setRealmNum(getOperatorId().getRealmNum()).build())
                // Default auto renewal period recommended from Hedera
                .setAutoRenewPeriod(DurationHelper.durationFrom(Duration.ofDays(30)))
                // Default to maximum values for record thresholds. Without this records would be
                // auto-created whenever a send or receive transaction takes place for this new account. This should
                // be an explicit ask.
                .setSendRecordThreshold(Long.MAX_VALUE)
                .setReceiveRecordThreshold(Long.MAX_VALUE)
                .setKey(publicKey.toKeyProto())
                .setInitialBalance(initialBalance)
                .build();

        TransactionBody txCreateAccountBody = createDefaultTxBodyBuilder(pickedNode)
                .setCryptoCreateAccount(cryptoCreateTransactionBody)
                .build();

        SignatureMap txCreateAccountSigmap = createDefaultTxSignatureMap(txCreateAccountBody);
        com.hedera.hashgraph.sdk.proto.Transaction txCreateAccount = com.hedera.hashgraph.sdk.proto.Transaction.newBuilder()
                .setBody(txCreateAccountBody)
                .setSigMap(txCreateAccountSigmap).build();

        Timber.d("Sending txCreateAccount: %s", txCreateAccount.toString());
        return RxCryptoServiceGrpc.newRxStub(pickedNode.getChannel())
                .createAccount(txCreateAccount)
                .flatMap(transactionResponse -> {
                    Timber.d("Transaction response: %s", transactionResponse.toString());
                    HederaException.throwIfExceptional(transactionResponse.getNodeTransactionPrecheckCode());
                    TransactionID txId = txCreateAccount.getBody().getTransactionID();
                    Timber.d("Retrieving receipt in 5 seconds, please wait...");
                    return Single.timer(5, TimeUnit.SECONDS).flatMap(timeElapsed -> getTransactionReceiptQueryObservable(txId));
                })
                .map(response -> response.getTransactionGetReceipt().getReceipt().getAccountID().getAccountNum());

    }

    /**
     * Based on {{{@link AccountBalanceQuery}}}
     *  - cost hardcoded to 100_000 tinybars
     *  - response type: answer only
     * @param id - accountId for which we want to retrieve balance info
     * @return
     */
    public Single<Long> getAccountBalanceObservable(AccountId id) {
        Node pickedNode = pickNode();

        org.hashdrive.hederahg.Transaction txPayment = createDefaultTxPayment(pickedNode);
        QueryHeader queryHeader = QueryHeader.newBuilder().setPayment(txPayment.toProto())
                .setResponseType(ResponseType.ANSWER_ONLY)
                .build();

        CryptoGetAccountBalanceQuery cryptoGetAccountBalanceQuery = CryptoGetAccountBalanceQuery.newBuilder()
                .setHeader(queryHeader)
                .setAccountID(id.toProto())
                .build();

        Query query = Query.newBuilder().setCryptogetAccountBalance(cryptoGetAccountBalanceQuery).build();
        Timber.d("Sending getAccountBalance query: %s", query.toString());
        return RxCryptoServiceGrpc.newRxStub(pickedNode.getChannel())
                .cryptoGetBalance(query) // toProto will also validate the query
                .map(response -> {
                    Timber.d("Transaction response: %s", response.toString());
                    return response.getCryptogetAccountBalance().getBalance();
                });
    }


    /**
     * Based on {{{@link org.hashdrive.hederahg.account.AccountInfoQuery}}}
     * @param id - accountId for which we want to obtain the info
     */
    public Single<AccountInfo> getAccountInfoObservable(AccountId id) {
        Node pickedNode = pickNode();
        org.hashdrive.hederahg.Transaction txPayment = createDefaultTxPayment(pickedNode);
        QueryHeader queryHeader = QueryHeader.newBuilder().setPayment(txPayment.toProto())
                .setResponseType(ResponseType.ANSWER_ONLY)
                .build();

        CryptoGetInfoQuery cryptoGetInfoQuery = CryptoGetInfoQuery.newBuilder()
                .setHeader(queryHeader)
                .setAccountID(id.toProto())
                .build();
        Query query = Query.newBuilder().setCryptoGetInfo(cryptoGetInfoQuery).build();
        Timber.d("Sending getAccountInfo query: %s", query.toString());
        return RxCryptoServiceGrpc.newRxStub(pickedNode.getChannel())
                .getAccountInfo(query)
                .doOnSuccess(response -> Timber.d("Transaction response: %s", response.toString()))
                .map(AccountInfo::new);
    }


    /**
     * Based on {{{{@link CryptoTransferTransaction}}}}
     * @param recipient
     * @param amount
     */
    public Single<TransactionReceipt> transferCryptoObservable(AccountId recipient, long amount) {
        Node pickedNode = pickNode();
        TransferList transferList = TransferList.newBuilder()
                .addAccountAmounts(addSender(operatorId, amount))
                .addAccountAmounts(addRecipient(recipient, amount))
                .build();

        CryptoTransferTransactionBody cryptoTransferTransactionBody = CryptoTransferTransactionBody.newBuilder()
                .setTransfers(transferList).build();

        TransactionBody txBody = createDefaultTxBodyBuilder(pickedNode)
                .setCryptoTransfer(cryptoTransferTransactionBody)
                .build();

        SignatureMap txSignatureMap = createDefaultTxSignatureMap(txBody);

        com.hedera.hashgraph.sdk.proto.Transaction txCryptoTransfer = com.hedera.hashgraph.sdk.proto.Transaction.newBuilder()
                .setBody(txBody)
                .setSigMap(txSignatureMap).build();

        return RxCryptoServiceGrpc.newRxStub(pickedNode.getChannel())
                .cryptoTransfer(txCryptoTransfer)
                .flatMap(transactionResponse -> {
                    HederaException.throwIfExceptional(transactionResponse.getNodeTransactionPrecheckCode());
                    TransactionID txId = txBody.getTransactionID();
                    Timber.d("Retrieving receipt in 5 seconds, please wait...");
                    return Single.timer(5, TimeUnit.SECONDS).flatMap(timeElapsed -> getTransactionReceiptQueryObservable(txId));
                })
                .map(TransactionReceipt::new);
    }

    private AccountAmount addSender(AccountId senderId, @Nonnegative long value) {
        return createNewTransfer(senderId, value * -1L);
    }

    private AccountAmount addRecipient(AccountId recipientId, @Nonnegative long value) {
        return createNewTransfer(recipientId, value);
    }

    private AccountAmount createNewTransfer(AccountId accountId, long value) {
        return AccountAmount.newBuilder()
                .setAccountID(accountId.toProto())
                .setAmount(value)
                .build();
    }

}
