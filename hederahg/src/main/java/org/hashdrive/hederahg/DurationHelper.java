package org.hashdrive.hederahg;

import com.hedera.hashgraph.sdk.proto.Duration;

public final class DurationHelper {
    private DurationHelper() { }

    public static Duration durationFrom(org.threeten.bp.Duration duration) {
        return Duration.newBuilder()
            .setSeconds(duration.getSeconds())
            .build();
    }

    public static org.threeten.bp.Duration durationTo(Duration duration) {
        return org.threeten.bp.Duration.ofSeconds(duration.getSeconds());
    }
}
